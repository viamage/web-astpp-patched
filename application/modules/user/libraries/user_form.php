<?php
// ##############################################################################
// ASTPP - Open Source VoIP Billing Solution
//
// Copyright (C) 2016 iNextrix Technologies Pvt. Ltd.
// Samir Doshi <samir.doshi@inextrix.com>
// ASTPP Version 3.0 and above
// License https://www.gnu.org/licenses/agpl-3.0.html
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
// ##############################################################################
if (!defined('BASEPATH')) {
    exit ('No direct script access allowed');
}

class User_form
{
    function __construct($library_name = '')
    {
        $this->CI = &get_instance();
    }

    function build_packages_list_for_user()
    {
        $grid_field_arr = json_encode(
            [
                [
                    gettext("Name"),
                    "310",
                    "package_name",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Rate Group"),
                    "250",
                    "pricelist_id",
                    "name",
                    "pricelists",
                    "get_field_name",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Included Seconds"),
                    "260",
                    "includedseconds",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Status"),
                    "160",
                    "status",
                    "status",
                    "status",
                    "get_status",
                    "",
                    "true",
                    "center",
                ],
            ]
        );

        return $grid_field_arr;
    }

    function build_refill_list_for_user()
    {
        $grid_field_arr = json_encode(
            [

                [
                    gettext("Date"),
                    "225",
                    "payment_date",
                    "",
                    "",
                    "",
                ],
                [
                    gettext("Amount"),
                    "250",
                    "credit",
                    "credit",
                    "credit",
                    "convert_to_currency",
                ],
                [
                    gettext("Refill By"),
                    "230",
                    "payment_by",
                    "first_name,last_name,number",
                    "accounts",
                    "get_refill_by",
                ],
                [
                    gettext("Note"),
                    "290",
                    "notes",
                    "",
                    "",
                    "",
                ],
            ]
        );

        return $grid_field_arr;
    }

    function build_emails_list_for_user()
    {
        $grid_field_arr = json_encode(
            [
                [
                    gettext("Date"),
                    "110",
                    "date",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("From"),
                    "170",
                    "from",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Body"),
                    "550",
                    "body",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Attachement"),
                    "100",
                    "attachment",
                    "attachment",
                    "attachment",
                    "attachment_icons",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Status"),
                    "100",
                    "status",
                    "status",
                    "status",
                    "email_status",
                    "",
                    "true",
                    "center",
                ],
            ]
        );

        return $grid_field_arr;
    }

    function build_user_emails_search()
    {
        $form ['forms'] = [
            "",
            [
                'id' => "user_emails_search",
            ],
        ];
        $form ['Search'] = [
            [
                gettext('From Date'),
                'INPUT',
                [
                    'name'  => 'date[]',
                    'id'    => 'customer_cdr_from_date',
                    'size'  => '20',
                    'class' => "text field ",
                ],
                '',
                'tOOL TIP',
                '',
                'date[date-date]',
            ],
            [
                gettext('To Date'),
                'INPUT',
                [
                    'name'  => 'date[]',
                    'id'    => 'customer_cdr_to_date',
                    'size'  => '20',
                    'class' => "text field ",
                ],
                '',
                'tOOL TIP',
                '',
                'date[date-date]',
            ],
            [
                gettext('From'),
                'INPUT',
                [
                    'name'  => 'from[from]',
                    '',
                    'id'    => 'from',
                    'size'  => '20',
                    'class' => "text field ",
                ],
                '',
                'tOOL TIP',
                '1',
                'from[from-string]',
                '',
                '',
                '',
                'search_string_type',
                '',
            ],
            [
                gettext('Body'),
                'INPUT',
                [
                    'name'  => 'body[body]',
                    '',
                    'id'    => 'body',
                    'size'  => '20',
                    'class' => "text field ",
                ],
                '',
                'tOOL TIP',
                '1',
                'body[body-string]',
                '',
                '',
                '',
                'search_string_type',
                '',
            ],
            [
                '',
                'HIDDEN',
                'ajax_search',
                '1',
                '',
                '',
                '',
            ],
            [
                '',
                'HIDDEN',
                'advance_search',
                '1',
                '',
                '',
                '',
            ],
        ];
        $form ['button_search'] = [
            'name'    => 'action',
            'id'      => "user_email_search_btn",
            'content' => gettext('Search'),
            'value'   => 'save',
            'type'    => 'button',
            'class'   => 'btn btn-line-parrot pull-right',
        ];
        $form ['button_reset'] = [
            'name'    => 'action',
            'id'      => "id_reset",
            'content' => gettext('Clear'),
            'value'   => 'cancel',
            'type'    => 'reset',
            'class'   => 'btn btn-line-sky pull-right margin-x-10',
        ];

        return $form;
    }

    function get_userprofile_form_fields($dataArr = false)
    {
        if ($dataArr ['id'] > 0) {
            $val = 'accounts.email.'.$dataArr ['id'];
        } else {
            $val = 'accounts.email';
        }
        $uname = $this->CI->common->find_uniq_rendno(
            common_model::$global_config ['system_config'] ['cardlength'],
            'number',
            'accounts'
        );
        $password = $this->CI->common->generate_password();
        $logintype = $this->CI->session->userdata('logintype');
        $pin = ($logintype == '0') ? [
            gettext('Pin'),
            'INPUT',
            [
                'name'  => 'pin',
                'size'  => '20',
                'class' => "text field medium",
            ],
            'tOOL TIP',
            '',
        ] : [
            '',
            'HIDDEN',
            [
                'name' => 'Pin',
            ],
            '',
            '',
            '',
            '',
        ];
        $form ['forms'] = [
            base_url().'user/user_myprofile/',
            [
                "id"   => "user_form",
                "name" => "user_form",
            ],
        ];

        $form ['User Profile'] = [
            [
                '',
                'HIDDEN',
                [
                    'name' => 'id',
                ],
                '',
                '',
                '',
                '',
            ],
            [
                '',
                'HIDDEN',
                [
                    'name'  => 'type',
                    'value' => '0',
                ],
                '',
                '',
                '',
            ],
            [
                gettext('Account Number'),
                'INPUT',
                [
                    'name'     => 'number',
                    'value'    => $uname,
                    'size'     => '20',
                    'readonly' => true,
                    'class'    => "text field medium",
                ],
                '',
                'tOOL TIP',
                'Please Enter account number',
            ],
            $pin,
            [
                gettext('Company'),
                'INPUT',
                [
                    'name'  => 'company_name',
                    'size'  => '15',
                    'class' => 'text field medium',
                ],
                'trim|xss_clean',
                'tOOL TIP',
                '',
            ],
            [
                gettext('First Name'),
                'INPUT',
                [
                    'name'  => 'first_name',
                    'id'    => 'first_name',
                    'size'  => '15',
                    'class' => "text field medium",
                ],
                'trim|required|xss_clean',
                'tOOL TIP',
                'Please Enter account number',
            ],
            [
                gettext('Last Name'),
                'INPUT',
                [
                    'name'  => 'last_name',
                    'size'  => '15',
                    'class' => "text field medium",
                ],
                'trim|alpha_numeric_space|xss_clean',
                'tOOL TIP',
                'Please Enter Password',
            ],
            [
                gettext('Telephone 1'),
                'INPUT',
                [
                    'name'  => 'telephone_1',
                    'size'  => '15',
                    'class' => "text field medium",
                ],
                'phn_number',
                'tOOL TIP',
                'Please Enter Password',
            ],
            [
                gettext('Telephone 2'),
                'INPUT',
                [
                    'name'  => 'telephone_2',
                    'size'  => '15',
                    'class' => "text field medium",
                ],
                'phn_number',
                'tOOL TIP',
                'Please Enter Password',
            ],
            [
                gettext('Email'),
                'INPUT',
                [
                    'name'  => 'email',
                    'size'  => '50',
                    'class' => "text field medium",
                ],
                'required|valid_email|is_unique['.$val.']',
                'tOOL TIP',
                'Please Enter Password',
            ],
            [
                gettext('Address 1'),
                'INPUT',
                [
                    'name'  => 'address_1',
                    'size'  => '15',
                    'class' => "text field medium",
                ],
                '',
                'tOOL TIP',
                'Please Enter Password',
            ],
            [
                gettext('Address 2'),
                'INPUT',
                [
                    'name'  => 'address_2',
                    'size'  => '15',
                    'class' => "text field medium",
                ],
                '',
                'tOOL TIP',
                'Please Enter Password',
            ],
            [
                gettext('City'),
                'INPUT',
                [
                    'name'  => 'city',
                    'size'  => '20',
                    'class' => "text field medium",
                ],
                '',
                'tOOL TIP',
                'Please Enter Password',
            ],
            [
                gettext('Province/State'),
                'INPUT',
                [
                    'name'  => 'province',
                    'size'  => '15',
                    'class' => "text field medium",
                ],
                '',
                'tOOL TIP',
                'Please Enter Password',
            ],
            [
                gettext('Zip/Postal Code'),
                'INPUT',
                [
                    'name'  => 'postal_code',
                    'size'  => '15',
                    'class' => "text field medium",
                ],
                'trim|xss_clean',
                'tOOL TIP',
                'Please Enter Password',
            ],
            [
                gettext('Country'),
                [
                    'name'  => 'country_id',
                    'class' => 'country_id',
                ],
                'SELECT',
                '',
                '',
                'tOOL TIP',
                'Please Enter account number',
                'id',
                'country',
                'countrycode',
                'build_dropdown',
                '',
                '',
            ],
            [
                gettext('Timezone'),
                [
                    'name'  => 'timezone_id',
                    'class' => 'timezone_id',
                ],
                'SELECT',
                '',
                '',
                'tOOL TIP',
                'Please Enter account number',
                'id',
                'gmtzone',
                'timezone',
                'build_dropdown',
                '',
                '',
            ],
            [
                gettext('Tax Number'),
                'INPUT',
                [
                    'name'  => 'tax_number',
                    'size'  => '100',
                    'class' => "text field medium",
                ],
                '',
                'tOOL TIP',
                '',
            ],
        ];
        $form ['button_save'] = [
            'name'    => 'action',
            'content' => gettext('Save'),
            'value'   => 'save',
            'type'    => 'submit',
            'class'   => 'btn btn-line-parrot',
        ];

        return $form;
    }

    function get_userprofile_change_password()
    {
        $form ['forms'] = [
            base_url().'user/user_change_password/',
            [
                "id"   => "customer_alert_threshold",
                "name" => "user_change_password",
            ],
        ];
        $form [gettext('Change Password')] = [
            [
                '',
                'HIDDEN',
                [
                    'name' => 'id',
                ],
                '',
                '',
                '',
                '',
            ],
            [
                gettext('Old Password'),
                'PASSWORD',
                [
                    'name'        => 'password',
                    'size'        => '20',
                    'class'       => "text field medium",
                    'id'          => 'old_password_show',
                    'onmouseover' => 'seetext(old_password_show)',
                    'onmouseout'  => 'hidepassword(old_password_show)',
                ],
                'required|password_check[accounts]',
                'tOOL TIP',
                '',
                '',
            ],
            [
                gettext('New Password'),
                'PASSWORD',
                [
                    'name'        => 'new_password',
                    'size'        => '20',
                    'class'       => "text field medium",
                    'id'          => 'new_password_show',
                    'onmouseover' => 'seetext(new_password_show)',
                    'onmouseout'  => 'hidepassword(new_password_show)',
                ],
                'required|',
                'tOOL TIP',
                '',
                '',
            ],
            [
                gettext('Confirm Password'),
                'PASSWORD',
                [
                    'name'        => 'new_confirm_password',
                    'size'        => '20',
                    'class'       => "text field medium",
                    'id'          => 'password_show',
                    'onmouseover' => 'seetext(password_show)',
                    'onmouseout'  => 'hidepassword(password_show)',
                ],
                "required|matches[new_password]",
                'tOOL TIP',
                '',
                '',
            ],
        ];
        $form ['button_save'] = [
            'name'    => 'action',
            'content' => gettext('Save'),
            'value'   => 'save',
            'type'    => 'submit',
            'class'   => 'btn btn-line-parrot',
        ];

        return $form;
    }

    function build_user_invoices()
    {
        $account_info = $accountinfo = $this->CI->session->userdata('accountinfo');
        $currency_id = $account_info ['currency_id'];
        $currency = $this->CI->common->get_field_name('currency', 'currency', $currency_id);
        $url = ($this->CI->session->userdata(
                'logintype'
            ) == 0) ? "/user/user_invoice_download/" : '/invoices/invoice_main_download/';
        $grid_field_arr = json_encode(
            [
                [
                    gettext("Number"),
                    "110",
                    "id",
                    "id,'',type",
                    "invoices",
                    "build_concat_string",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Type"),
                    "100",
                    "id",
                    "id,'',type",
                    "invoices",
                    "build_concat_string",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Generated Date"),
                    "110",
                    "invoice_date",
                    "invoice_date",
                    "",
                    "get_invoice_date",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("From Date"),
                    "100",
                    "from_date",
                    "from_date",
                    "",
                    "get_from_date",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Due Date"),
                    "100",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Last Pay Date"),
                    "100",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Amount")."($currency)",
                    "100",
                    "id",
                    "id",
                    "id",
                    "get_invoice_total",
                    "",
                    "true",
                    "right",
                ],
                [
                    gettext("Outstanding Amount")."<br>($currency)",
                    "150",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "right",
                ],
                [
                    gettext("Action"),
                    "140",
                    "",
                    "",
                    "",
                    [
                        "DOWNLOAD" => [
                            "url"  => $url,
                            "mode" => "single",
                        ],
                    ],
                ],
            ]
        );

        return $grid_field_arr;
    }

    function build_user_invoices_search()
    {
        $form ['forms'] = [
            "",
            [
                'id' => "user_invoice_search",
            ],
        ];
        $form ['Search'] = [
            [
                gettext('From Date'),
                'INPUT',
                [
                    'name'  => 'from_date[0]',
                    'id'    => 'invoice_from_date',
                    'size'  => '20',
                    'class' => "text field",
                ],
                '',
                'tOOL TIP',
                '',
                'from_date[from_date-date]',
            ],
            [
                gettext('To Date'),
                'INPUT',
                [
                    'name'  => 'to_date[1]',
                    'id'    => 'invoice_to_date',
                    'size'  => '20',
                    'class' => "text field",
                ],
                '',
                'tOOL TIP',
                '',
                'to_date[to_date-date]',
            ],
            [
                gettext('Amount'),
                'INPUT',
                [
                    'name'  => 'amount[amount]',
                    'value' => '',
                    'size'  => '20',
                    'class' => "text field ",
                ],
                '',
                'Tool tips info',
                '1',
                'amount[amount-string]',
                '',
                '',
                '',
                'search_string_type',
                '',
            ],
            [
                gettext('Generated Date'),
                'INPUT',
                [
                    'name'  => 'invoice_date[0]',
                    '',
                    'size'  => '20',
                    'class' => "text field",
                    'id'    => 'invoice_date',
                ],
                '',
                'tOOL TIP',
                '',
                'invoice_date[invoice_date-date]',
            ],
            [
                '',
                'HIDDEN',
                'ajax_search',
                '1',
                '',
                '',
                '',
            ],
            [
                '',
                'HIDDEN',
                'advance_search',
                '1',
                '',
                '',
                '',
            ],
        ];
        $form ['button_search'] = [
            'name'    => 'action',
            'id'      => "user_invoice_search_btn",
            'content' => gettext('Search'),
            'value'   => 'save',
            'type'    => 'button',
            'class'   => 'btn btn-line-parrot pull-right',
        ];
        $form ['button_reset'] = [
            'name'    => 'action',
            'id'      => "id_reset",
            'content' => gettext('Clear'),
            'value'   => 'cancel',
            'type'    => 'reset',
            'class'   => 'btn btn-line-sky pull-right margin-x-10',
        ];

        return $form;
    }

    function build_user_charge_history()
    {
        $account_info = $accountinfo = $this->CI->session->userdata('accountinfo');
        $currency_id = $account_info ['currency_id'];
        $currency = $this->CI->common->get_field_name('currency', 'currency', $currency_id);
        $grid_field_arr = json_encode(
            [
                [
                    gettext("Created Date"),
                    "140",
                    "created_date",
                    "",
                    "",
                    "",
                ],
                [
                    gettext("Invoice Number"),
                    "120",
                    "created_date",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Charge Type"),
                    "100",
                    "item_type",
                    "",
                    "",
                    "",
                ],
                [
                    gettext("Before Balance")."<br/>($currency)",
                    "120",
                    "before_balance",
                    "before_balance",
                    "before_balance",
                    "convert_to_currency",
                    "",
                    "true",
                    "right",
                ],
                [
                    gettext("Debit")."<br/>($currency)",
                    "120",
                    "debit",
                    "debit",
                    "debit",
                    "convert_to_currency",
                    "",
                    "true",
                    "right",
                ],
                [
                    gettext("Credit")."<br/>($currency)",
                    "120",
                    "credit",
                    "credit",
                    "credit",
                    "convert_to_currency",
                    "",
                    "true",
                    "right",
                ],
                [
                    gettext("After Balance")."<br/>($currency)",
                    "120",
                    "after_balance",
                    "after_balance",
                    "after_balance",
                    "convert_to_currency",
                    "",
                    "true",
                    "right",
                ],
                [
                    gettext("Description"),
                    "180",
                    "description",
                    "",
                    "",
                    "",
                ],
            ]
        );

        return $grid_field_arr;
    }

    function build_user_charge_history_search()
    {
        $form ['forms'] = [
            "",
            [
                'id' => "user_charge_history_search",
            ],
        ];
        $form ['Search'] = [
            [
                gettext('From Date'),
                'INPUT',
                [
                    'name'  => 'created_date[]',
                    'id'    => 'charge_from_date',
                    'size'  => '20',
                    'class' => "text field ",
                ],
                '',
                'tOOL TIP',
                '',
                'start_date[start_date-date]',
            ],
            [
                gettext('To Date'),
                'INPUT',
                [
                    'name'  => 'created_date[]',
                    'id'    => 'charge_to_date',
                    'size'  => '20',
                    'class' => "text field ",
                ],
                '',
                'tOOL TIP',
                '',
                'end_date[end_date-date]',
            ],
            [
                gettext('Debit '),
                'INPUT',
                [
                    'name'  => 'debit[debit]',
                    'value' => '',
                    'size'  => '20',
                    'class' => "text field ",
                ],
                '',
                'Tool tips info',
                '1',
                'debit[debit-integer]',
                '',
                '',
                '',
                'search_int_type',
                '',
            ],
            [
                gettext('Credit '),
                'INPUT',
                [
                    'name'  => 'credit[credit]',
                    'value' => '',
                    'size'  => '20',
                    'class' => "text field ",
                ],
                '',
                'Tool tips info',
                '1',
                'credit[credit-integer]',
                '',
                '',
                '',
                'search_int_type',
                '',
            ],
            [
                '',
                'HIDDEN',
                'ajax_search',
                '1',
                '',
                '',
                '',
            ],
            [
                '',
                'HIDDEN',
                'advance_search',
                '1',
                '',
                '',
                '',
            ],
        ];
        $form ['button_search'] = [
            'name'    => 'action',
            'id'      => "charges_search_btn",
            'content' => gettext('Search'),
            'value'   => 'save',
            'type'    => 'button',
            'class'   => 'btn btn-line-parrot pull-right',
        ];
        $form ['button_reset'] = [
            'name'    => 'action',
            'id'      => "id_reset",
            'content' => gettext('Clear'),
            'value'   => 'cancel',
            'type'    => 'reset',
            'class'   => 'btn btn-line-sky pull-right margin-x-10',
        ];

        return $form;
    }

    function build_user_subscription()
    {
        $account_info = $accountinfo = $this->CI->session->userdata('accountinfo');
        $currency_id = $account_info ['currency_id'];
        $currency = $this->CI->common->get_field_name('currency', 'currency', $currency_id);
        $grid_field_arr = json_encode(
            [
                [
                    gettext("Name"),
                    "335",
                    "description",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Amount")."($currency)",
                    "335",
                    "charge",
                    "charge",
                    "charge",
                    "convert_to_currency",
                    "",
                    "true",
                    "right",
                ],
                [
                    gettext("Billing Cycle"),
                    "335",
                    "sweep_id",
                    "sweep",
                    "sweeplist",
                    "get_field_name",
                    "",
                    "true",
                    "center",
                ],
            ]
        );

        return $grid_field_arr;
    }

    function build_user_subscription_search()
    {
        $accountinfo = $this->CI->session->userdata("accountinfo");
        $form ['forms'] = [
            "",
            [
                'id' => "user_subscription_search",
            ],
        ];
        $form ['Search'] = [
            [
                gettext('Name'),
                'INPUT',
                [
                    'name'  => 'description[description]',
                    '',
                    'size'  => '20',
                    'class' => "text field",
                ],
                '',
                'tOOL TIP',
                '1',
                'description[description-string]',
                '',
                '',
                '',
                'search_string_type',
                '',
            ],
            [
                gettext('Amount'),
                'INPUT',
                [
                    'name'  => 'charge[charge]',
                    'value' => '',
                    'size'  => '20',
                    'class' => "text field",
                ],
                '',
                'Tool tips info',
                '1',
                'charge[charge-integer]',
                '',
                '',
                '',
                'search_int_type',
                '',
            ],
            [
                gettext('Bill Cycle'),
                'sweep_id',
                'SELECT',
                '',
                '',
                'tOOL TIP',
                'Please Enter account number',
                'id',
                'sweep',
                'sweeplist',
                'build_dropdown',
                '',
                '',
            ],
            [
                '',
                'HIDDEN',
                'ajax_search',
                '1',
                '',
                '',
                '',
            ],
            [
                '',
                'HIDDEN',
                'advance_search',
                '1',
                '',
                '',
                '',
            ],
        ];
        $form ['button_search'] = [
            'name'    => 'action',
            'id'      => "user_subscriptions_button",
            'content' => gettext('Search'),
            'value'   => 'save',
            'type'    => 'button',
            'class'   => 'btn btn-line-parrot pull-right',
        ];
        $form ['button_reset'] = [
            'name'    => 'action',
            'id'      => "id_reset",
            'content' => gettext('Clear'),
            'value'   => 'cancel',
            'type'    => 'reset',
            'class'   => 'btn btn-line-sky pull-right margin-x-10',
        ];

        return $form;
    }

    function build_user_didlist()
    {
        $account_info = $accountinfo = $this->CI->session->userdata('accountinfo');
        $currency_id = $account_info ['currency_id'];
        $currency = $this->CI->common->get_field_name('currency', 'currency', $currency_id);
        $grid_field_arr = json_encode(
            [
                [
                    gettext("DID"),
                    "105",
                    "number",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Country"),
                    "90",
                    "country_id",
                    "country",
                    "countrycode",
                    "get_field_name",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Per Minute<br/>Cost")."($currency)",
                    "90",
                    "cost",
                    "cost",
                    "cost",
                    "convert_to_currency",
                    "",
                    "true",
                    "right",
                ],
                [
                    gettext("Initial<br/>Increment"),
                    "100",
                    "init_inc",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Increment"),
                    "100",
                    "inc",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Setup<br/>Fee")."($currency)",
                    "100",
                    "setup",
                    "setup",
                    "setup",
                    "convert_to_currency",
                    "",
                    "true",
                    "right",
                ],
                [
                    gettext("Monthly<br/>Fee")."($currency)",
                    "100",
                    "monthlycost",
                    "monthlycost",
                    "monthlycost",
                    "convert_to_currency",
                    "",
                    "true",
                    "right",
                ],
                [
                    gettext("Call Type"),
                    "105",
                    "call_type",
                    "call_type",
                    "call_type",
                    "get_call_type",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Destination"),
                    "153",
                    "extensions",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Status"),
                    "90",
                    "status",
                    "status",
                    "dids",
                    "get_status",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Modified <br/>Date"),
                    "100",
                    "last_modified_date",
                    "last_modified_date",
                    "last_modified_date",
                    "convert_GMT_to",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Action"),
                    "80",
                    "",
                    "",
                    "",
                    [
                        "EDIT"   => [
                            "url"  => "/user/user_did_edit/",
                            "mode" => "popup",
                        ],
                        "DELETE" => [
                            "url"  => "/user/user_dids_action/delete/",
                            "mode" => "single",
                        ],
                    ],
                ],
            ]
        );

        return $grid_field_arr;
    }

    function build_user_didlist_search()
    {
        $form ['forms'] = [
            "",
            [
                'id' => "user_did_search",
            ],
        ];
        $form ['Search'] = [
            [
                gettext('DID'),
                'INPUT',
                [
                    'name'  => 'number[number]',
                    '',
                    'size'  => '20',
                    'class' => "text field",
                ],
                '',
                'tOOL TIP',
                '1',
                'number[number-string]',
                '',
                '',
                '',
                'search_string_type',
                '',
            ],
            [
                gettext('Country'),
                'country_id',
                'SELECT',
                '',
                '',
                'tOOL TIP',
                'Please Enter account number',
                'id',
                'country',
                'countrycode',
                'build_dropdown',
                '',
                '',
            ],
            [
                gettext('Initial Increment'),
                'INPUT',
                [
                    'name'  => 'init_inc[init_inc]',
                    '',
                    'size'  => '20',
                    'class' => "text field ",
                ],
                '',
                'tOOL TIP',
                '1',
                'init_inc[init_inc-integer]',
                '',
                '',
                '',
                'search_int_type',
                '',
            ],
            [
                gettext('Call Type'),
                'call_type',
                'SELECT',
                '',
                '',
                'tOOL TIP',
                '',
                '',
                '',
                '',
                'set_call_type_search',
                '',
                '',
            ],
            [
                gettext('Destination'),
                'INPUT',
                [
                    'name'  => 'extensions[extensions]',
                    '',
                    'size'  => '20',
                    'class' => "text field",
                ],
                '',
                'tOOL TIP',
                '1',
                'extensions[extensions-string]',
                '',
                '',
                '',
                'search_string_type',
                '',
            ],

            [
                gettext('Status'),
                'status',
                'SELECT',
                '',
                '',
                'tOOL TIP',
                'Please Enter account number',
                '',
                '',
                '',
                'set_search_status',
                '',
                '',
            ],
            [
                '',
                'HIDDEN',
                'ajax_search',
                '1',
                '',
                '',
                '',
            ],
            [
                '',
                'HIDDEN',
                'advance_search',
                '1',
                '',
                '',
                '',
            ],
        ];

        $form ['button_search'] = [
            'name'    => 'action',
            'id'      => "user_did_search_btn",
            'content' => gettext('Search'),
            'value'   => 'save',
            'type'    => 'button',
            'class'   => 'btn btn-line-parrot pull-right',
        ];
        $form ['button_reset'] = [
            'name'    => 'action',
            'id'      => "id_reset",
            'content' => gettext('Clear'),
            'value'   => 'cancel',
            'type'    => 'reset',
            'class'   => 'btn btn-line-sky pull-right margin-x-10',
        ];

        return $form;
    }

    function build_user_ipmap()
    {
        $grid_field_arr = json_encode(
            [
                [
                    gettext("Name"),
                    "240",
                    "name",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("IP"),
                    "240",
                    "ip",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Prefix"),
                    "220",
                    "prefix",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Created Date"),
                    "174",
                    "created_date",
                    "created_date",
                    "created_date",
                    "convert_GMT_to",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Action"),
                    "150",
                    "",
                    "",
                    "",
                    [
                        "DELETE" => [
                            "url"  => "user/user_ipmap_action/delete/",
                            "mode" => "single",
                        ],
                    ],
                ],
            ]
        );

        return $grid_field_arr;
    }

    function build_user_ipmap_search()
    {
        $form ['forms'] = [
            "",
            [
                'id' => "user_ipmap_search",
            ],
        ];
        $form ['Search'] = [
            [
                gettext('Name'),
                'INPUT',
                [
                    'name'  => 'name[name]',
                    '',
                    'size'  => '20',
                    'class' => "text field",
                ],
                '',
                'tOOL TIP',
                '1',
                'name[name-string]',
                '',
                '',
                '',
                'search_string_type',
                '',
            ],
            [
                gettext('IP'),
                'INPUT',
                [
                    'name'  => 'ip[ip]',
                    '',
                    'size'  => '20',
                    'class' => "text field",
                ],
                '',
                'tOOL TIP',
                '1',
                'ip[ip-string]',
                '',
                '',
                '',
                'search_string_type',
                '',
            ],
            [
                gettext('Prefix'),
                'INPUT',
                [
                    'name'  => 'prefix[prefix]',
                    '',
                    'size'  => '20',
                    'class' => "text field",
                ],
                '',
                'tOOL TIP',
                '1',
                'prefix[prefix-string]',
                '',
                '',
                '',
                'search_string_type',
                '',
            ],
            [
                '',
                'HIDDEN',
                'ajax_search',
                '1',
                '',
                '',
                '',
            ],
            [
                '',
                'HIDDEN',
                'advance_search',
                '1',
                '',
                '',
                '',
            ],
        ];
        $form ['button_search'] = [
            'name'    => 'action',
            'id'      => "user_ipmap_search_btn",
            'content' => gettext('Search'),
            'value'   => 'save',
            'type'    => 'button',
            'class'   => 'btn btn-line-parrot pull-right',
        ];
        $form ['button_reset'] = [
            'name'    => 'action',
            'id'      => "id_reset",
            'content' => gettext('Clear'),
            'value'   => 'cancel',
            'type'    => 'reset',
            'class'   => "btn btn-line-sky pull-right margin-x-10",
        ];

        return $form;
    }

    function build_user_sipdevices()
    {
        $grid_field_arr = json_encode(
            [
                [
                    "<input type='checkbox' name='chkAll' class='ace checkall'/><label class='lbl'></label>",
                    "30",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "false",
                    "center",
                ],
                [
                    gettext("User Name"),
                    "105",
                    "username",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Password"),
                    "105",
                    "password",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Caller Name"),
                    "110",
                    "effective_caller_id_name",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Caller Number"),
                    "110",
                    "effective_caller_id_number",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Status"),
                    "125",
                    "status",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Created Date"),
                    "110",
                    "creation_date",
                    "creation_date",
                    "creation_date",
                    "convert_GMT_to",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Modified Date"),
                    "130",
                    "last_modified_date",
                    "last_modified_date",
                    "last_modified_date",
                    "convert_GMT_to",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Voicemail"),
                    "90",
                    "voicemail_enabled",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Action"),
                    "110",
                    "",
                    "",
                    "",
                    [
                        "EDIT"   => [
                            "url"    => "/accounts/fssipdevices_action/edit/",
                            "mode"   => "single",
                            "layout" => "medium",
                        ],
                        "DELETE" => [
                            "url"  => "/accounts/fssipdevices_action/delete/",
                            "mode" => "single",
                        ],
                    ],
                ],
            ]
        );

        return $grid_field_arr;
    }

    function build_user_sipdevices_search()
    {
        $form ['forms'] = [
            "",
            [
                'id' => "user_sipdevices_search",
            ],
        ];
        $form ['Search'] = [
            [
                gettext('Username'),
                'INPUT',
                [
                    'name'  => 'username[username]',
                    '',
                    'size'  => '20',
                    'class' => "text field",
                ],
                '',
                'tOOL TIP',
                '1',
                'username[username-string]',
                '',
                '',
                '',
                'search_string_type',
                '',
            ],
            [
                '',
                'HIDDEN',
                'ajax_search',
                '1',
                '',
                '',
                '',
            ],
            [
                '',
                'HIDDEN',
                'advance_search',
                '1',
                '',
                '',
                '',
            ],
        ];
        $form ['button_search'] = [
            'name'    => 'action',
            'id'      => "user_sipdevices_search_btn",
            'content' => gettext('Search'),
            'value'   => 'save',
            'type'    => 'button',
            'class'   => 'btn btn-line-parrot pull-right',
        ];
        $form ['button_reset'] = [
            'name'    => 'action',
            'id'      => "id_reset",
            'content' => gettext('Clear'),
            'value'   => 'cancel',
            'type'    => 'reset',
            'class'   => 'btn btn-line-sky pull-right margin-x-10',
        ];

        return $form;
    }

    function build_user_sipdevices_form($id = '')
    {
        $val = $id > 0 ? 'sip_devices.username.'.$id : 'sip_devices.username';
        $uname_user = $this->CI->common->find_uniq_rendno('10', '', '');
        $password = $this->CI->common->generate_password();
        $form ['forms'] = [
            base_url().'user/user_sipdevices_save/',
            [
                "id"   => "user_sipdevices_form",
                "name" => "user_sipdevices_form",
            ],
        ];
        $form [gettext('Device Information')] = [
            [
                '',
                'HIDDEN',
                [
                    'name' => 'id',
                ],
                '',
                '',
                '',
                '',
            ],
            [
                gettext('Username'),
                'INPUT',
                [
                    'name'  => 'fs_username',
                    'size'  => '20',
                    'value' => $uname_user,
                    'id'    => 'username',
                    'class' => "text field medium",
                ],
                'trim|required|is_unique['.$val.']|xss_clean',
                'tOOL TIP',
                'Please Enter account number',
                '<i style="color: #1BCB61;font-size: 14px;padding-left: 5px;padding-top: 8px;float: left;" title="Reset Password" class="change_number  fa fa-refresh"></i>',
            ],
            [
                gettext('Password'),
                'INPUT',
                [
                    'name'  => 'fs_password',
                    'size'  => '20',
                    'value' => $password,
                    'id'    => 'password',
                    'class' => "text field medium",
                ],
                'trim|required|xss_clean',
                'tOOL TIP',
                'Please Enter Password',
                '<i style="color: #1BCB61;font-size: 14px;padding-left: 5px;padding-top: 8px;float: left;" title="Reset Password" class="change_pass fa fa-refresh"></i>',
            ],
            [
                gettext('Caller Name'),
                'INPUT',
                [
                    'name'  => 'effective_caller_id_name',
                    'size'  => '20',
                    'class' => "text field medium",
                ],
                '',
                'tOOL TIP',
                'Please Enter account number',
            ],
            [
                gettext('Caller Number'),
                'INPUT',
                [
                    'name'  => 'effective_caller_id_number',
                    'size'  => '20',
                    'class' => "text field medium",
                ],
                '',
                'tOOL TIP',
                'Please Enter account number',
            ],
            [
                gettext('Status'),
                'status',
                'SELECT',
                '',
                '',
                'tOOL TIP',
                'Please Select Status',
                '',
                '',
                '',
                'set_status',
            ],
        ];

        $form [gettext('Voicemail Options')] = [
            [
                gettext('Enable'),
                'voicemail_enabled',
                'SELECT',
                '',
                '',
                'tOOL TIP',
                'Please Select Status',
                '',
                '',
                '',
                'set_sip_config_option',
            ],
            [
                gettext('Password'),
                'INPUT',
                [
                    'name'  => 'voicemail_password',
                    'size'  => '20',
                    'class' => "text field medium",
                ],
                '',
                'tOOL TIP',
                'Please Enter account number',
            ],
            [
                gettext('Mail To'),
                'INPUT',
                [
                    'name'  => 'voicemail_mail_to',
                    'size'  => '20',
                    'class' => "text field medium",
                ],
                '',
                'tOOL TIP',
                'Please Enter account number',
            ],
            [
                gettext('Attach File'),
                'voicemail_attach_file',
                'SELECT',
                '',
                '',
                'tOOL TIP',
                'Please Select Status',
                '',
                '',
                '',
                'set_sip_config_option',
            ],
            [
                gettext('Local After Email'),
                'vm_keep_local_after_email',
                'SELECT',
                '',
                '',
                'tOOL TIP',
                'Please Select Status',
                '',
                '',
                '',
                'set_sip_config_option',
            ],
            [
                gettext('Send all Message'),
                'vm_send_all_message',
                'SELECT',
                '',
                '',
                'tOOL TIP',
                'Please Select Status',
                '',
                '',
                '',
                'set_sip_config_option',
            ],
        ];
        $form ['button_cancel'] = [
            'name'    => 'action',
            'content' => 'Close',
            'value'   => 'cancel',
            'type'    => 'button',
            'class'   => 'btn btn-line-sky margin-x-10',
            'onclick' => 'return redirect_page(\'NULL\')',
        ];
        $form ['button_save'] = [
            'name'    => 'action',
            'content' => 'Save',
            'value'   => 'save',
            'id'      => 'submit',
            'type'    => 'button',
            'class'   => 'btn btn-line-parrot',
        ];

        return $form;
    }

    function build_user_animap()
    {
        $grid_field_arr = json_encode(
            [
                [
                    gettext("Caller ID"),
                    "735",
                    "number",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Action"),
                    "275",
                    "",
                    "",
                    "",
                    [
                        "DELETE" => [
                            "url"  => "user/user_animap_action/delete/",
                            "mode" => "single",
                        ],
                    ],
                ],
            ]
        );

        return $grid_field_arr;
    }

    function user_rates_list_buttons()
    {
        $buttons_json = json_encode(
            [
                [
                    gettext("Export CSV"),
                    "btn btn-xing",
                    "fa fa-download fa-lg",
                    "button_action",
                    "/user/user_rates_list_export/",
                    'single',
                ],
            ]
        );

        return $buttons_json;
    }

    function user_rates_list()
    {
        $account_info = $accountinfo = $this->CI->session->userdata('accountinfo');
        $currency_id = $account_info ['currency_id'];
        $currency = $this->CI->common->get_field_name('currency', 'currency', $currency_id);
        $grid_field_arr = json_encode(
            [
                [
                    gettext("Code"),
                    "155",
                    "pattern",
                    "pattern",
                    "",
                    "get_only_numeric_val",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Destination"),
                    "200",
                    "comment",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Connect Cost")."($currency)",
                    "200",
                    "connectcost",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "right",
                ],
                [
                    gettext("Included Seconds"),
                    "200",
                    "includedseconds",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Per Minute Cost")."($currency)",
                    "200",
                    "cost",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "right",
                ],
                [
                    gettext("Initial Increment"),
                    "130",
                    "init_inc",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Increment"),
                    "180",
                    "inc",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "center",
                ],
            ]
        );

        return $grid_field_arr;
    }

    function user_rates_list_search()
    {
        $form ['forms'] = [
            "",
            [
                'id' => "user_rates_list_search",
            ],
        ];
        $form ['Search'] = [
            [
                gettext('Code'),
                'INPUT',
                [
                    'name'  => 'pattern[pattern]',
                    '',
                    'size'  => '20',
                    'class' => "text field ",
                ],
                '',
                'tOOL TIP',
                '1',
                'pattern[pattern-string]',
                '',
                '',
                '',
                'search_int_type',
                '',
            ],
            [
                gettext('Destination'),
                'INPUT',
                [
                    'name'  => 'comment[comment]',
                    '',
                    'size'  => '20',
                    'class' => "text field ",
                ],
                '',
                'tOOL TIP',
                '1',
                'comment[comment-string]',
                '',
                '',
                '',
                'search_string_type',
                '',
            ],
            [
                gettext('Connect Cost'),
                'INPUT',
                [
                    'name'  => 'connectcost[connectcost]',
                    'value' => '',
                    'size'  => '20',
                    'class' => "text field ",
                ],
                '',
                'Tool tips info',
                '1',
                'connectcost[connectcost-integer]',
                '',
                '',
                '',
                'search_int_type',
                '',
            ],
            [
                gettext('Included Seconds'),
                'INPUT',
                [
                    'name'  => 'includedseconds[includedseconds]',
                    'value' => '',
                    'size'  => '20',
                    'class' => "text field ",
                ],
                '',
                'Tool tips info',
                '1',
                'includedseconds[includedseconds-integer]',
                '',
                '',
                '',
                'search_int_type',
                '',
            ],
            [
                gettext('Per Minute Cost'),
                'INPUT',
                [
                    'name'  => 'cost[cost]',
                    'value' => '',
                    'size'  => '20',
                    'class' => "text field ",
                ],
                '',
                'Tool tips info',
                '1',
                'cost[cost-integer]',
                '',
                '',
                '',
                'search_int_type',
                '',
            ],
            [
                gettext('Initial Increment'),
                'INPUT',
                [
                    'name'  => 'init_inc[init_inc]',
                    '',
                    'size'  => '20',
                    'class' => "text field ",
                ],
                '',
                'tOOL TIP',
                '1',
                'init_inc[init_inc-integer]',
                '',
                '',
                '',
                'search_int_type',
                '',
            ],
            [
                gettext('Increment'),
                'INPUT',
                [
                    'name'  => 'inc[inc]',
                    '',
                    'size'  => '20',
                    'class' => "text field ",
                ],
                '',
                'tOOL TIP',
                '1',
                'inc[inc-integer]',
                '',
                '',
                '',
                'search_int_type',
                '',
            ],
            [
                '',
                'HIDDEN',
                'ajax_search',
                '1',
                '',
                '',
                '',
            ],
            [
                '',
                'HIDDEN',
                'advance_search',
                '1',
                '',
                '',
                '',
            ],
        ];

        $form ['button_search'] = [
            'name'    => 'action',
            'id'      => "user_rates_list_search_btn",
            'content' => gettext('Search'),
            'value'   => 'save',
            'type'    => 'button',
            'class'   => 'btn btn-line-parrot pull-right',
        ];
        $form ['button_reset'] = [
            'name'    => 'action',
            'id'      => "id_reset",
            'content' => gettext('Clear'),
            'value'   => 'cancel',
            'type'    => 'reset',
            'class'   => 'btn btn-line-sky pull-right margin-x-10',
        ];

        return $form;
    }

    function user_alert_threshold()
    {
        $form ['forms'] = [
            base_url().'user/user_alert_threshold/',
            [
                "id"   => "customer_alert_threshold",
                "name" => "customer_alert_threshold",
            ],
        ];
        $form [gettext('Low Balance Alert Email')] = [
            [
                '',
                'HIDDEN',
                [
                    'name' => 'id',
                ],
                '',
                '',
                '',
                '',
            ],
            [
                gettext('Enable Email Alerts ?'),
                'notify_flag',
                'SELECT',
                '',
                '',
                'tOOL TIP',
                '',
                '',
                '',
                '',
                'custom_status',
            ],
            [
                gettext('Low Balance Alert Level'),
                'INPUT',
                [
                    'name'  => 'notify_credit_limit',
                    'size'  => '20',
                    'class' => "text field medium",
                ],
                'currency_decimal',
                'tOOL TIP',
                '',
            ],
            [
                gettext('Email Address'),
                'INPUT',
                [
                    'name'  => 'notify_email',
                    'size'  => '50',
                    'class' => "text field medium",
                ],
                'valid_email',
                'tOOL TIP',
                '',
            ],
        ];
        $form ['button_save'] = [
            'name'    => 'action',
            'content' => gettext('Save'),
            'value'   => 'save',
            'type'    => 'submit',
            'class'   => 'btn btn-line-parrot',
        ];

        return $form;
    }

    function build_cdrs_report($type)
    {
        $account_info = $accountinfo = $this->CI->session->userdata('accountinfo');
        $currency_id = $account_info ['currency_id'];
        $currency = $this->CI->common->get_field_name('currency', 'currency', $currency_id);

        if ($type == '0' || $type == '1') {
            $cost_array = [
                gettext("Debit")."($currency)",
                "140",
                "debit",
                "debit",
                "debit",
                "convert_to_currency",
                "",
                "true",
                "right",
            ];
        }
        if ($type == '3') {
            $cost_array = [
                gettext("Debit")."($currency)",
                "140",
                "cost",
                "cost",
                "cost",
                "convert_to_currency",
                "",
                "true",
                "right",
            ];
        }
        $grid_field_arr = json_encode(
            [
                [
                    gettext("Date"),
                    "170",
                    "callstart",
                    "callstart",
                    "callstart",
                    "convert_GMT_to",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Caller ID"),
                    "110",
                    "callerid",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Called Number"),
                    "160",
                    "callednum",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Destination"),
                    "160",
                    "notes",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Duration"),
                    "140",
                    "billseconds",
                    "user_cdrs_report_search",
                    "billseconds",
                    "convert_to_show_in",
                    "",
                    "true",
                    "center",
                ],
                $cost_array,
                [
                    gettext("Disposition"),
                    "160",
                    "disposition",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Call Type"),
                    "233",
                    "calltype",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "center",
                ],
            ]
        );

        return $grid_field_arr;
    }

    function build_cdrs_report_search($type)
    {
        if ($type == '0' || $type == '1') {
            $cost_array = [
                gettext('Debit'),
                'INPUT',
                [
                    'name'  => 'debit[debit]',
                    'value' => '',
                    'size'  => '20',
                    'class' => "text field ",
                ],
                '',
                'Tool tips info',
                '1',
                'debit[debit-integer]',
                '',
                '',
                '',
                'search_int_type',
                '',
            ];
        }
        if ($type == '3') {
            $cost_array = [
                gettext('Debit'),
                'INPUT',
                [
                    'name'  => 'cost[cost]',
                    'value' => '',
                    'size'  => '20',
                    'class' => "text field ",
                ],
                '',
                'Tool tips info',
                '1',
                'cost[cost-integer]',
                '',
                '',
                '',
                'search_int_type',
                '',
            ];
        }
        $form ['forms'] = [
            "",
            [
                'id' => "user_cdrs_report_search",
            ],
        ];
        $form ['Search'] = [
            [
                gettext('From Date'),
                'INPUT',
                [
                    'name'  => 'callstart[]',
                    'id'    => 'customer_cdr_from_date',
                    'size'  => '20',
                    'class' => "text field ",
                ],
                '',
                'tOOL TIP',
                '',
                'start_date[start_date-date]',
            ],
            [
                gettext('To Date'),
                'INPUT',
                [
                    'name'  => 'callstart[]',
                    'id'    => 'customer_cdr_to_date',
                    'size'  => '20',
                    'class' => "text field ",
                ],
                '',
                'tOOL TIP',
                '',
                'end_date[end_date-date]',
            ],
            [
                gettext('Caller ID'),
                'INPUT',
                [
                    'name'  => 'callerid[callerid]',
                    '',
                    'id'    => 'first_name',
                    'size'  => '15',
                    'class' => "text field ",
                ],
                '',
                'tOOL TIP',
                '1',
                'callerid[callerid-string]',
                '',
                '',
                '',
                'search_string_type',
                '',
            ],
            [
                gettext('Called Number'),
                'INPUT',
                [
                    'name'  => 'callednum[callednum]',
                    'value' => '',
                    'size'  => '20',
                    'class' => "text field ",
                ],
                '',
                'Tool tips info',
                '1',
                'callednum[callednum-string]',
                '',
                '',
                '',
                'search_string_type',
                '',
            ],
            [
                gettext('Destination'),
                'INPUT',
                [
                    'name'  => 'notes[notes]',
                    'value' => '',
                    'size'  => '20',
                    'class' => "text field ",
                ],
                '',
                'Tool tips info',
                '1',
                'notes[notes-integer]',
                '',
                '',
                '',
                'search_int_type',
                '',
            ],
            [
                gettext('Duration'),
                'INPUT',
                [
                    'name'  => 'billseconds[billseconds]',
                    'value' => '',
                    'size'  => '20',
                    'class' => "text field ",
                ],
                '',
                'Tool tips info',
                '1',
                'billseconds[billseconds-integer]',
                '',
                '',
                '',
                'search_int_type',
                '',
            ],
            $cost_array,
            [
                gettext('Disposition'),
                'disposition',
                'SELECT',
                '',
                '',
                'tOOL TIP',
                'Please Enter account number',
                '',
                '',
                '',
                'set_despostion',
            ],
            [
                gettext('Call Type'),
                'calltype',
                'SELECT',
                '',
                '',
                'tOOL TIP',
                'Please Enter account number',
                '',
                '',
                '',
                'set_calltype',
            ],
            [
                '',
                'HIDDEN',
                'ajax_search',
                '1',
                '',
                '',
                '',
            ],
            [
                '',
                'HIDDEN',
                'advance_search',
                '1',
                '',
                '',
                '',
            ],
        ];
        $form ['display_in'] = [
            'name'           => 'search_in',
            "id"             => "search_in",
            "function"       => "search_report_in",
            "content"        => gettext("Display records in"),
            'label_class'    => "search_label col-md-3 no-padding",
            "dropdown_class" => "form-control",
            "label_style"    => "font-size:13px;",
            "dropdown_style" => "background: #ddd; width: 21% !important;",
        ];
        $form ['button_search'] = [
            'name'    => 'action',
            'id'      => "user_cdr_search_btn",
            'content' => gettext('Search'),
            'value'   => 'save',
            'type'    => 'button',
            'class'   => 'btn btn-line-parrot pull-right',
        ];
        $form ['button_reset'] = [
            'name'    => 'action',
            'id'      => "id_reset",
            'content' => gettext('Clear'),
            'value'   => 'cancel',
            'type'    => 'reset',
            'class'   => 'btn btn-line-sky pull-right margin-x-10',
        ];

        return $form;
    }

    function build_cdrs_report_buttons()
    {
        $buttons_json = json_encode(
            [
                [
                    gettext("Export"),
                    "btn btn-xing",
                    " fa fa-download fa-lg",
                    "button_action",
                    "/user/user_report_export/",
                    'single',
                ],
            ]
        );

        return $buttons_json;
    }

    function build_user_refill_report()
    {
        $account_info = $accountinfo = $this->CI->session->userdata('accountinfo');
        $currency_id = $account_info ['currency_id'];
        $currency = $this->CI->common->get_field_name('currency', 'currency', $currency_id);
        $grid_field_arr = json_encode(
            [
                [
                    gettext("Date"),
                    "220",
                    "payment_date",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Amount")."($currency)",
                    "220",
                    "credit",
                    "credit",
                    "credit",
                    "convert_to_currency",
                    "",
                    "true",
                    "right",
                ],
                [
                    gettext("Refill By"),
                    "270",
                    "payment_by",
                    "payment_by",
                    "payment_by",
                    "get_refill_by",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Note"),
                    "300",
                    "notes",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "center",
                ],
            ]
        );

        return $grid_field_arr;
    }

    function build_user_refill_report_search()
    {
        $form ['forms'] = [
            "",
            [
                'id' => "user_refill_report_search",
            ],
        ];
        $form ['Search'] = [
            [
                gettext('From Date'),
                'INPUT',
                [
                    'name'  => 'payment_date[]',
                    'id'    => 'customer_cdr_from_date',
                    'size'  => '20',
                    'class' => "text field ",
                ],
                '',
                'tOOL TIP',
                '',
                'payment_date[payment_date-date]',
            ],
            [
                gettext('To Date'),
                'INPUT',
                [
                    'name'  => 'payment_date[]',
                    'id'    => 'customer_cdr_to_date',
                    'size'  => '20',
                    'class' => "text field ",
                ],
                '',
                'tOOL TIP',
                '',
                'payment_date[payment_date-date]',
            ],
            [
                gettext('Amount'),
                'INPUT',
                [
                    'name'  => 'credit[credit]',
                    'value' => '',
                    'size'  => '20',
                    'class' => "text field",
                ],
                '',
                'Tool tips info',
                '1',
                'credit[credit-integer]',
                '',
                '',
                '',
                'search_int_type',
                '',
            ],
            [
                '',
                'HIDDEN',
                'ajax_search',
                '1',
                '',
                '',
                '',
            ],
            [
                '',
                'HIDDEN',
                'advance_search',
                '1',
                '',
                '',
                '',
            ],
        ];

        $form ['button_search'] = [
            'name'    => 'action',
            'id'      => "user_refill_report_search_btn",
            'content' => gettext('Search'),
            'value'   => 'save',
            'type'    => 'button',
            'class'   => 'btn btn-line-parrot pull-right',
        ];
        $form ['button_reset'] = [
            'name'    => 'action',
            'id'      => "id_reset",
            'content' => gettext('Clear'),
            'value'   => 'cancel',
            'type'    => 'reset',
            'class'   => 'btn btn-line-sky pull-right margin-x-10',
        ];

        return $form;
    }

    function build_user_fund_transfer_form($number, $currency_id, $id)
    {
        $form ['forms'] = [
            base_url().'user/user_fund_transfer_save/',
            [
                'id'     => 'user_fund_transfer_form',
                'method' => 'POST',
                'class'  => 'build_user_fund_transfer_frm',
                'name'   => 'user_fund_transfer_form',
            ],
        ];
        $form [gettext('Fund Transfer')] = [
            [
                '',
                'HIDDEN',
                [
                    'name'  => 'id',
                    'value' => $id,
                ],
                '',
                '',
                '',
                '',
            ],
            [
                '',
                'HIDDEN',
                [
                    'name'  => 'account_currency',
                    'value' => $currency_id,
                ],
                '',
                '',
                '',
            ],
            [
                gettext('From Account'),
                'INPUT',
                [
                    'name'     => 'fromaccountid',
                    'size'     => '20',
                    'value'    => $number,
                    'readonly' => true,
                    'class'    => "text field medium",
                ],
                'required',
                'tOOL TIP',
                'Please Enter account number',
            ],
            [
                gettext('To Account'),
                'INPUT',
                [
                    'name'  => 'toaccountid',
                    'size'  => '20',
                    'class' => "text field medium",
                ],
                'trim|required|numeric',
                'tOOL TIP',
                'Please Enter to account number',
            ],
            [
                gettext('Amount'),
                'INPUT',
                [
                    'name'  => 'credit',
                    'size'  => '20',
                    'class' => "text field medium",
                ],
                'trim|required|numeric',
                'tOOL TIP',
                '',
            ],
            [
                gettext('Note'),
                'TEXTAREA',
                [
                    'name'  => 'notes',
                    'size'  => '20',
                    'cols'  => '63',
                    'rows'  => '5',
                    'class' => "form-control col-md-5  text field medium",
                    'style' => 'height: 80px;',
                ],
                '',
                'tOOL TIP',
                '',
            ],
        ];
        $form ['button_save'] = [
            'name'    => 'action',
            'content' => 'Transfer',
            'value'   => gettext('save'),
            'id'      => "submit",
            'type'    => 'submit',
            'class'   => 'btn btn-line-parrot',
        ];

        return $form;
    }

    function build_user_opensips_buttons()
    {
        $buttons_json = json_encode(
            [
                [
                    "Create",
                    "btn btn-line-warning btn",
                    "fa fa-plus-circle fa-lg",
                    "button_action",
                    "/user/user_opensips_add/",
                    "popup",
                ],
                [
                    "Delete",
                    "btn btn-line-danger",
                    "fa fa-times-circle fa-lg",
                    "button_action",
                    "/user/user_opensips_delete_multiple/",
                ],
            ]
        );

        return $buttons_json;
    }

    function build_user_opensips()
    {
        $grid_field_arr = json_encode(
            [
                [
                    "<input type='checkbox' name='chkAll' class='ace checkall'/><label class='lbl'></label>",
                    "30",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "false",
                    "center",
                ],
                [
                    "Username",
                    "240",
                    "username",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "center",
                ],
                [
                    "Password",
                    "240",
                    "password",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "center",
                ],
                [
                    "Domain",
                    "240",
                    "domain",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "center",
                ],
                [
                    "Action",
                    "200",
                    "",
                    "",
                    "",
                    [
                        "EDIT"   => [
                            "url"  => 'user/user_opensips_edit/',
                            "mode" => "popup",
                        ],
                        "DELETE" => [
                            "url"  => 'user/user_opensips_delete/',
                            "mode" => "popup",
                        ],
                    ],
                ],
            ]
        );

        return $grid_field_arr;
    }

    function build_user_opensips_form($id = false)
    {
        $val = $id > 0 ? 'subscriber.username.'.$id : 'subscriber.username';
        $uname_user = $this->CI->common->find_uniq_rendno('10', '', '');
        $password = $this->CI->common->generate_password();
        $accountinfo = $this->CI->session->userdata('accountinfo');
        $form ['forms'] = [
            base_url().'user/user_opensips_save/',
            [
                "id"   => "opensips_form",
                "name" => "opensips_form",
            ],
        ];
        $form ['Opensips Device'] = [
            [
                '',
                'HIDDEN',
                [
                    'name' => 'id',
                ],
                '',
                '',
                '',
                '',
            ],
            [
                '',
                'HIDDEN',
                [
                    'name'  => 'accountcode',
                    'value' => $accountinfo ['number'],
                ],
                '',
                '',
                '',
                '',
            ],
            [
                'Username',
                'INPUT',
                [
                    'name'  => 'username',
                    'size'  => '20',
                    'id'    => 'username',
                    'value' => $uname_user,
                    'class' => "text field medium",
                ],
                'trim|required|xss_clean',
                'tOOL TIP',
                'Please Enter account number',
                '<i style="cursor:pointer; font-size: 17px; padding-left:10px; padding-top:6px;" title="Reset Password" class="change_number fa fa-refresh"></i>',
            ],
            [
                'Password',
                'PASSWORD',
                [
                    'name'  => 'password',
                    'size'  => '20',
                    'id'    => 'password1',
                    'value' => $password,
                    'class' => "text field medium",
                ],
                'trim|required|xss_clean',
                'tOOL TIP',
                'Please Enter Password',
                '<i style="cursor:pointer; font-size: 17px; padding-left:10px; padding-top:6px;" title="Reset Password" class="change_pass fa fa-refresh"></i>',
            ],
            [
                'Domain',
                'INPUT',
                [
                    'name'  => 'domain',
                    'size'  => '20',
                    'class' => "text field medium",
                ],
                '',
                'tOOL TIP',
                'Please Enter account number',
            ],
            [
                'Status',
                'status',
                'SELECT',
                '',
                '',
                'tOOL TIP',
                'Please Select Status',
                '',
                '',
                '',
                'set_status',
            ],
        ];
        $form ['button_save'] = [
            'name'    => 'action',
            'content' => 'Save',
            'value'   => 'save',
            'type'    => 'button',
            'id'      => 'submit',
            'class'   => 'btn btn-line-parrot',
        ];
        $form ['button_cancel'] = [
            'name'    => 'action',
            'content' => 'Close',
            'value'   => 'cancel',
            'type'    => 'button',
            'class'   => 'btn btn-line-sky margin-x-10',
            'onclick' => 'return redirect_page(\'NULL\')',
        ];

        return $form;
    }

    function build_user_opensips_search()
    {
        $form ['forms'] = [
            "",
            [
                'id' => "opensips_list_search",
            ],
        ];
        $form ['Search'] = [
            [
                'Username',
                'INPUT',
                [
                    'name'  => 'username[username]',
                    '',
                    'size'  => '20',
                    'class' => "text field ",
                ],
                '',
                'tOOL TIP',
                '1',
                'username[username-string]',
                '',
                '',
                '',
                'search_string_type',
                '',
            ],
            [
                '',
                'HIDDEN',
                'ajax_search',
                '1',
                '',
                '',
                '',
            ],
            [
                '',
                'HIDDEN',
                'advance_search',
                '1',
                '',
                '',
                '',
            ],
        ];

        $form ['button_search'] = [
            'name'    => 'action',
            'id'      => "opensipsdevice_search_btn",
            'content' => 'Search',
            'value'   => 'save',
            'type'    => 'button',
            'class'   => 'btn btn-line-parrot pull-right',
        ];
        $form ['button_reset'] = [
            'name'    => 'action',
            'id'      => "id_reset",
            'content' => 'Clear',
            'value'   => 'cancel',
            'type'    => 'reset',
            'class'   => 'btn btn-line-sky pull-right margin-x-10',
        ];

        return $form;
    }

    function build_user_did_form()
    {
        $form ['forms'] = [
            base_url().'user/user_dids_action/edit/',
            [
                "id"   => "user_did_form",
                "name" => "user_did_form",
            ],
        ];
        $form ['Edit'] = [
            [
                '',
                'HIDDEN',
                [
                    'name' => 'free_didlist',
                ],
                '',
                '',
                '',
                '',
            ],
            [
                gettext('DID'),
                'INPUT',
                [
                    'name'     => 'number',
                    'size'     => '20',
                    'class'    => "text field medium",
                    "readonly" => "true",
                ],
                'trim|required|is_numeric|xss_clean|integer',
                'tOOL TIP',
                'Please Enter account number',
            ],
            [
                gettext('Call Type'),
                'call_type',
                'SELECT',
                '',
                '',
                'tOOL TIP',
                '',
                '',
                '',
                '',
                'set_call_type',
                '',
            ],
            [
                gettext('Destination'),
                'INPUT',
                [
                    'name'  => 'extensions',
                    'size'  => '20',
                    'class' => "text field medium",
                ],
                'trim|required|xss_clean',
                'tOOL TIP',
                'Please Enter Password',
            ],
        ];
        $form ['button_save'] = [
            'name'    => 'action',
            'content' => 'Save',
            'value'   => 'save',
            'type'    => 'button',
            'id'      => 'submit',
            'class'   => 'btn btn-line-parrot',
        ];

        return $form;
    }

    function build_provider_report_buttons()
    {
        $buttons_json = json_encode(
            [
                [
                    "Export",
                    "btn btn-xing",
                    " fa fa-download fa-lg",
                    "button_action",
                    "/user/user_provider_cdrreport_export/",
                    'single',
                ],
            ]
        );

        return $buttons_json;
    }

    function build_provider_report($type)
    {
        $account_info = $accountinfo = $this->CI->session->userdata('accountinfo');
        $currency_id = $account_info ['currency_id'];
        $currency = $this->CI->common->get_field_name('currency', 'currency', $currency_id);

        if ($type == '0' || $type == '1') {
            $cost_array = [
                "Debit($currency)",
                "140",
                "debit",
                "debit",
                "debit",
                "convert_to_currency",
                "",
                "true",
                "right",
            ];
        }
        if ($type == '3') {
            $cost_array = [
                "Cost($currency)",
                "140",
                "cost",
                "cost",
                "cost",
                "convert_to_currency",
                "",
                "true",
                "right",
            ];
        }
        $grid_field_arr = json_encode(
            [
                [
                    "Date",
                    "170",
                    "callstart",
                    "callstart",
                    "callstart",
                    "convert_GMT_to",
                    "",
                    "true",
                    "center",
                ],
                [
                    "Caller ID",
                    "110",
                    "callerid",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "center",
                ],
                [
                    "Called Number",
                    "160",
                    "callednum",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "center",
                ],
                [
                    "Destination",
                    "160",
                    "notes",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "center",
                ],
                [
                    "Duration",
                    "140",
                    "billseconds",
                    "user_provider_cdrs_report_search",
                    "billseconds",
                    "convert_to_show_in",
                    "",
                    "true",
                    "center",
                ],
                $cost_array,
                [
                    "Disposition",
                    "160",
                    "disposition",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "center",
                ],
                [
                    "Call Type",
                    "233",
                    "calltype",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "center",
                ],
            ]
        );

        return $grid_field_arr;
    }

    function build_provider_report_search($type)
    {
        $cost_array = [
            'Cost ',
            'INPUT',
            [
                'name'  => 'cost[cost]',
                'value' => '',
                'size'  => '20',
                'class' => "text field ",
            ],
            '',
            'Tool tips info',
            '1',
            'cost[cost-integer]',
            '',
            '',
            '',
            'search_int_type',
            '',
        ];
        $form ['forms'] = [
            "",
            [
                'id' => "user_provider_cdrs_report_search",
            ],
        ];
        $form ['Search'] = [
            [
                'From Date',
                'INPUT',
                [
                    'name'  => 'callstart[]',
                    'id'    => 'customer_cdr_from_date',
                    'size'  => '20',
                    'class' => "text field ",
                ],
                '',
                'tOOL TIP',
                '',
                'start_date[start_date-date]',
            ],
            [
                'To Date',
                'INPUT',
                [
                    'name'  => 'callstart[]',
                    'id'    => 'customer_cdr_to_date',
                    'size'  => '20',
                    'class' => "text field ",
                ],
                '',
                'tOOL TIP',
                '',
                'end_date[end_date-date]',
            ],
            [
                'Caller ID',
                'INPUT',
                [
                    'name'  => 'callerid[callerid]',
                    '',
                    'id'    => 'first_name',
                    'size'  => '15',
                    'class' => "text field ",
                ],
                '',
                'tOOL TIP',
                '1',
                'callerid[callerid-string]',
                '',
                '',
                '',
                'search_string_type',
                '',
            ],
            [
                'Called Number',
                'INPUT',
                [
                    'name'  => 'callednum[callednum]',
                    'value' => '',
                    'size'  => '20',
                    'class' => "text field ",
                ],
                '',
                'Tool tips info',
                '1',
                'callednum[callednum-string]',
                '',
                '',
                '',
                'search_string_type',
                '',
            ],
            [
                'Destination ',
                'INPUT',
                [
                    'name'  => 'notes[notes]',
                    'value' => '',
                    'size'  => '20',
                    'class' => "text field ",
                ],
                '',
                'Tool tips info',
                '1',
                'notes[notes-integer]',
                '',
                '',
                '',
                'search_int_type',
                '',
            ],
            [
                'Duration',
                'INPUT',
                [
                    'name'  => 'billseconds[billseconds]',
                    'value' => '',
                    'size'  => '20',
                    'class' => "text field ",
                ],
                '',
                'Tool tips info',
                '1',
                'billseconds[billseconds-integer]',
                '',
                '',
                '',
                'search_int_type',
                '',
            ],
            $cost_array,
            [
                'Disposition',
                'disposition',
                'SELECT',
                '',
                '',
                'tOOL TIP',
                'Please Enter account number',
                '',
                '',
                '',
                'set_despostion',
            ],
            [
                'Call Type',
                'calltype',
                'SELECT',
                '',
                '',
                'tOOL TIP',
                'Please Enter account number',
                '',
                '',
                '',
                'set_calltype',
            ],

            [
                '',
                'HIDDEN',
                'ajax_search',
                '1',
                '',
                '',
                '',
            ],
            [
                '',
                'HIDDEN',
                'advance_search',
                '1',
                '',
                '',
                '',
            ],
        ];
        $form ['display_in'] = [
            'name'           => 'search_in',
            "id"             => "search_in",
            "function"       => "search_report_in",
            "content"        => "Display records in",
            'label_class'    => "search_label col-md-3 no-padding",
            "dropdown_class" => "form-control",
            "label_style"    => "font-size:13px;",
            "dropdown_style" => "background: #ddd; width: 21% !important;",
        ];
        $form ['button_search'] = [
            'name'    => 'action',
            'id'      => "user_provider_cdr_search_btn",
            'content' => 'Search',
            'value'   => 'save',
            'type'    => 'button',
            'class'   => 'btn btn-line-parrot pull-right',
        ];
        $form ['button_reset'] = [
            'name'    => 'action',
            'id'      => "id_reset",
            'content' => 'Clear',
            'value'   => 'cancel',
            'type'    => 'reset',
            'class'   => 'btn btn-line-sky pull-right margin-x-10',
        ];

        return $form;
    }
}

?>
