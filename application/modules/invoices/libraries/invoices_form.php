<?php
// ##############################################################################
// ASTPP - Open Source VoIP Billing Solution
//
// Copyright (C) 2016 iNextrix Technologies Pvt. Ltd.
// Samir Doshi <samir.doshi@inextrix.com>
// ASTPP Version 3.0 and above
// License https://www.gnu.org/licenses/agpl-3.0.html
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
// ##############################################################################
if (!defined('BASEPATH')) {
    exit ('No direct script access allowed');
}

class invoices_form
{
    function __construct($library_name = '')
    {
        $this->CI = &get_instance();
    }

    function build_invoices_list_for_admin()
    {
        $account_info = $accountinfo = $this->CI->session->userdata('accountinfo');
        $currency_id = $account_info ['currency_id'];
        $currency = $this->CI->common->get_field_name('currency', 'currency', $currency_id);

        $logintype = $this->CI->session->userdata('logintype');
        $url = ($logintype == 0 || $logintype == 3) ? "/user/user_invoice_download/" : '/invoices/invoice_main_download/';
        $grid_field_arr = json_encode(
            [
                [
                    gettext("Number"),
                    "110",
                    "id",
                    "id,'',type",
                    "invoices",
                    "build_concat_string",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Type"),
                    "130",
                    "id",
                    "id,'',type",
                    "invoices",
                    "build_concat_string",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Account"),
                    "130",
                    "accountid",
                    "first_name,last_name,number",
                    "accounts",
                    "build_concat_string",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Generated<br/> Date"),
                    "140",
                    "invoice_date",
                    "invoice_date",
                    "",
                    "get_invoice_date",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("From Date"),
                    "120",
                    "from_date",
                    "from_date",
                    "",
                    "get_from_date",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Due Date"),
                    "130",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Last <br/>Pay Date"),
                    "100",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Amount($currency)"),
                    "120",
                    "id",
                    "id",
                    "id",
                    "get_invoice_total",
                    "",
                    "true",
                    "right",
                ],

                [
                    gettext("Outstanding <br/>Amount($currency)"),
                    "140",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "right",
                ],
                // array("Payment", "110", "payment", "", "", ""),
                [
                    gettext("Action"),
                    "120",
                    "",
                    "",
                    "",
                    [
                        "DOWNLOAD" => [
                            "url"  => $url,
                            "mode" => "single",
                        ],
                    ]
                    // "VIEW" => array("url" => "invoices/invoice_summary_payment/", "mode" => "popup")

                ],
            ]
        );

        return $grid_field_arr;
    }

    function build_invoices_list_for_customer_admin()
    {
        $account_info = $accountinfo = $this->CI->session->userdata('accountinfo');
        $currency_id = $account_info ['currency_id'];
        $currency = $this->CI->common->get_field_name('currency', 'currency', $currency_id);

        $logintype = $this->CI->session->userdata('logintype');
        $url = ($logintype == 0 || $logintype == 3) ? "/user/user_invoice_download/" : '/invoices/invoice_main_download/';
        $grid_field_arr = json_encode(
            [
                [
                    gettext("Number"),
                    "110",
                    "id",
                    "id,'',type",
                    "invoices",
                    "build_concat_string",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Type"),
                    "110",
                    "id",
                    "id,'',type",
                    "invoices",
                    "build_concat_string",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Generated<br/> Date"),
                    "120",
                    "invoice_date",
                    "invoice_date",
                    "",
                    "get_invoice_date",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("From Date"),
                    "120",
                    "from_date",
                    "from_date",
                    "",
                    "get_from_date",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Due Date"),
                    "130",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Last <br/>Pay Date"),
                    "100",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Amount($currency)"),
                    "100",
                    "id",
                    "id",
                    "id",
                    "get_invoice_total",
                    "",
                    "true",
                    "right",
                ],

                [
                    gettext("Outstanding <br/>Amount($currency)"),
                    "100",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "right",
                ],
                [
                    gettext("Action"),
                    "120",
                    "",
                    "",
                    "",
                    [
                        "DOWNLOAD" => [
                            "url"  => $url,
                            "mode" => "single",
                        ],
                    ],
                ],
            ]
        );

        return $grid_field_arr;
    }

    function build_invoices_list_for_customer()
    {
        $url = ($this->CI->session->userdata(
                'logintype'
            ) == 0) ? "/user/user_invoice_download/" : '/invoices/invoice_main_download/';
        // array(display name, width, db_field_parent_table,feidname, db_field_child_table,function name);
        $grid_field_arr = json_encode(
            [
                [
                    gettext("Number"),
                    "100",
                    "id",
                    "id,'',type",
                    "invoices",
                    "build_concat_string",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Account"),
                    "110",
                    "accountid",
                    "first_name,last_name,number",
                    "accounts",
                    "build_concat_string",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Generated Date"),
                    "140",
                    "invoice_date",
                    "invoice_date",
                    "",
                    "get_invoice_date",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("From Date"),
                    "140",
                    "from_date",
                    "from_date",
                    "",
                    "get_from_date",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Due Date"),
                    "150",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Last Pay Date"),
                    "150",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Amount)"),
                    "150",
                    "id",
                    "id",
                    "id",
                    "get_invoice_total",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Outstanding Amount"),
                    "150",
                    "",
                    "",
                    "",
                    "",
                ],
                // array("Payment", "110", "payment", "", "", ""),
                [
                    gettext("Action"),
                    "160",
                    "",
                    "",
                    "",
                    [
                        "DOWNLOAD" => [
                            "url"  => $url,
                            "mode" => "single",
                        ],
                    ]
                    // "VIEW" => array("url" => "invoices/invoice_summary_payment/", "mode" => "popup")

                ],
            ]
        );

        return $grid_field_arr;
    }

    function get_invoice_search_form()
    {
        $account_data = $this->CI->session->userdata("accountinfo");
        $reseller_id = $account_data ['type'] == 1 ? $account_data ['id'] : 0;
        $form ['forms'] = [
            "",
            [
                'id' => "invoice_search",
            ],
        ];
        $form [gettext('Search')] = [
            [
                gettext('Number'),
                'INPUT',
                [
                    'name'  => 'invoiceid[invoiceid]',
                    '',
                    'id'    => 'invoiceid',
                    'size'  => '15',
                    'class' => "text field ",
                ],
                '',
                'tOOL TIP',
                '1',
                'invoiceid[invoiceid-string]',
                '',
                '',
                '',
                'search_string_type',
                '',
            ],
            [
                gettext('From Date'),
                'INPUT',
                [
                    'name'  => 'from_date[0]',
                    'id'    => 'invoice_from_date',
                    'size'  => '20',
                    'class' => "text field",
                ],
                '',
                'tOOL TIP',
                '',
                'from_date[from_date-date]',
            ],
            [
                gettext('To Date'),
                'INPUT',
                [
                    'name'  => 'to_date[0]',
                    'id'    => 'invoice_to_date',
                    'size'  => '20',
                    'class' => "text field",
                ],
                '',
                'tOOL TIP',
                '',
                'from_date[from_date-date]',
            ],
            [
                gettext('Amount'),
                'INPUT',
                [
                    'name'  => 'amount[amount]',
                    'value' => '',
                    'size'  => '20',
                    'class' => "text field ",
                ],
                '',
                'Tool tips info',
                '1',
                'amount[amount-string]',
                '',
                '',
                '',
                'search_string_type',
                '',
            ],
            [
                gettext('Generated Date'),
                'INPUT',
                [
                    'name'  => 'invoice_date[0]',
                    '',
                    'size'  => '20',
                    'class' => "text field",
                    'id'    => 'invoice_date',
                ],
                '',
                'tOOL TIP',
                '',
                'invoice_date[invoice_date-date]',
            ],
            // array('Invoice', 'deleted', 'SELECT', '', '', 'tOOL TIP', 'Please Enter account number', '', '', '', 'set_invoice_details'),
            [
                gettext('Account'),
                'accountid',
                'SELECT',
                '',
                '',
                'tOOL TIP',
                'Please Enter account number',
                'id',
                'IF(`deleted`=1,concat( first_name, " ", last_name, " ", "(", number, ")^" ),concat( first_name, " ", last_name, " ", "(", number, ")" )) as number',
                'accounts',
                'build_dropdown_deleted',
                'where_arr',
                [
                    "reseller_id" => $reseller_id,
                    "type"        => "GLOBAL",
                ],
            ],
            [
                '',
                'HIDDEN',
                'ajax_search',
                '1',
                '',
                '',
                '',
            ],
            [
                '',
                'HIDDEN',
                'advance_search',
                '1',
                '',
                '',
                '',
            ],
        ];

        $form ['button_search'] = [
            'name'    => 'action',
            'id'      => "invoice_search_btn",
            'content' => gettext('Search'),
            'value'   => 'save',
            'type'    => 'button',
            'class'   => 'btn btn-line-parrot pull-right',
        ];
        $form ['button_reset'] = [
            'name'    => 'action',
            'id'      => "id_reset",
            'content' => gettext('Clear'),
            'value'   => 'cancel',
            'type'    => 'reset',
            'class'   => 'btn btn-line-sky pull-right margin-x-10',
        ];

        return $form;
    }

    function build_grid_buttons()
    {
        $buttons_json = json_encode([]);

        return $buttons_json;
    }

    function get_invoiceconf_form_fields($invoiceconf = '0')
    {
        if (!empty ($invoiceconf)) {
            if ($invoiceconf ['logo'] != '') {
                $logo = $invoiceconf ['file'];
            } else {
                $logo = $invoiceconf ['logo'];
            }
            if ($invoiceconf ['favicon'] != '') {
                $favicon = $invoiceconf ['file_fav'];
            } else {
                $favicon = $invoiceconf ['favicon'];
            }
            $accountid = $invoiceconf ['accountid'];
            if ($logo != '') {
                $file_name = base_url()."upload/$logo";
                $image_path = [
                    'Existing Image',
                    'IMAGE',
                    [
                        'type'  => 'image',
                        'name'  => 'image',
                        'style' => 'width:100%;margin-top:20px;',
                        'src'   => $file_name,
                    ],
                    '',
                    'tOOL TIP',
                    '',
                ];
                $delete_logo = [
                    'Delete logo',
                    'DEL_BUTTON',
                    [
                        'value' => 'ankit',
                        'style' => 'margin-top:20px;',
                        'name'  => 'button',
                        'id'    => 'logo_delete',
                        'size'  => '20',
                        'class' => "btn btn-line-parrot",
                    ],
                    '',
                    'tOOL TIP',
                    'Please Enter account number',
                ];
            } else {
                $image_path = [
                    'Existing Image',
                    'HIDDEN',
                    [
                        'type'  => '',
                        'name'  => '',
                        'style' => 'width:250px;',
                    ],
                    '',
                    'tOOL TIP',
                    '',
                ];
                $delete_logo = [
                    'Delete logo',
                    'HIDDEN',
                    [
                        'value'     => 'ankit',
                        'style'     => 'margin-top:0px;',
                        'name'      => 'button',
                        'id'        => 'logo_delete',
                        'size'      => '20',
                        'maxlength' => '100',
                        'class'     => "btn btn-line-parrot",
                    ],
                    '',
                    'tOOL TIP',
                    'Please Enter account number',
                ];
                // $image_path=array();
            }
            if ($favicon != '') {

                $file_name_fav = base_url()."upload/$favicon";

                $image_fav = [
                    'Existing Favicon',
                    'IMAGE',
                    [
                        'type'  => 'image',
                        'name'  => 'image',
                        'style' => 'width:100%;margin-top:20px;',
                        'src'   => $file_name_fav,
                    ],
                    '',
                    'tOOL TIP',
                    '',
                ];
                $delete_fav = [
                    'Delete Favicon',
                    'DEL_BUTTON',
                    [
                        'value' => '',
                        'style' => 'margin-top:20px;',
                        'name'  => 'button',
                        'id'    => 'fav_delete',
                        'size'  => '20',
                        'class' => "btn btn-line-parrot",
                    ],
                    '',
                    'tOOL TIP',
                    'Please Enter account number',
                ];
            } else {
                $image_fav = [
                    'Existing Favicon',
                    'HIDDEN',
                    [
                        'type'  => '',
                        'name'  => '',
                        'style' => 'width:250px;',
                    ],
                    '',
                    'tOOL TIP',
                    '',
                ];
                $delete_fav = [
                    'Delete Favicon',
                    'HIDDEN',
                    [
                        'value'     => '',
                        'style'     => 'margin-top:0px;',
                        'name'      => 'button',
                        'id'        => 'fav_delete',
                        'size'      => '20',
                        'maxlength' => '100',
                        'class'     => "btn btn-line-parrot",
                    ],
                    '',
                    'tOOL TIP',
                    'Please Enter account number',
                ];
                // $image_path=array();
            }
        } else {
            $logo = '';
            $file_name = '';
            $favicon = '';
            $file_name_fav = '';
            $accountid = 0;
            $image_path = [
                'Existing Logo',
                'HIDDEN',
                [
                    'type' => '',
                    'name' => '',
                ],
                '',
                'tOOL TIP',
                '',
            ];
            $delete_logo = [
                'Delete logo',
                'HIDDEN',
                [
                    'value'     => '',
                    'style'     => 'margin-top:0px;',
                    'name'      => 'button',
                    'onclick'   => 'return image_delete('.$accountid.')',
                    'size'      => '20',
                    'maxlength' => '100',
                    'class'     => "btn btn-line-parrot",
                ],
                '',
                'tOOL TIP',
                'Please Enter account number',
            ];
            $image_fav = [
                'Existing Favicon',
                'HIDDEN',
                [
                    'type' => '',
                    'name' => '',
                ],
                '',
                'tOOL TIP',
                '',
            ];
            $delete_fav = [
                'Delete Favicon',
                'HIDDEN',
                [
                    'value'     => '',
                    'style'     => 'margin-top:0px;',
                    'name'      => 'button',
                    'onclick'   => 'return image_delete('.$accountid.')',
                    'size'      => '20',
                    'maxlength' => '100',
                    'class'     => "btn btn-line-parrot",
                ],
                '',
                'tOOL TIP',
                'Please Enter account number',
            ];
            // $image_path=array();
        }
        $form ['forms'] = [
            base_url().'invoices/invoice_conf/',
            [
                'id'      => 'invoice_conf_form',
                'method'  => 'POST',
                'name'    => 'invoice_conf_form',
                'enctype' => 'multipart/form-data',
            ],
        ];
        $form ['Configuration '] = [
            [
                '',
                'HIDDEN',
                [
                    'name' => 'id',
                ],
                '',
                '',
                '',
                '',
            ],
            [
                '',
                'HIDDEN',
                [
                    'name' => 'accountid',
                ],
                '',
                '',
                '',
                '',
            ],
            // array('', 'HIDDEN', array('name' => 'start_name','value' => '1'), '', '', ''),
            [
                gettext('Company name'),
                'INPUT',
                [
                    'name'  => 'company_name',
                    'size'  => '20',
                    'class' => "text field medium",
                ],
                '',
                'tOOL TIP',
                'Please Enter account number',
            ],
            [
                gettext('Address'),
                'INPUT',
                [
                    'name'  => 'address',
                    'size'  => '20',
                    'class' => "text field medium",
                ],
                '',
                'tOOL TIP',
                'Please Enter account number',
            ],
            [
                gettext('City'),
                'INPUT',
                [
                    'name'  => 'city',
                    'size'  => '20',
                    'class' => "text field medium",
                ],
                '',
                'tOOL TIP',
                'Please Enter account number',
            ],
            [
                gettext('Province'),
                'INPUT',
                [
                    'name'  => 'province',
                    'size'  => '20',
                    'class' => "text field medium",
                ],
                '',
                'tOOL TIP',
                'Please Enter account number',
            ],
            [
                gettext('Country'),
                'INPUT',
                [
                    'name'  => 'country',
                    'size'  => '20',
                    'class' => "text field medium",
                ],
                '',
                'tOOL TIP',
                'Please Enter account number',
            ],
            [
                gettext('Zipcode'),
                'INPUT',
                [
                    'name'  => 'zipcode',
                    'size'  => '20',
                    'class' => "text field medium",
                ],
                '',
                'tOOL TIP',
                'Please Enter account number',
            ],
            [
                gettext('Telephone'),
                'INPUT',
                [
                    'name'  => 'telephone',
                    'size'  => '20',
                    'class' => "text field medium",
                ],
                '',
                'tOOL TIP',
                'Please Enter account number',
            ],
            [
                gettext('Fax'),
                'INPUT',
                [
                    'name'  => 'fax',
                    'size'  => '20',
                    'class' => "text field medium",
                ],
                '',
                'tOOL TIP',
                'Please Enter account number',
            ],
            [
                gettext('Email Address'),
                'INPUT',
                [
                    'name'  => 'emailaddress',
                    'size'  => '20',
                    'class' => "text field medium",
                ],
                '',
                'tOOL TIP',
                'Please Enter account number',
            ],
            [
                gettext('Website'),
                'INPUT',
                [
                    'name'  => 'website',
                    'size'  => '20',
                    'class' => "text field medium",
                ],
                '',
                'tOOL TIP',
                'Please Enter account number',
            ],
            [
                gettext('Company Tax number'),
                'INPUT',
                [
                    'name'  => 'invoice_taxes_number',
                    'size'  => '20',
                    'class' => "text field medium",
                ],
                '',
                'tOOL TIP',
                'Please Enter account number',
            ],
        ];
        $form ['Invoice Configuration '] = [
            [
                '',
                'HIDDEN',
                [
                    'name' => 'id',
                ],
                '',
                '',
                '',
                '',
            ],
            [
                '',
                'HIDDEN',
                [
                    'name' => 'accountid',
                ],
                '',
                '',
                '',
                '',
            ],
            [
                gettext('Invoice Notification'),
                'invoice_notification',
                'SELECT',
                '',
                '',
                'tOOL TIP',
                '',
                '',
                '',
                '',
                'set_allow_invoice',
            ],
            [
                gettext('Invoice Due Notification'),
                'invoice_due_notification',
                'SELECT',
                '',
                '',
                'tOOL TIP',
                '',
                '',
                '',
                '',
                'set_allow_invoice',
            ],
            [
                gettext('Invoice Due Days'),
                'INPUT',
                [
                    'name'  => 'interval',
                    'size'  => '20',
                    'class' => "text field medium",
                ],
                '',
                'tOOL TIP',
                'Please Enter account number',
            ],
            [
                gettext('Notify before due days'),
                'INPUT',
                [
                    'name'  => 'notify_before_day',
                    'size'  => '20',
                    'class' => "text field medium",
                ],
                '',
                'tOOL TIP',
                'Please Enter account number',
            ],
            [
                gettext('Invoice Prefix'),
                'INPUT',
                [
                    'name'  => 'invoice_prefix',
                    'size'  => '20',
                    'class' => "text field medium",
                ],
                '',
                'tOOL TIP',
                'Please Enter account number',
            ],
            [
                gettext('Invoice Start Form'),
                'INPUT',
                [
                    'name'  => 'invoice_start_from',
                    'size'  => '20',
                    'class' => "text field medium",
                ],
                '',
                'tOOL TIP',
                'Please Enter account number',
            ],
            [
                '',
                'HIDDEN',
                [
                    'name' => '',
                ],
                '',
                '',
                '',
                '',
            ],
            [
                '',
                'HIDDEN',
                [
                    'name' => '',
                ],
                '',
                '',
                '',
                '',
            ],
        ];
        $form ['Portal personalization'] = [

            [
                gettext('Domain'),
                'INPUT',
                [
                    'name'      => 'domain',
                    'size'      => '20',
                    'maxlength' => '100',
                    'class'     => "text field medium",
                ],
                '',
                'tOOL TIP',
                '',
            ],
            [
                gettext('Header'),
                'INPUT',
                [
                    'name'      => 'website_title',
                    'size'      => '100',
                    'maxlength' => '100',
                    'class'     => "text field medium",
                ],
                '',
                'tOOL TIP',
                '',
            ],
            [
                gettext('Footer'),
                'INPUT',
                [
                    'name'      => 'website_footer',
                    'size'      => '200',
                    'maxlength' => '200',
                    'class'     => "text field medium",
                ],
                '',
                'tOOL TIP',
                '',
            ],
            [
                gettext('Logo'),
                'IMAGE',
                [
                    'name'      => 'file',
                    'size'      => '20',
                    'maxlength' => '100',
                    'class'     => "",
                    'id'        => 'uploadFile',
                    'type'      => 'file',
                ],
                'class' => '',
                'tOOL TIP',
                'Please Enter account number',
            ],
            // array('', 'BLANL_DIV', array('name'=>'accountid','id'=>'imagePreview'),'', '', '', ''),
            $delete_logo,
            $image_path,
            [
                'Favicon',
                'IMAGE',
                [
                    'name'      => 'file_fav',
                    'size'      => '20',
                    'maxlength' => '100',
                    'class'     => "",
                    'id'        => 'uploadFav',
                    'type'      => 'file',
                ],
                'class' => '',
                'tOOL TIP',
                'Please Enter account number',
            ],
            $delete_fav,
            $image_fav,
        ];
        $form ['button_save'] = [
            'name'    => 'action',
            'content' => gettext('Save'),
            'value'   => 'save',
            'type'    => 'submit',
            'class'   => 'btn btn-line-parrot',
        ];

        return $form;
    }
}

?>
