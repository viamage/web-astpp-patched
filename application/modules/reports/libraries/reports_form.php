<?php
// ##############################################################################
// ASTPP - Open Source VoIP Billing Solution
//
// Copyright (C) 2016 iNextrix Technologies Pvt. Ltd.
// Samir Doshi <samir.doshi@inextrix.com>
// ASTPP Version 3.0 and above
// License https://www.gnu.org/licenses/agpl-3.0.html
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
// ##############################################################################
if (!defined('BASEPATH')) {
    exit ('No direct script access allowed');
}

class Reports_form
{
    function __construct()
    {
        $this->CI = &get_instance();
    }

    function get_customer_cdr_form()
    {
        $logintype = $this->CI->session->userdata('userlevel_logintype');
        if ($logintype != 1) {
            if ($this->CI->session->userdata('logintype') == 1 || $this->CI->session->userdata('logintype') == 5) {
                $accountinfo = $this->CI->session->userdata ['accountinfo'];
                $reseller_id = $accountinfo ["id"];
            } else {
                $reseller_id = "0";
            }
            $form ['forms'] = [
                "",
                [
                    'id' => "cdr_customer_search",
                ],
            ];
            $form [gettext('Search')] = [
                [
                    gettext('From Date'),
                    'INPUT',
                    [
                        'name'  => 'callstart[]',
                        'id'    => 'customer_cdr_from_date',
                        'size'  => '20',
                        'class' => "text field ",
                    ],
                    '',
                    'tOOL TIP',
                    '',
                    'start_date[start_date-date]',
                ],
                [
                    gettext('To Date'),
                    'INPUT',
                    [
                        'name'  => 'callstart[]',
                        'id'    => 'customer_cdr_to_date',
                        'size'  => '20',
                        'class' => "text field ",
                    ],
                    '',
                    'tOOL TIP',
                    '',
                    'end_date[end_date-date]',
                ],
                [
                    gettext('Caller ID'),
                    'INPUT',
                    [
                        'name'  => 'callerid[callerid]',
                        '',
                        'id'    => 'first_name',
                        'size'  => '15',
                        'class' => "text field ",
                    ],
                    '',
                    'tOOL TIP',
                    '1',
                    'callerid[callerid-string]',
                    '',
                    '',
                    '',
                    'search_string_type',
                    '',
                ],
                [
                    gettext('Called Number'),
                    'INPUT',
                    [
                        'name'  => 'callednum[callednum]',
                        'value' => '',
                        'size'  => '20',
                        'class' => "text field ",
                    ],
                    '',
                    'Tool tips info',
                    '1',
                    'callednum[callednum-string]',
                    '',
                    '',
                    '',
                    'search_string_type',
                    '',
                ],
                [
                    gettext('Code'),
                    'INPUT',
                    [
                        'name'  => 'pattern[pattern]',
                        'value' => '',
                        'size'  => '20',
                        'class' => "text field ",
                    ],
                    '',
                    'Tool tips info',
                    '1',
                    'pattern[pattern-string]',
                    '',
                    '',
                    '',
                    'search_string_type',
                    '',
                ],
                [
                    gettext('Destination'),
                    'INPUT',
                    [
                        'name'  => 'notes[notes]',
                        'value' => '',
                        'size'  => '20',
                        'class' => "text field ",
                    ],
                    '',
                    'Tool tips info',
                    '1',
                    'notes[notes-string]',
                    '',
                    '',
                    '',
                    'search_string_type',
                    '',
                ],
                [
                    gettext('Duration'),
                    'INPUT',
                    [
                        'name'  => 'billseconds[billseconds]',
                        'value' => '',
                        'size'  => '20',
                        'class' => "text field ",
                    ],
                    '',
                    'Tool tips info',
                    '1',
                    'billseconds[billseconds-integer]',
                    '',
                    '',
                    '',
                    'search_int_type',
                    '',
                ],
                [
                    gettext('Debit'),
                    'INPUT',
                    [
                        'name'  => 'debit[debit]',
                        'value' => '',
                        'size'  => '20',
                        'class' => "text field ",
                    ],
                    '',
                    'Tool tips info',
                    '1',
                    'debit[debit-integer]',
                    '',
                    '',
                    '',
                    'search_int_type',
                    '',
                ],
                [
                    gettext('Cost'),
                    'INPUT',
                    [
                        'name'  => 'cost[cost]',
                        'value' => '',
                        'size'  => '20',
                        'class' => "text field ",
                    ],
                    '',
                    'Tool tips info',
                    '1',
                    'cost[cost-integer]',
                    '',
                    '',
                    '',
                    'search_int_type',
                    '',
                ],
                [
                    gettext('Disposition [Q.850]'),
                    'disposition',
                    'SELECT',
                    '',
                    '',
                    'tOOL TIP',
                    'Please Enter account number',
                    '',
                    '',
                    '',
                    'set_despostion',
                ],
                [
                    gettext('Account'),
                    'accountid',
                    'SELECT',
                    '',
                    '',
                    'tOOL TIP',
                    'Please Enter account number',
                    'id',
                    'IF(`deleted`=1,concat( first_name, " ", last_name, " ", "(", number, ")^" ),concat( first_name, " ", last_name, " ", "(", number, ")" )) as number',
                    'accounts',
                    'build_dropdown_deleted',
                    'where_arr',
                    [
                        "reseller_id" => "0",
                        "type"        => "GLOBAL",
                    ],
                ],

                [
                    gettext('Trunk'),
                    'trunk_id',
                    'SELECT',
                    '',
                    '',
                    'tOOL TIP',
                    'Please Enter account number',
                    'id',
                    'IF(`status`=2, concat(name,"","^"),name) as name',
                    'trunks',
                    'build_dropdown_deleted',
                    '',
                    [
                        "status" => "1",
                    ],
                ],

                [
                    gettext('Rate Group'),
                    'pricelist_id',
                    'SELECT',
                    '',
                    '',
                    'tOOL TIP',
                    'Please Enter account number',
                    'id',
                    'IF(`status`=2, concat(name,"","^"),name) as name',
                    'pricelists',
                    'build_dropdown_deleted',
                    'where_arr',
                    [
                        "reseller_id" => "0",
                    ],
                ],
                [
                    gettext('Call Type'),
                    'calltype',
                    'SELECT',
                    '',
                    '',
                    'tOOL TIP',
                    'Please Enter account number',
                    '',
                    '',
                    '',
                    'set_calltype',
                ],
                [
                    '',
                    'HIDDEN',
                    'ajax_search',
                    '1',
                    '',
                    '',
                    '',
                ],
                [
                    '',
                    'HIDDEN',
                    'advance_search',
                    '1',
                    '',
                    '',
                    '',
                ],
            ];
        } else {
            $form ['forms'] = [
                "",
                [
                    'id' => "cdr_customer_search",
                ],
            ];
            $form [gettext('Search')] = [
                [
                    gettext('From Date'),
                    'INPUT',
                    [
                        'name'  => 'callstart[]',
                        'id'    => 'customer_cdr_from_date',
                        'size'  => '20',
                        'class' => "text field ",
                    ],
                    '',
                    'tOOL TIP',
                    '',
                    'start_date[start_date-date]',
                ],
                [
                    gettext('To Date'),
                    'INPUT',
                    [
                        'name'  => 'callstart[]',
                        'id'    => 'customer_cdr_to_date',
                        'size'  => '20',
                        'class' => "text field ",
                    ],
                    '',
                    'tOOL TIP',
                    '',
                    'end_date[end_date-date]',
                ],
                [
                    gettext('Caller ID'),
                    'INPUT',
                    [
                        'name'  => 'callerid[callerid]',
                        '',
                        'id'    => 'first_name',
                        'size'  => '15',
                        'class' => "text field ",
                    ],
                    '',
                    'tOOL TIP',
                    '1',
                    'callerid[callerid-string]',
                    '',
                    '',
                    '',
                    'search_string_type',
                    '',
                ],
                [
                    gettext('Called Number'),
                    'INPUT',
                    [
                        'name'  => 'callednum[callednum]',
                        'value' => '',
                        'size'  => '20',
                        'class' => "text field ",
                    ],
                    '',
                    'Tool tips info',
                    '1',
                    'callednum[callednum-string]',
                    '',
                    '',
                    '',
                    'search_string_type',
                    '',
                ],
                [
                    gettext('Code'),
                    'INPUT',
                    [
                        'name'  => 'pattern[pattern]',
                        'value' => '',
                        'size'  => '20',
                        'class' => "text field ",
                    ],
                    '',
                    'Tool tips info',
                    '1',
                    'pattern[pattern-string]',
                    '',
                    '',
                    '',
                    'search_string_type',
                    '',
                ],
                [
                    gettext('Destination'),
                    'INPUT',
                    [
                        'name'  => 'notes[notes]',
                        'value' => '',
                        'size'  => '20',
                        'class' => "text field ",
                    ],
                    '',
                    'Tool tips info',
                    '1',
                    'notes[notes-string]',
                    '',
                    '',
                    '',
                    'search_string_type',
                    '',
                ],
                [
                    gettext('Duration'),
                    'INPUT',
                    [
                        'name'  => 'billseconds[billseconds]',
                        'value' => '',
                        'size'  => '20',
                        'class' => "text field ",
                    ],
                    '',
                    'Tool tips info',
                    '1',
                    'billseconds[billseconds-integer]',
                    '',
                    '',
                    '',
                    'search_int_type',
                    '',
                ],
                [
                    gettext('Debit'),
                    'INPUT',
                    [
                        'name'  => 'debit[debit]',
                        'value' => '',
                        'size'  => '20',
                        'class' => "text field ",
                    ],
                    '',
                    'Tool tips info',
                    '1',
                    'debit[debit-integer]',
                    '',
                    '',
                    '',
                    'search_int_type',
                    '',
                ],
                [
                    gettext('Cost'),
                    'INPUT',
                    [
                        'name'  => 'cost[cost]',
                        'value' => '',
                        'size'  => '20',
                        'class' => "text field ",
                    ],
                    '',
                    'Tool tips info',
                    '1',
                    'cost[cost-integer]',
                    '',
                    '',
                    '',
                    'search_int_type',
                    '',
                ],
                [
                    gettext('Disposition [Q.850]'),
                    'disposition',
                    'SELECT',
                    '',
                    '',
                    'tOOL TIP',
                    'Please Enter account number',
                    '',
                    '',
                    '',
                    'set_despostion',
                ],
                [
                    gettext('Account'),
                    'accountid',
                    'SELECT',
                    '',
                    '',
                    'tOOL TIP',
                    'Please Enter account number',
                    'id',
                    'IF(`deleted`=1,concat( first_name, " ", last_name, " ", "(", number, ")^" ),concat( first_name, " ", last_name, " ", "(", number, ")" )) as number',
                    'accounts',
                    'build_dropdown_deleted',
                    'where_arr',
                    [
                        "reseller_id" => "0",
                        "type"        => "GLOBAL",
                    ],
                ],

                // array('Trunk', 'trunk_id', 'SELECT', '', '', 'tOOL TIP', 'Please Enter account number', 'id', 'IF(`status`=2, concat(name,"","^"),name) as name', 'trunks', 'build_dropdown_deleted', '', array("status" => "1")),

                [
                    gettext('Rate Group'),
                    'pricelist_id',
                    'SELECT',
                    '',
                    '',
                    'tOOL TIP',
                    'Please Enter account number',
                    'id',
                    'IF(`status`=2, concat(name,"","^"),name) as name',
                    'pricelists',
                    'build_dropdown_deleted',
                    'where_arr',
                    [
                        "reseller_id" => "0",
                    ],
                ],
                [
                    gettext('Call Type'),
                    'calltype',
                    'SELECT',
                    '',
                    '',
                    'tOOL TIP',
                    'Please Enter account number',
                    '',
                    '',
                    '',
                    'set_calltype',
                ],
                [
                    '',
                    'HIDDEN',
                    'ajax_search',
                    '1',
                    '',
                    '',
                    '',
                ],
                [
                    '',
                    'HIDDEN',
                    'advance_search',
                    '1',
                    '',
                    '',
                    '',
                ],
            ];
        }

        $form ['display_in'] = [
            'name'           => 'search_in',
            "id"             => "search_in",
            "function"       => "search_report_in",
            "content"        => "Display records in",
            'label_class'    => "search_label col-md-3 no-padding",
            "dropdown_class" => "form-control",
            "label_style"    => "font-size:13px;",
            "dropdown_style" => "background: #ddd; width: 21% !important;",
        ];

        /**
         * *************************************
         */
        $form ['button_search'] = [
            'name'    => 'action',
            'id'      => "cusotmer_cdr_search_btn",
            'content' => gettext('Search'),
            'value'   => 'save',
            'type'    => 'button',
            'class'   => 'btn btn-line-parrot pull-right',
        ];
        $form ['button_reset'] = [
            'name'    => 'action',
            'id'      => "id_reset",
            'content' => 'Clear',
            'value'   => gettext('Cancel'),
            'type'    => 'reset',
            'class'   => 'btn btn-line-sky pull-right margin-x-10',
        ];

        return $form;
    }

    function get_reseller_cdr_form()
    {
        $form ['forms'] = [
            "",
            [
                'id' => "cdr_reseller_search",
            ],
        ];
        $form [gettext('Search')] = [
            [
                gettext('From Date'),
                'INPUT',
                [
                    'name'  => 'callstart[]',
                    'id'    => 'customer_cdr_from_date',
                    'size'  => '20',
                    'class' => "text field ",
                ],
                '',
                'tOOL TIP',
                '',
                'number[number-string]',
            ],
            [
                gettext('To Date'),
                'INPUT',
                [
                    'name'  => 'callstart[]',
                    'id'    => 'customer_cdr_to_date',
                    'size'  => '20',
                    'class' => "text field ",
                ],
                '',
                'tOOL TIP',
                '',
                'number[number-string]',
            ],
            [
                gettext('Caller ID'),
                'INPUT',
                [
                    'name'  => 'callerid[callerid]',
                    '',
                    'id'    => 'first_name',
                    'size'  => '15',
                    'class' => "text field ",
                ],
                '',
                'tOOL TIP',
                '1',
                'callerid[callerid-string]',
                '',
                '',
                '',
                'search_string_type',
                '',
            ],
            [
                gettext('Called Number'),
                'INPUT',
                [
                    'name'  => 'callednum[callednum]',
                    'value' => '',
                    'size'  => '20',
                    'class' => "text field ",
                ],
                '',
                'Tool tips info',
                '1',
                'callednum[callednum-string]',
                '',
                '',
                '',
                'search_string_type',
                '',
            ],

            [
                gettext('Code'),
                'INPUT',
                [
                    'name'  => 'pattern[pattern]',
                    'value' => '',
                    'size'  => '20',
                    'class' => "text field ",
                ],
                '',
                'Tool tips info',
                '1',
                'pattern[pattern-string]',
                '',
                '',
                '',
                'search_string_type',
                '',
            ],
            [
                gettext('Destination'),
                'INPUT',
                [
                    'name'  => 'notes[notes]',
                    'value' => '',
                    'size'  => '20',
                    'class' => "text field ",
                ],
                '',
                'Tool tips info',
                '1',
                'notes[notes-string]',
                '',
                '',
                '',
                'search_string_type',
                '',
            ],

            [
                gettext('Duration'),
                'INPUT',
                [
                    'name'  => 'billseconds[billseconds]',
                    'value' => '',
                    'size'  => '20',
                    'class' => "text field ",
                ],
                '',
                'Tool tips info',
                '1',
                'billseconds[billseconds-integer]',
                '',
                '',
                '',
                'search_int_type',
                '',
            ],
            [
                gettext('Debit'),
                'INPUT',
                [
                    'name'  => 'debit[debit]',
                    'value' => '',
                    'size'  => '20',
                    'class' => "text field ",
                ],
                '',
                'Tool tips info',
                '1',
                'debit[debit-integer]',
                '',
                '',
                '',
                'search_int_type',
                '',
            ],
            [
                gettext('Cost'),
                'INPUT',
                [
                    'name'  => 'cost[cost]',
                    'value' => '',
                    'size'  => '20',
                    'class' => "text field ",
                ],
                '',
                'Tool tips info',
                '1',
                'cost[cost-integer]',
                '',
                '',
                '',
                'search_int_type',
                '',
            ],

            [
                gettext('Disposition [Q.850]'),
                'disposition',
                'SELECT',
                '',
                '',
                'tOOL TIP',
                'Please Enter account number',
                '',
                '',
                '',
                'set_despostion',
            ],
            [
                gettext('Account'),
                'accountid',
                'SELECT',
                '',
                '',
                'tOOL TIP',
                'Please Enter account number',
                'id',
                'IF(`deleted`=1,concat( first_name, " ", last_name, " ", "(", number, ")^" ),concat( first_name, " ", last_name, " ", "(", number, ")" )) as number',
                'accounts',
                'build_dropdown_deleted',
                'where_arr',
                [
                    "reseller_id" => "0",
                    "type"        => "1",
                ],
            ],
            [
                gettext('Rate Group'),
                'pricelist_id',
                'SELECT',
                '',
                '',
                'tOOL TIP',
                'Please Enter account number',
                'id',
                'IF(`status`=2, concat(name,"","^"),name) as name',
                'pricelists',
                'build_dropdown_deleted',
                'where_arr',
                [
                    "reseller_id" => "0",
                ],
            ],
            [
                gettext('Call Type'),
                'calltype',
                'SELECT',
                '',
                '',
                'tOOL TIP',
                'Please Enter account number',
                '',
                '',
                '',
                'set_calltype',
            ],

            [
                '',
                'HIDDEN',
                'ajax_search',
                '1',
                '',
                '',
                '',
            ],
            [
                '',
                'HIDDEN',
                'advance_search',
                '1',
                '',
                '',
                '',
            ],
        ];
        if ($this->CI->session->userdata('logintype') != 1 && $this->CI->session->userdata('logintype') != 5) {
            $new_Array = [
                'Trunk',
                'trunk_id',
                'SELECT',
                '',
                '',
                'tOOL TIP',
                'Please Enter account number',
                'id',
                'name',
                'trunks',
                'build_dropdown',
                'where_arr',
                [
                    "status" => "1",
                ],
            ];
        }

        $form ['display_in'] = [
            'name'           => 'search_in',
            "id"             => "search_in",
            "function"       => "search_report_in",
            "content"        => "Display records in &nbsp;&nbsp;",
            'label_class'    => "search_label col-md-3 no-padding",
            "dropdown_class" => "form-control",
            "label_style"    => "font-size:13px;text-align:right;",
            "dropdown_style" => "background: #ddd; width: 23% !important;",
        ];

        /**
         * *************************************
         */
        $form ['button_search'] = [
            'name'    => 'action',
            'id'      => "reseller_cdr_search_btn",
            'content' => gettext('Search'),
            'value'   => 'save',
            'type'    => 'button',
            'class'   => 'btn btn-line-parrot pull-right',
        ];
        $form ['button_reset'] = [
            'name'    => 'action',
            'id'      => "id_reset",
            'content' => 'Clear',
            'value'   => gettext('Cancel'),
            'type'    => 'reset',
            'class'   => 'btn btn-line-sky pull-right margin-x-10',
        ];

        return $form;
    }

    function get_provider_cdr_form()
    {
        $form ['forms'] = [
            "",
            [
                'id' => "cdr_provider_search",
            ],
        ];
        $form [gettext('Search')] = [
            [
                gettext('From Date'),
                'INPUT',
                [
                    'name'  => 'callstart[]',
                    'id'    => 'customer_cdr_from_date',
                    'size'  => '20',
                    'class' => "text field ",
                ],
                '',
                'tOOL TIP',
                '',
                'number[number-string]',
            ],
            [
                gettext('To Date'),
                'INPUT',
                [
                    'name'  => 'callstart[]',
                    'id'    => 'customer_cdr_to_date',
                    'size'  => '20',
                    'class' => "text field ",
                ],
                '',
                'tOOL TIP',
                '',
                'number[number-string]',
            ],
            [
                gettext('Caller ID'),
                'INPUT',
                [
                    'name'  => 'callerid[callerid]',
                    '',
                    'id'    => 'first_name',
                    'size'  => '15',
                    'class' => "text field ",
                ],
                '',
                'tOOL TIP',
                '1',
                'callerid[callerid-string]',
                '',
                '',
                '',
                'search_string_type',
                '',
            ],
            [
                gettext('Called Number'),
                'INPUT',
                [
                    'name'  => 'callednum[callednum]',
                    'value' => '',
                    'size'  => '20',
                    'class' => "text field ",
                ],
                '',
                'Tool tips info',
                '1',
                'callednum[callednum-string]',
                '',
                '',
                '',
                'search_string_type',
                '',
            ],
            [
                gettext('Code'),
                'INPUT',
                [
                    'name'  => 'pattern[pattern]',
                    'value' => '',
                    'size'  => '20',
                    'class' => "text field ",
                ],
                '',
                'Tool tips info',
                '1',
                'pattern[pattern-string]',
                '',
                '',
                '',
                'search_string_type',
                '',
            ],
            [
                gettext('Destination'),
                'INPUT',
                [
                    'name'  => 'notes[notes]',
                    'value' => '',
                    'size'  => '20',
                    'class' => "text field ",
                ],
                '',
                'Tool tips info',
                '1',
                'notes[notes-string]',
                '',
                '',
                '',
                'search_string_type',
                '',
            ],
            [
                gettext('Duration'),
                'INPUT',
                [
                    'name'  => 'billseconds[billseconds]',
                    'value' => '',
                    'size'  => '20',
                    'class' => "text field ",
                ],
                '',
                'Tool tips info',
                '1',
                'billseconds[billseconds-integer]',
                '',
                '',
                '',
                'search_int_type',
                '',
            ],
            [
                gettext('Cost'),
                'INPUT',
                [
                    'name'  => 'provider_call_cost[provider_call_cost]',
                    'value' => '',
                    'size'  => '20',
                    'class' => "text field ",
                ],
                '',
                'Tool tips info',
                '1',
                'provider_call_cost[provider_call_cost-integer]',
                '',
                '',
                '',
                'search_int_type',
                '',
            ],
            // array('Cost ', 'INPUT', array('name' => 'cost[cost]', 'value' => '', 'size' => '20', 'class' => "text field "), '', 'Tool tips info', '1', 'cost[cost-integer]', '', '', '', 'search_int_type', ''),
            [
                gettext('Disposition [Q.850]'),
                'disposition',
                'SELECT',
                '',
                '',
                'tOOL TIP',
                'Please Enter account number',
                '',
                '',
                '',
                'set_despostion',
            ],
            [
                gettext('Account'),
                'provider_id',
                'SELECT',
                '',
                '',
                'tOOL TIP',
                'Please Enter account number',
                'id',
                'IF(`deleted`=1,concat( first_name, " ", last_name, " ", "(", number, ")^" ),concat( first_name, " ", last_name, " ", "(", number, ")" )) as number',
                'accounts',
                'build_dropdown_deleted',
                'where_arr',
                [
                    "reseller_id" => "0",
                    "type"        => "3",
                ],
            ],
            [
                '',
                'HIDDEN',
                'ajax_search',
                '1',
                '',
                '',
                '',
            ],
            [
                '',
                'HIDDEN',
                'advance_search',
                '1',
                '',
                '',
                '',
            ],
        ]// array('Trunk', 'trunk_id', 'SELECT', '', '', 'tOOL TIP', 'Please Enter account number', 'id', 'name', 'trunks', 'build_dropdown', '', ''),

        ;
        $form ['display_in'] = [
            'name'           => 'search_in',
            "id"             => "search_in",
            "function"       => "search_report_in",
            "content"        => "Display records in",
            'label_class'    => "search_label col-md-3 no-padding",
            "dropdown_class" => "form-control",
            "label_style"    => "font-size:13px;",
            "dropdown_style" => "background: #ddd; width: 21% !important;",
        ];

        /**
         * *************************************
         */
        $form ['button_search'] = [
            'name'    => 'action',
            'id'      => "provider_cdr_search_btn",
            'content' => gettext('Search'),
            'value'   => 'save',
            'type'    => 'button',
            'class'   => 'btn btn-line-parrot pull-right',
        ];
        $form ['button_reset'] = [
            'name'    => 'action',
            'id'      => "id_reset",
            'content' => gettext('Clear'),
            'value'   => 'Cancel',
            'type'    => 'reset',
            'class'   => 'btn btn-line-sky pull-right margin-x-10',
        ];

        return $form;
    }

    /**
     * ****
     * ASTPP 3.0
     * Addrecording field in grid
     * *****
     */
    function build_report_list_for_customer()
    {
        $logintype = $this->CI->session->userdata('userlevel_logintype');
        if ($logintype != 1) {
            $account_info = $accountinfo = $this->CI->session->userdata('accountinfo');
            $currency_id = $account_info ['currency_id'];
            $currency = $this->CI->common->get_field_name('currency', 'currency', $currency_id);

            // $recording=array();
            $account_data = $this->CI->session->userdata("accountinfo");
            // if($account_data['type'] == 1){
            // $recording=array("Recording", "127", "recording", "", "", "");
            // }
            $grid_field_arr = json_encode(
                [
                    [
                        gettext("Date"),
                        "100",
                        "callstart",
                        "callstart",
                        "callstart",
                        "convert_GMT_to",
                        "",
                        "true",
                        "center",
                    ],
                    [
                        gettext("Caller ID"),
                        "120",
                        "callerid",
                        "",
                        "",
                        "",
                        "",
                        "true",
                        "center",
                    ],
                    [
                        gettext("Called Number"),
                        "103",
                        "callednum",
                        "",
                        "",
                        "",
                        "",
                        "true",
                        "center",
                    ],
                    [
                        gettext("Code"),
                        "71",
                        "pattern",
                        "pattern",
                        "",
                        "get_only_numeric_val",
                        "",
                        "true",
                        "center",
                    ],
                    [
                        gettext("Destination"),
                        "90",
                        "notes",
                        "",
                        "",
                        "",
                        "",
                        "true",
                        "center",
                    ],
                    [
                        gettext("Duration"),
                        "80",
                        "billseconds",
                        "customer_cdr_list_search",
                        "billseconds",
                        "convert_to_show_in",
                        "",
                        "true",
                        "center",
                    ],
                    [
                        gettext("Debit($currency)"),
                        "75",
                        "debit",
                        "debit",
                        "debit",
                        "convert_to_currency",
                        "",
                        "true",
                        "right",
                    ],
                    [
                        gettext("Cost($currency)"),
                        "75",
                        "cost",
                        "cost",
                        "cost",
                        "convert_to_currency",
                        "",
                        "true",
                        "right",
                    ],
                    [
                        gettext("Disposition [Q.850]"),
                        "150",
                        "disposition",
                        "",
                        "",
                        "",
                        "",
                        "true",
                        "center",
                    ],
                    [
                        gettext("Account"),
                        "110",
                        "accountid",
                        "first_name,last_name,number",
                        "accounts",
                        "build_concat_string",
                        "",
                        "true",
                        "center",
                    ],
                    [
                        gettext("Trunk"),
                        "90",
                        "trunk_id",
                        "name",
                        "trunks",
                        "get_field_name",
                        "",
                        "true",
                        "center",
                    ],
                    [
                        gettext("Rate Group"),
                        "90",
                        "pricelist_id",
                        "name",
                        "pricelists",
                        "get_field_name",
                        "",
                        "true",
                        "center",
                    ],
                    [
                        gettext("Call Type"),
                        "112",
                        "calltype",
                        "",
                        "",
                        "",
                    ],
                ]
            // $recording,
            );
        } else {
            $account_info = $accountinfo = $this->CI->session->userdata('accountinfo');
            $currency_id = $account_info ['currency_id'];
            $currency = $this->CI->common->get_field_name('currency', 'currency', $currency_id);
            // $recording=array("Recording", "127", "recording", "", "", "");
            $grid_field_arr = json_encode(
                [
                    [
                        gettext("Date"),
                        "100",
                        "callstart",
                        "callstart",
                        "callstart",
                        "convert_GMT_to",
                        "",
                        "true",
                        "center",
                    ],
                    [
                        gettext("Caller ID"),
                        "100",
                        "callerid",
                        "",
                        "",
                        "",
                        "",
                        "true",
                        "center",
                    ],
                    [
                        gettext("Called Number"),
                        "103",
                        "callednum",
                        "",
                        "",
                        "",
                        "",
                        "true",
                        "center",
                    ],
                    [
                        gettext("Code"),
                        "71",
                        "pattern",
                        "pattern",
                        "",
                        "get_only_numeric_val",
                        "",
                        "true",
                        "center",
                    ],
                    [
                        gettext("Destination"),
                        "90",
                        "notes",
                        "",
                        "",
                        "",
                        "",
                        "true",
                        "center",
                    ],
                    [
                        gettext("Duration"),
                        "80",
                        "billseconds",
                        "customer_cdr_list_search",
                        "billseconds",
                        "convert_to_show_in",
                        "",
                        "true",
                        "center",
                    ],
                    [
                        gettext("Debit($currency)"),
                        "75",
                        "debit",
                        "debit",
                        "debit",
                        "convert_to_currency",
                        "",
                        "true",
                        "right",
                    ],
                    [
                        gettext("Cost($currency)"),
                        "75",
                        "cost",
                        "cost",
                        "cost",
                        "convert_to_currency",
                        "",
                        "true",
                        "right",
                    ],
                    [
                        gettext("Disposition [Q.850]"),
                        "130",
                        "disposition",
                        "",
                        "",
                        "",
                        "",
                        "true",
                        "center",
                    ],
                    [
                        gettext("Account"),
                        "110",
                        "accountid",
                        "first_name,last_name,number",
                        "accounts",
                        "build_concat_string",
                        "",
                        "true",
                        "center",
                    ],
                    // array("Trunk", "90", "trunk_id", "name", "trunks", "get_field_name","","true","center"),
                    [
                        gettext("Rate Group"),
                        "159",
                        "pricelist_id",
                        "name",
                        "pricelists",
                        "get_field_name",
                        "",
                        "true",
                        "center",
                    ],
                    [
                        gettext("Call Type"),
                        "112",
                        "calltype",
                        "",
                        "",
                        "",
                    ],
                ]
            // $recording,
            );
        }

        return $grid_field_arr;
    }

    /**
     * ***************************
     */
    function build_report_list_for_reseller()
    {
        $account_info = $accountinfo = $this->CI->session->userdata('accountinfo');
        $currency_id = $account_info ['currency_id'];
        $currency = $this->CI->common->get_field_name('currency', 'currency', $currency_id);

        // array(display name, width, db_field_parent_table,feidname, db_field_child_table,function name);
        $grid_field_arr = json_encode(
            [
                [
                    gettext("Date"),
                    "100",
                    "callstart",
                    "callstart",
                    "callstart",
                    "convert_GMT_to",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Caller ID"),
                    "100",
                    "callerid",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Called Number"),
                    "120",
                    "callednum",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Code"),
                    "80",
                    "pattern",
                    "pattern",
                    "",
                    "get_only_numeric_val",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Destination"),
                    "120",
                    "notes",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Duration"),
                    "107",
                    "billseconds",
                    "reseller_cdr_list_search",
                    "billseconds",
                    "convert_to_show_in",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Debit($currency)"),
                    "105",
                    "debit",
                    "debit",
                    "debit",
                    "convert_to_currency",
                    "",
                    "true",
                    "right",
                ],
                [
                    gettext("Cost($currency)"),
                    "104",
                    "cost",
                    "cost",
                    "cost",
                    "convert_to_currency",
                    "",
                    "true",
                    "right",
                ],
                [
                    gettext("Disposition [Q.850]"),
                    "100",
                    "disposition",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Account"),
                    "120",
                    "accountid",
                    "first_name,last_name,number",
                    "accounts",
                    "build_concat_string",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Rate Group"),
                    "90",
                    "pricelist_id",
                    "name",
                    "pricelists",
                    "get_field_name",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Call Type"),
                    "120",
                    "calltype",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "center",
                ],
            ]
        );

        return $grid_field_arr;
    }

    function build_report_list_for_provider()
    {
        $account_info = $accountinfo = $this->CI->session->userdata('accountinfo');
        $currency_id = $account_info ['currency_id'];
        $currency = $this->CI->common->get_field_name('currency', 'currency', $currency_id);

        // array(display name, width, db_field_parent_table,feidname, db_field_child_table,function name);
        $grid_field_arr = json_encode(
            [
                [
                    "Date",
                    "100",
                    "callstart",
                    "callstart",
                    "callstart",
                    "convert_GMT_to",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Caller ID"),
                    "120",
                    "callerid",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Called Number"),
                    "160",
                    "callednum",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Code"),
                    "117",
                    "pattern",
                    "pattern",
                    "",
                    "get_only_numeric_val",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Destination"),
                    "130",
                    "notes",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Duration"),
                    "110",
                    "billseconds",
                    "provider_cdr_list_search",
                    "billseconds",
                    "convert_to_show_in",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Cost($currency)"),
                    "150",
                    "provider_call_cost",
                    "provider_cost",
                    "provider_cost",
                    "convert_to_currency",
                    "",
                    "true",
                    "right",
                ],
                [
                    gettext("Disposition [Q.850]"),
                    "200",
                    "disposition",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Account"),
                    "181",
                    "provider_id",
                    "first_name,last_name,number",
                    "accounts",
                    "build_concat_string",
                    "",
                    "true",
                    "center",
                ],
            ]
        );

        return $grid_field_arr;
    }

    function build_grid_customer()
    {
        $buttons_json = json_encode(
            [
                [
                    gettext("Export"),
                    "btn btn-xing",
                    " fa fa-download fa-lg",
                    "button_action",
                    "/reports/customerReport_export/",
                    'single',
                ],
            ]
        );

        return $buttons_json;
    }

    /**
     * ASTPP 3.0
     * For Customer CDRs export
     * *
     */
    function build_grid_buttons_user()
    {
        $buttons_json = json_encode(
            [
                [
                    gettext("Export"),
                    "btn btn-xing",
                    " fa fa-download fa-lg",
                    "button_action",
                    "/user/user_report_export/",
                    'single',
                ],
            ]
        );

        return $buttons_json;
    }

    /**
     * **************************************************************
     */
    function build_grid_buttons_reseller()
    {
        $buttons_json = json_encode(
            [
                [
                    gettext("Export"),
                    "btn btn-xing",
                    " fa fa-download fa-lg",
                    "button_action",
                    "/reports/resellerReport_export/",
                    'single',
                ],
            ]
        );

        return $buttons_json;
    }

    function build_grid_buttons_provider()
    {
        $buttons_json = json_encode(
            [
                [
                    gettext("Export"),
                    "btn btn-xing",
                    " fa fa-download fa-lg",
                    "button_action",
                    "/reports/providerReport_export/",
                    'single',
                ],
            ]
        );

        return $buttons_json;
    }

    function build_report_list_for_user($accounttype = 'customer')
    {
        $account_info = $accountinfo = $this->CI->session->userdata('accountinfo');
        $currency_id = $account_info ['currency_id'];
        $currency = $this->CI->common->get_field_name('currency', 'currency', $currency_id);

        if ($accounttype == 'customer' || $accounttype == 'reseller') {
            $cost_array = [
                "Debit($currency)",
                "100",
                "debit",
                "debit",
                "debit",
                "convert_to_currency",
                "",
                "true",
                "right",
            ];
        }
        if (strtolower($accounttype) == 'provider') {
            $cost_array = [
                "Debit($currency)",
                "140",
                "cost",
                "cost",
                "cost",
                "convert_to_currency",
            ];
        }
        // array(display name, width, db_field_parent_table,feidname, db_field_child_table,function name);
        $grid_field_arr = json_encode(
            [
                [
                    gettext("Date"),
                    "130",
                    "callstart",
                    "callstart",
                    "callstart",
                    "convert_GMT_to",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Caller ID"),
                    "100",
                    "callerid",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Called Number"),
                    "120",
                    "callednum",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Destination"),
                    "135",
                    "notes",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "center",
                ],
                // array("Account Number", "120", "accountid", "number", "accounts", "get_field_name"),
                [
                    gettext("Duration"),
                    "120",
                    "billseconds",
                    "user_cdrs_report_search",
                    "billseconds",
                    "convert_to_show_in",
                    "",
                    "true",
                    "center",
                ],
                $cost_array,
                [
                    gettext("Disposition [Q.850]"),
                    "160",
                    "disposition",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Call Type"),
                    "140",
                    "calltype",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "center",
                ],
            ]
        );

        return $grid_field_arr;
    }

    /**
     * ****
     * ASTPP 3.0
     * Payment to refill
     * *****
     */
    function build_refill_report_for_admin()
    {
        $account_info = $accountinfo = $this->CI->session->userdata('accountinfo');
        $currency_id = $account_info ['currency_id'];
        $currency = $this->CI->common->get_field_name('currency', 'currency', $currency_id);
        $grid_field_arr = json_encode(
            [
                [
                    gettext("Date"),
                    "225",
                    "payment_date",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Account"),
                    "240",
                    "accountid",
                    "first_name,last_name,number",
                    "accounts",
                    "build_concat_string",
                    "",
                    "true",
                    "center",
                ],
                [
                    "Amount($currency)",
                    "250",
                    "credit",
                    "credit",
                    "credit",
                    "convert_to_currency",
                    "",
                    "true",
                    "right",
                ],
                // array(gettext("Refill By"), "230", "payment_by", "payment_by", "payment_by", "get_refill_by","","true","center"),
                [
                    gettext("Refill By"),
                    "230",
                    "payment_by",
                    "first_name,last_name,number",
                    "accounts",
                    "get_refill_by",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Note"),
                    "327",
                    "notes",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "center",
                ],
            ]
        );

        return $grid_field_arr;
    }

    function build_refillreport_buttons()
    {
        $buttons_json = json_encode(
            [
                [
                    gettext("Export"),
                    "btn btn-xing",
                    " fa fa-download fa-lg",
                    "button_action",
                    "/reports/refillreport_export/",
                    'single',
                ],
            ]
        );

        return $buttons_json;
    }

    function build_search_refill_report_for_admin()
    {
        $accountinfo = $this->CI->session->userdata('accountinfo');
        $reseller_id = $accountinfo ['type'] == 1 ? $accountinfo ['id'] : 0;
        $form ['forms'] = [
            "",
            [
                'id' => "cdr_refill_search",
            ],
        ];
        $account_data = $this->CI->session->userdata("accountinfo");
        $acc_arr = [
            'Account',
            'accountid',
            'SELECT',
            '',
            '',
            'tOOL TIP',
            'Please Enter account number',
            'id',
            'IF(`deleted`=1,concat( first_name, " ", last_name, " ", "(", number, ")^" ),concat( first_name, " ", last_name, " ", "(", number, ")" )) as number',
            'accounts',
            'build_dropdown_deleted',
            'where_arr',
            [
                "reseller_id" => $reseller_id,
                "type"        => "0,1,3",
            ],
        ];
        $logintype = $this->CI->session->userdata('logintype');
        if ($logintype == 1 || $logintype == 5) {
            $account_data = $this->CI->session->userdata("accountinfo");
            $loginid = $account_data ['id'];
        } else {
            $loginid = "0";
        }
        if ($logintype == 0 || $logintype == 3) {
            $acc_arr = null;
        }
        $form [gettext('Search')] = [
            [
                gettext('From Date'),
                'INPUT',
                [
                    'name'  => 'payment_date[]',
                    'id'    => 'refill_from_date',
                    'size'  => '20',
                    'class' => "text field ",
                ],
                '',
                'tOOL TIP',
                '',
                'payment_date[payment_date-date]',
            ],
            [
                gettext('To Date'),
                'INPUT',
                [
                    'name'  => 'payment_date[]',
                    'id'    => 'refill_to_date',
                    'size'  => '20',
                    'class' => "text field ",
                ],
                '',
                'tOOL TIP',
                '',
                'payment_date[payment_date-date]',
            ],
            $acc_arr,
            // array('Account', 'accountid', 'SELECT', '', '', 'tOOL TIP', 'Please Enter account number', 'id', 'first_name,last_name,number', 'accounts', 'build_concat_dropdown', 'where_arr', array("reseller_id" => "0","type"=>"0")),
            [
                gettext('Amount'),
                'INPUT',
                [
                    'name'  => 'credit[credit]',
                    'value' => '',
                    'size'  => '20',
                    'class' => "text field",
                ],
                '',
                'Tool tips info',
                '1',
                'credit[credit-integer]',
                '',
                '',
                '',
                'search_int_type',
                '',
            ],
            // array('', 'HIDDEN', 'ajax_search', '1', '', '', ''),array('', 'HIDDEN', 'advance_search', '1', '', '', ''),
            [
                '',
                'HIDDEN',
                'ajax_search',
                '1',
                '',
                '',
                '',
            ],
            [
                '',
                'HIDDEN',
                'advance_search',
                '1',
                '',
                '',
                '',
            ],
        ];

        $form ['button_search'] = [
            'name'    => 'action',
            'id'      => "cusotmer_cdr_refill_search_btn",
            'content' => gettext('Search'),
            'value'   => 'save',
            'type'    => 'button',
            'class'   => 'btn btn-line-parrot pull-right',
        ];
        $form ['button_reset'] = [
            'name'    => 'action',
            'id'      => "id_reset",
            'content' => gettext('Clear'),
            'value'   => 'Cancel',
            'type'    => 'reset',
            'class'   => 'btn btn-line-sky pull-right margin-x-10',
        ];

        // echo '<pre>'; print_r($form); exit;
        return $form;
    }

    /**
     * ************************
     */
    function build_commission_report_for_admin()
    {
        $grid_field_arr = json_encode(
            [
                [
                    gettext("Account"),
                    "150",
                    "accountid",
                    "first_name,last_name,number",
                    "accounts",
                    "build_concat_string",
                ],
                [
                    gettext("Amount"),
                    "150",
                    "amount",
                    "credit",
                    "credit",
                    "convert_to_currency",
                ],
                [
                    gettext("Description"),
                    "150",
                    "description",
                    "",
                    "",
                    "",
                ],
                [
                    gettext("Reseller"),
                    "150",
                    "reseller_id",
                    "first_name,last_name,number",
                    "accounts",
                    "build_concat_string",
                ],
                [
                    gettext("Commission Rate(%)"),
                    "150",
                    "commission_percent",
                    "",
                    "",
                    "",
                ],
                [
                    gettext("Date"),
                    "150",
                    "date",
                    "",
                    "",
                    "",
                ],
            ]
        );

        return $grid_field_arr;
    }

    function reseller_commission_search_form()
    {
        $form ['forms'] = [
            "",
            [
                'id' => "reseller_commission_search",
            ],
        ];
        /**
         * ****
         * ASTPP 3.0
         * Payment to refill
         * *****
         */
        $form ['User Refill Report'] = [
            /**
             * **********************
             */
            [
                '',
                'HIDDEN',
                'ajax_search',
                '1',
                '',
                '',
                '',
            ],
            [
                '',
                'HIDDEN',
                'advance_search',
                '1',
                '',
                '',
                '',
            ],
            [
                gettext('From Date'),
                'INPUT',
                [
                    'name'  => 'date[]',
                    'id'    => 'commission_from_date',
                    'size'  => '20',
                    'class' => "text field ",
                ],
                '',
                'tOOL TIP',
                '',
                'start_date[start_date-date]',
            ],
            [
                gettext('To Date'),
                'INPUT',
                [
                    'name'  => 'date[]',
                    'id'    => 'commission_to_date',
                    'size'  => '20',
                    'class' => "text field ",
                ],
                '',
                'tOOL TIP',
                '',
                'end_date[end_date-date]',
            ],
            [
                gettext('Account'),
                'accountid',
                'SELECT',
                '',
                '',
                'tOOL TIP',
                'Please Enter account number',
                'id',
                'first_name,last_name,number',
                'accounts',
                'build_concat_dropdown',
                'where_arr',
                [
                    "reseller_id" => "0",
                    "type"        => "1",
                    "deleted"     => "0",
                ],
            ],
            [
                gettext('Amount'),
                'INPUT',
                [
                    'name'  => 'amount[amount]',
                    'value' => '',
                    'size'  => '20',
                    'class' => "text field",
                ],
                '',
                'Tool tips info',
                '1',
                'amount[amount-integer]',
                '',
                '',
                '',
                'search_int_type',
                '',
            ],
        ];

        $form ['button_search'] = [
            'name'    => 'action',
            'id'      => "commission_search_btn",
            'content' => gettext('Search'),
            'value'   => 'save',
            'type'    => 'button',
            'class'   => 'ui-state-default float-right ui-corner-all ui-button',
        ];
        $form ['button_reset'] = [
            'name'    => 'action',
            'id'      => "id_reset",
            'content' => gettext('Clear Search Filter'),
            'value'   => 'cancel',
            'type'    => 'reset',
            'class'   => 'ui-state-default float-right ui-corner-all ui-button',
        ];

        return $form;
    }

    function get_providersummary_search_form()
    {
        $form ['forms'] = [
            '',
            [
                'id' => "providersummary_search",
            ],
        ];
        $form [gettext('Search')] = [
            [
                gettext('From Date'),
                'INPUT',
                [
                    'name'  => 'callstart[]',
                    'id'    => 'customer_from_date',
                    'size'  => '20',
                    'class' => "text field ",
                ],
                '',
                'tOOL TIP',
                '',
                'start_date[start_date-date]',
            ],
            [
                gettext('To Date'),
                'INPUT',
                [
                    'name'  => 'callstart[]',
                    'id'    => 'customer_to_date',
                    'size'  => '20',
                    'class' => "text field ",
                ],
                '',
                'tOOL TIP',
                '',
                'end_date[end_date-date]',
            ],
            [
                gettext('Account'),
                'provider_id',
                'SELECT',
                '',
                '',
                'tOOL TIP',
                'Please Enter account number',
                'id',
                'IF(`deleted`=1,concat( first_name, " ", last_name, " ", "(", number, ")^" ),concat( first_name, " ", last_name, " ", "(", number, ")" )) as number',
                'accounts',
                'build_dropdown_deleted',
                'where_arr',
                [
                    "reseller_id" => "0",
                    "type"        => "3",
                ],
            ],
            [
                gettext('Code'),
                'INPUT',
                [
                    'name'  => 'pattern[pattern]',
                    'value' => '',
                    'size'  => '20',
                    'class' => "text field ",
                ],
                '',
                'Tool tips info',
                '1',
                'pattern[pattern-string]',
                '',
                '',
                '',
                'search_string_type',
                '',
            ],

            [
                gettext('Destination'),
                'INPUT',
                [
                    'name'  => 'notes[notes]',
                    'value' => '',
                    'size'  => '20',
                    'class' => "text field ",
                ],
                '',
                'Tool tips info',
                '1',
                'notes[notes-string]',
                '',
                '',
                '',
                'search_string_type',
                '',
            ],
            // array('Account Number', 'number', 'SELECT', '', '', 'tOOL TIP', 'Please Enter account number', 'id', 'first_name,last_name,number', 'accounts', 'build_concat_dropdown', 'where_arr', array("type"=>"3", "deleted" => "0")),
            [
                '',
                'HIDDEN',
                'ajax_search',
                '1',
                '',
                '',
                '',
            ],
            [
                '',
                'HIDDEN',
                'advance_search',
                '1',
                '',
                '',
                '',
            ],
        ];
        $form ['button_search'] = [
            'name'    => 'action',
            'id'      => "providersummary_search_btn",
            'content' => gettext('Search'),
            'value'   => 'save',
            'type'    => 'button',
            'class'   => 'btn btn-line-parrot pull-right',
        ];
        $form ['button_reset'] = [
            'name'    => 'action',
            'id'      => "id_reset",
            'content' => gettext('Clear'),
            'value'   => 'cancel',
            'type'    => 'reset',
            'class'   => 'btn btn-line-sky pull-right margin-x-10',
        ];

        return $form;
    }

    function build_providersummary()
    {
        $grid_field_arr = json_encode(
            [
                [
                    gettext("Provider"),
                    "220",
                    "provider_id",
                    "first_name,last_name,number",
                    "accounts",
                    "build_concat_string",
                ],
                [
                    gettext("Code"),
                    "120",
                    "pattern",
                    "pattern",
                    "",
                    "get_only_numeric_val",
                ],
                [
                    gettext("Destination"),
                    "150",
                    "notes",
                    "",
                    "",
                    "",
                ],
                [
                    gettext("Attempted Calls"),
                    "130",
                    "attempted_calls",
                    "",
                    "",
                    "",
                ],
                [
                    gettext("Completed Calls"),
                    "150",
                    "description",
                    "",
                    "",
                    "",
                ],
                [
                    gettext("ASR"),
                    "95",
                    "asr",
                    '',
                    '',
                    '',
                ],
                [
                    gettext("ACD"),
                    "95",
                    "acd  ",
                    '',
                    '',
                    '',
                ],
                [
                    gettext("MCD"),
                    "95",
                    "mcd",
                    '',
                    '',
                    '',
                ],
                [
                    gettext("Bilable"),
                    "100",
                    "billable",
                    '',
                    '',
                    '',
                ],
                [
                    gettext("Cost"),
                    "115",
                    "cost",
                    '',
                    '',
                    '',
                ],
            ]
        // array("Profit", "95", "profit", "", "", ""),
        );

        return $grid_field_arr;
    }

    function build_grid_buttons_providersummary()
    {
        $buttons_json = json_encode(
            [
                [
                    gettext("Export"),
                    "btn btn-xing",
                    " fa fa-download fa-lg",
                    "button_action",
                    "/reports/providersummary_export_cdr_xls",
                    'single',
                ],
            ]
        );

        return $buttons_json;
    }

    function get_resellersummary_search_form()
    {
        $form ['forms'] = [
            "",
            [
                'id' => "resellersummary_search",
            ],
        ];
        $form [gettext('Search')] = [
            [
                gettext('From Date'),
                'INPUT',
                [
                    'name'  => 'callstart[]',
                    'id'    => 'customer_from_date',
                    'size'  => '20',
                    'class' => "text field ",
                ],
                '',
                'tOOL TIP',
                '',
                'start_date[start_date-date]',
            ],
            [
                gettext('To Date'),
                'INPUT',
                [
                    'name'  => 'callstart[]',
                    'id'    => 'customer_to_date',
                    'size'  => '20',
                    'class' => "text field ",
                ],
                '',
                'tOOL TIP',
                '',
                'end_date[end_date-date]',
            ],
            [
                gettext('Account'),
                'reseller_id',
                'SELECT',
                '',
                '',
                'tOOL TIP',
                'Please Enter account number',
                'id',
                'IF(`deleted`=1,concat( first_name, " ", last_name, " ", "(", number, ")^" ),concat( first_name, " ", last_name, " ", "(", number, ")" )) as number',
                'accounts',
                'build_dropdown_deleted',
                'where_arr',
                [
                    "reseller_id" => "0",
                    "type"        => "1",
                ],
            ],
            [
                gettext('Code'),
                'INPUT',
                [
                    'name'  => 'pattern[pattern]',
                    'value' => '',
                    'size'  => '20',
                    'class' => "text field ",
                ],
                '',
                'Tool tips info',
                '1',
                'pattern[pattern-string]',
                '',
                '',
                '',
                'search_string_type',
                '',
            ],

            [
                gettext('Destination'),
                'INPUT',
                [
                    'name'  => 'notes[notes]',
                    'value' => '',
                    'size'  => '20',
                    'class' => "text field ",
                ],
                '',
                'Tool tips info',
                '1',
                'notes[notes-string]',
                '',
                '',
                '',
                'search_string_type',
                '',
            ],
            // array('Account Number', 'number', 'SELECT', '', '', 'tOOL TIP', 'Please Enter account number', 'id', 'first_name,last_name,number', 'accounts', 'build_concat_dropdown', 'where_arr', array("type"=>"1", "deleted" => "0")),
            [
                '',
                'HIDDEN',
                'ajax_search',
                '1',
                '',
                '',
                '',
            ],
            [
                '',
                'HIDDEN',
                'advance_search',
                '1',
                '',
                '',
                '',
            ],
        ];
        $form ['button_search'] = [
            'name'    => 'action',
            'id'      => "resellersummary_search_btn",
            'content' => gettext('Search'),
            'value'   => 'save',
            'type'    => 'button',
            'class'   => 'btn btn-line-parrot pull-right',
        ];
        $form ['button_reset'] = [
            'name'    => 'action',
            'id'      => "id_reset",
            'content' => gettext('Clear'),
            'value'   => 'cancel',
            'type'    => 'reset',
            'class'   => 'btn btn-line-sky pull-right margin-x-10',
        ];

        return $form;
    }

    function build_resellersummary()
    {
        $grid_field_arr = json_encode(
            [
                [
                    gettext("Account"),
                    "148",
                    "accountid",
                    "first_name,last_name,number",
                    "accounts",
                    "build_concat_string",
                ],
                [
                    gettext("Code"),
                    "120",
                    "pattern",
                    "pattern",
                    "",
                    "get_only_numeric_val",
                ],
                [
                    gettext("Destination"),
                    "150",
                    "notes",
                    "",
                    "",
                    "",
                ],
                [
                    gettext("Attempted Calls"),
                    "120",
                    "attempted_calls",
                    "",
                    "",
                    "",
                ],
                [
                    gettext("Completed Calls"),
                    "120",
                    "description",
                    "",
                    "",
                    "",
                ],
                [
                    gettext("ASR"),
                    "80",
                    "asr",
                    '',
                    '',
                    '',
                ],
                [
                    gettext("ACD"),
                    "80",
                    "acd  ",
                    '',
                    '',
                    '',
                ],
                [
                    gettext("MCD"),
                    "80",
                    "mcd",
                    '',
                    '',
                    '',
                ],
                [
                    gettext("Bilable"),
                    "90",
                    "billable",
                    '',
                    '',
                    '',
                ],
                [
                    gettext("Price"),
                    "90",
                    "price",
                    '',
                    '',
                    '',
                ],
                [
                    gettext("Cost"),
                    "90",
                    "cost",
                    '',
                    '',
                    '',
                ],
                [
                    gettext("Profit"),
                    "100",
                    "profit",
                    "",
                    "",
                    "",
                ],
            ]
        );

        return $grid_field_arr;
    }

    function build_grid_buttons_resellersummary()
    {
        $buttons_json = json_encode(
            [
                [
                    gettext("Export"),
                    "btn btn-xing",
                    " fa fa-download fa-lg",
                    "button_action",
                    "/reports/resellersummary_export_cdr_xls",
                    'single',
                ],
            ]
        );

        return $buttons_json;
    }

    function get_customersummary_search_form()
    {
        $form ['forms'] = [
            "",
            [
                'id' => "customersummary_search",
            ],
        ];
        $form [gettext('Search')] = [
            [
                gettext('From Date'),
                'INPUT',
                [
                    'name'  => 'callstart[]',
                    'id'    => 'customer_from_date',
                    'size'  => '20',
                    'class' => "text field ",
                ],
                '',
                'tOOL TIP',
                '',
                'start_date[start_date-date]',
            ],
            [
                gettext('To Date'),
                'INPUT',
                [
                    'name'  => 'callstart[]',
                    'id'    => 'customer_to_date',
                    'size'  => '20',
                    'class' => "text field ",
                ],
                '',
                'tOOL TIP',
                '',
                'end_date[end_date-date]',
            ],
            [
                gettext('Account'),
                'accountid',
                'SELECT',
                '',
                '',
                'tOOL TIP',
                'Please Enter account number',
                'id',
                'IF(`deleted`=1,concat( first_name, " ", last_name, " ", "(", number, ")^" ),concat( first_name, " ", last_name, " ", "(", number, ")" )) as number',
                'accounts',
                'build_dropdown_deleted',
                'where_arr',
                [
                    "reseller_id" => "0",
                    "type"        => "GLOBAL",
                ],
            ],
            [
                gettext('Code'),
                'INPUT',
                [
                    'name'  => 'pattern[pattern]',
                    'value' => '',
                    'size'  => '20',
                    'class' => "text field ",
                ],
                '',
                'Tool tips info',
                '1',
                'pattern[pattern-string]',
                '',
                '',
                '',
                'search_string_type',
                '',
            ],

            [
                gettext('Destination'),
                'INPUT',
                [
                    'name'  => 'notes[notes]',
                    'value' => '',
                    'size'  => '20',
                    'class' => "text field ",
                ],
                '',
                'Tool tips info',
                '1',
                'notes[notes-string]',
                '',
                '',
                '',
                'search_string_type',
                '',
            ],
            // array('Account Number', 'number', 'SELECT', '', '', 'tOOL TIP', 'Please Enter account number', 'id', 'first_name,last_name,number', 'accounts', 'build_concat_dropdown', 'where_arr', array("type"=>"0", "deleted" => "0")),
            [
                '',
                'HIDDEN',
                'ajax_search',
                '1',
                '',
                '',
                '',
            ],
            [
                '',
                'HIDDEN',
                'advance_search',
                '1',
                '',
                '',
                '',
            ],
        ];
        $form ['button_search'] = [
            'name'    => 'action',
            'id'      => "customersummary_search_btn",
            'content' => gettext('Search'),
            'value'   => 'save',
            'type'    => 'button',
            'class'   => 'btn btn-line-parrot pull-right',
        ];
        $form ['button_reset'] = [
            'name'    => 'action',
            'id'      => "id_reset",
            'content' => gettext('Clear'),
            'value'   => 'cancel',
            'type'    => 'reset',
            'class'   => 'btn btn-line-sky pull-right margin-x-10',
        ];

        return $form;
    }

    function build_customersummary()
    {
        $grid_field_arr = json_encode(
            [
                [
                    gettext("Account"),
                    "190",
                    "accountid",
                    "first_name,last_name,number",
                    "accounts",
                    "build_concat_string",
                ],
                [
                    gettext("Code"),
                    "80",
                    "pattern",
                    "pattern",
                    "",
                    "get_only_numeric_val",
                ],
                [
                    gettext("Destination"),
                    "110",
                    "notes",
                    "",
                    "",
                    "",
                ],
                [
                    gettext("Attempted Calls"),
                    "140",
                    "attempted_calls",
                    "",
                    "",
                    "",
                ],
                [
                    gettext("Completed Calls"),
                    "130",
                    "description",
                    "",
                    "",
                    "",
                ],
                [
                    gettext("ASR"),
                    "70",
                    "asr",
                    '',
                    '',
                    '',
                ],
                [
                    gettext("ACD"),
                    "70",
                    "acd  ",
                    '',
                    '',
                    '',
                ],
                [
                    gettext("MCD"),
                    "80",
                    "mcd",
                    '',
                    '',
                    '',
                ],
                [
                    gettext("Bilable"),
                    "80",
                    "billable",
                    '',
                    '',
                    '',
                ],
                [
                    gettext("Debit"),
                    "85",
                    "cost",
                    '',
                    '',
                    '',
                ],
                [
                    gettext("Cost"),
                    "110",
                    "price",
                    '',
                    '',
                    '',
                ],
                [
                    gettext("Profit"),
                    "123",
                    "profit",
                    "",
                    "",
                    "",
                ],
            ]
        );

        return $grid_field_arr;
    }

    function build_grid_buttons_customersummary()
    {
        $buttons_json = json_encode(
            [
                [
                    gettext("Export"),
                    "btn btn-xing",
                    " fa fa-download fa-lg",
                    "button_action",
                    "/reports/customersummary_export_cdr_xls",
                    'single',
                ],
            ]
        );

        return $buttons_json;
    }

    /**
     * *******
     * ASTPP 3.0 .1
     * Charges History
     * ********
     */
    function build_charge_list_for_admin()
    {
        $account_info = $accountinfo = $this->CI->session->userdata('accountinfo');
        $currency_id = $account_info ['currency_id'];
        $currency = $this->CI->common->get_field_name('currency', 'currency', $currency_id);
        if ($this->CI->session->userdata("logintype") == '1') {
            $grid_field_arr = json_encode(
                [
                    [
                        gettext("Created Date"),
                        "120",
                        "created_date",
                        "",
                        "",
                        "",
                        "",
                        "true",
                        "center",
                    ],
                    [
                        gettext("Invoice Number"),
                        "120",
                        "created_date",
                        "",
                        "",
                        "",
                        "",
                        "true",
                        "center",
                    ],
                    [
                        gettext("Account"),
                        "120",
                        "accountid",
                        "first_name,last_name,number",
                        "accounts",
                        "build_concat_string",
                        "",
                        "true",
                        "center",
                    ],
                    // array("Reseller", "120", "reseller_id", "first_name,last_name,number", "accounts", "reseller_select_value"),
                    [
                        gettext("Charge Type"),
                        "120",
                        "item_type",
                        "",
                        "",
                        "",
                        "",
                        "true",
                        "center",
                    ],
                    [
                        gettext("Before Balance<br/>($currency)"),
                        "120",
                        "before_balance",
                        "before_balance",
                        "before_balance",
                        "convert_to_currency",
                        "",
                        "true",
                        "right",
                    ],
                    [
                        gettext("Debit (-)<br/>($currency)"),
                        "110",
                        "debit",
                        "debit",
                        "debit",
                        "convert_to_currency",
                        "",
                        "true",
                        "right",
                    ],
                    [
                        gettext("Credit (+)<br/>($currency)"),
                        "110",
                        "credit",
                        "credit",
                        "credit",
                        "convert_to_currency",
                        "",
                        "true",
                        "right",
                    ],
                    [
                        gettext("After Balance<br/>($currency)"),
                        "120",
                        "after_balance",
                        "after_balance",
                        "after_balance",
                        "convert_to_currency",
                        "",
                        "true",
                        "right",
                    ],
                    [
                        gettext("Description"),
                        "300",
                        "description",
                        "",
                        "",
                        "",
                        "",
                        "true",
                        "center",
                    ],
                ]
            );
        } else {
            $grid_field_arr = json_encode(
                [
                    [
                        gettext("Created Date"),
                        "120",
                        "created_date",
                        "",
                        "",
                        "",
                        "",
                        "true",
                        "center",
                    ],
                    [
                        gettext("Invoice Number"),
                        "120",
                        "created_date",
                        "",
                        "",
                        "",
                        "",
                        "true",
                        "center",
                    ],
                    [
                        gettext("Account"),
                        "120",
                        "accountid",
                        "first_name,last_name,number",
                        "accounts",
                        "build_concat_string",
                        "",
                        "true",
                        "center",
                    ],
                    [
                        gettext("Charge Type"),
                        "120",
                        "item_type",
                        "",
                        "",
                        "",
                        "",
                        "true",
                        "center",
                    ],
                    [
                        gettext("Before Balance<br/>($currency)"),
                        "120",
                        "before_balance",
                        "before_balance",
                        "before_balance",
                        "convert_to_currency",
                        "",
                        "true",
                        "right",
                    ],
                    [
                        gettext("Debit (-)<br/>($currency)"),
                        "110",
                        "debit",
                        "debit",
                        "debit",
                        "convert_to_currency",
                        "",
                        "true",
                        "right",
                    ],
                    [
                        gettext("Credit (+)<br/>($currency)"),
                        "110",
                        "credit",
                        "credit",
                        "credit",
                        "convert_to_currency",
                        "",
                        "true",
                        "right",
                    ],
                    [
                        gettext("After Balance<br/>($currency)"),
                        "120",
                        "after_balance",
                        "after_balance",
                        "after_balance",
                        "convert_to_currency",
                        "",
                        "true",
                        "right",
                    ],
                    [
                        gettext("Description"),
                        "300",
                        "description",
                        "",
                        "",
                        "",
                        "",
                        "true",
                        "center",
                    ],
                ]
            );
        }

        return $grid_field_arr;
    }

    function get_charges_search_form()
    {
        $accountinfo = $this->CI->session->userdata('accountinfo');
        $reseller_id = $accountinfo ['type'] == 1 ? $accountinfo ['id'] : 0;
        $form ['forms'] = [
            "",
            [
                'id' => "charges_search",
            ],
        ];
        $form [gettext('Search')] = [
            [
                gettext('From Date'),
                'INPUT',
                [
                    'name'  => 'created_date[]',
                    'id'    => 'charge_from_date',
                    'size'  => '20',
                    'class' => "text field ",
                ],
                '',
                'tOOL TIP',
                '',
                'start_date[start_date-date]',
            ],
            [
                gettext('To Date'),
                'INPUT',
                [
                    'name'  => 'created_date[]',
                    'id'    => 'charge_to_date',
                    'size'  => '20',
                    'class' => "text field ",
                ],
                '',
                'tOOL TIP',
                '',
                'end_date[end_date-date]',
            ],
            [
                gettext('Account'),
                'accountid',
                'SELECT',
                '',
                '',
                'tOOL TIP',
                'Please Enter account number',
                'id',
                'IF(`deleted`=1,concat( first_name, " ", last_name, " ", "(", number, ")^" ),concat( first_name, " ", last_name, " ", "(", number, ")" )) as number',
                'accounts',
                'build_dropdown_deleted',
                'where_arr',
                [
                    "reseller_id" => $reseller_id,
                    "type"        => "GLOBAL",
                ],
            ],
            [
                gettext('Debit'),
                'INPUT',
                [
                    'name'  => 'debit[debit]',
                    'value' => '',
                    'size'  => '20',
                    'class' => "text field ",
                ],
                '',
                'Tool tips info',
                '1',
                'debit[debit-integer]',
                '',
                '',
                '',
                'search_int_type',
                '',
            ],
            [
                gettext('Credit'),
                'INPUT',
                [
                    'name'  => 'credit[credit]',
                    'value' => '',
                    'size'  => '20',
                    'class' => "text field ",
                ],
                '',
                'Tool tips info',
                '1',
                'credit[credit-integer]',
                '',
                '',
                '',
                'search_int_type',
                '',
            ],

            [
                '',
                'HIDDEN',
                'ajax_search',
                '1',
                '',
                '',
                '',
            ],
            [
                '',
                'HIDDEN',
                'advance_search',
                '1',
                '',
                '',
                '',
            ],
        ];
        $form ['button_search'] = [
            'name'    => 'action',
            'id'      => "charges_search_btn",
            'content' => gettext('Search'),
            'value'   => 'save',
            'type'    => 'button',
            'class'   => 'btn btn-line-parrot pull-right',
        ];
        $form ['button_reset'] = [
            'name'    => 'action',
            'id'      => "id_reset",
            'content' => gettext('Clear'),
            'value'   => 'cancel',
            'type'    => 'reset',
            'class'   => 'btn btn-line-sky pull-right margin-x-10',
        ];

        return $form;
    }
    /**
     * ****************************
     */
    /**
     * *******
     * ASTPP 3.0
     * Charges History
     * *******
     */
    function build_charge_list_for_customer()
    {
        $account_info = $accountinfo = $this->CI->session->userdata('accountinfo');
        $currency_id = $account_info ['currency_id'];
        $currency = $this->CI->common->get_field_name('currency', 'currency', $currency_id);

        $grid_field_arr = json_encode(
            [
                [
                    gettext("Created Date"),
                    "100",
                    "created_date",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Invoice Number"),
                    "110",
                    "created_date",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Charge Type"),
                    "100",
                    "item_type",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Before Balance<br/>($currency)"),
                    "120",
                    "before_balance",
                    "before_balance",
                    "before_balance",
                    "convert_to_currency",
                    "",
                    "true",
                    "right",
                ],
                [
                    gettext("Debit (-)<br/>($currency)"),
                    "110",
                    "debit",
                    "debit",
                    "debit",
                    "convert_to_currency",
                    "",
                    "true",
                    "right",
                ],
                [
                    gettext("Credit (+)<br/>($currency)"),
                    "110",
                    "credit",
                    "credit",
                    "credit",
                    "convert_to_currency",
                    "",
                    "true",
                    "right",
                ],
                [
                    gettext("After Balance<br/>($currency)"),
                    "120",
                    "after_balance",
                    "after_balance",
                    "after_balance",
                    "convert_to_currency",
                    "",
                    "true",
                    "right",
                ],
                [
                    gettext("Description"),
                    "270",
                    "description",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "center",
                ],
            ]
        );

        return $grid_field_arr;
    }

    /**
     * *******
     * ASTPP 3.0
     * Refill History
     * *******
     */
    function build_refillreport_for_customer()
    {
        $account_info = $accountinfo = $this->CI->session->userdata('accountinfo');
        $currency_id = $account_info ['currency_id'];
        $currency = $this->CI->common->get_field_name('currency', 'currency', $currency_id);

        $grid_field_arr = json_encode(
            [
                [
                    gettext("Date"),
                    "225",
                    "payment_date",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Amount($currency)"),
                    "250",
                    "credit",
                    "credit",
                    "credit",
                    "convert_to_currency",
                    "",
                    "true",
                    "right",
                ],
                [
                    gettext("Refill By"),
                    "230",
                    "payment_by",
                    "payment_by",
                    "payment_by",
                    "get_refill_by",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Note"),
                    "325",
                    "notes",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "center",
                ],
            ]
        );

        return $grid_field_arr;
    }
}

?>

