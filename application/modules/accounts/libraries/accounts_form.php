<?php
// ##############################################################################
// ASTPP - Open Source VoIP Billing Solution
//
// Copyright (C) 2016 iNextrix Technologies Pvt. Ltd.
// Samir Doshi <samir.doshi@inextrix.com>
// ASTPP Version 3.0 and above
// License https://www.gnu.org/licenses/agpl-3.0.html
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
// ##############################################################################
if (!defined('BASEPATH')) {
    exit ('No direct script access allowed');
}

class Accounts_form
{
    function __construct($library_name = '')
    {
        $this->CI = &get_instance();
    }

    function get_customer_form_fields($entity_type = false, $id = false)
    {
        $expiry_date = gmdate('Y-m-d H:i:s', strtotime('+10 years'));
        $readable = false;
        $type = $entity_type == 'customer' ? 0 : 3;
        $uname = $this->CI->common->find_uniq_rendno_customer(
            common_model::$global_config ['system_config'] ['cardlength'],
            'number',
            'accounts'
        );
        $pin = Common_model::$global_config ['system_config'] ['pinlength'];
        $pin_number = $this->CI->common->find_uniq_rendno($pin, 'pin', 'accounts');
        $uname_user = $this->CI->common->find_uniq_rendno('10', 'number', 'accounts');
        $password = $this->CI->common->generate_password();
        $logintype = $this->CI->session->userdata('logintype');
        if ($logintype == 1 || $logintype == 5) {
            $account_data = $this->CI->session->userdata("accountinfo");
            $loginid = $account_data ['id'];
        } else {
            $loginid = "0";
        }
        $sip_device = null;
        $opensips_device = null;
        if (!$entity_type) {
            $entity_type = 'customer';
        }
        $params = [
            'name'     => 'number',
            'value'    => $uname,
            'size'     => '20',
            'class'    => "text field medium",
            'id'       => 'number',
            'readonly' => true,
        ];

        if ($id > 0) {
            $readable = 'disabled';
            $val = 'accounts.email.'.$id;
            $account_val = 'accounts.number.'.$id;
            $password = [
                'Password',
                'PASSWORD',
                [
                    'name'        => 'password',
                    'id'          => 'password_show',
                    'onmouseover' => 'seetext(password_show)',
                    'onmouseout'  => 'hidepassword(password_show)',
                    'size'        => '20',
                    'class'       => "text field medium",
                ],
                'required|',
                'tOOL TIP',
                '',
            ];
            $balance = [
                'Balance',
                'INPUT',
                [
                    'name'     => 'balance',
                    'size'     => '20',
                    'readonly' => true,
                    'class'    => "text field medium",
                ],
                '',
                'tOOL TIP',
                '',
            ];
            $account = [
                'Account',
                'INPUT',
                $params,
                'required|integer|greater_than[0]|is_unique['.$account_val.']',
                'tOOL TIP',
                '',
            ];
            /* * ******************************** */
        } else {
            $val = 'accounts.email';
            $account_val = 'accounts.number';
            if (common_model::$global_config ['system_config'] ['opensips'] == 0) {
                $sip_device = [
                    'Create Opensips Device',
                    'opensips_device_flag',
                    'SELECT',
                    '',
                    '',
                    'tOOL TIP',
                    'Please Enter account number',
                    '',
                    '',
                    '',
                    'set_prorate',
                ];
            } else {
                $sip_device = [
                    'Create SIP Device',
                    'sip_device_flag',
                    'SELECT',
                    '',
                    '',
                    'tOOL TIP',
                    'Please Enter account number',
                    '',
                    '',
                    '',
                    'set_prorate',
                ];
            }
            $account = [
                'Account',
                'INPUT',
                $params,
                'required|integer|greater_than[0]|is_unique['.$account_val.']',
                'tOOL TIP',
                '',
                '<i style="cursor:pointer; font-size: 17px; padding-left:10px; padding-top:6px;color: #1bcb61;" title="Generate Account" class="change_number fa fa-refresh" ></i>',
            ];
            $password = [
                'Password',
                'INPUT',
                [
                    'name'  => 'password',
                    'value' => $password,
                    'size'  => '20',
                    'class' => "text field medium",
                    'id'    => 'password',
                ],
                'required|',
                'tOOL TIP',
                '',
                '<i style="cursor:pointer; font-size: 17px; padding-left:10px; padding-top:6px;color: #1bcb61;" title="Reset Password" class="change_pass fa fa-refresh" ></i>',
            ];
            $balance = [
                'Balance',
                'INPUT',
                [
                    'name'  => 'balance',
                    'size'  => '20',
                    'class' => "text field medium",
                ],
                '',
                'tOOL TIP',
                '',
            ];
        }
        $pin = [
            'Pin',
            'INPUT',
            [
                'name'  => 'pin',
                'value' => $pin_number,
                'size'  => '20',
                'class' => "text field medium",
                'id'    => 'change_pin',
            ],
            'is_numeric',
            'tOOL TIP',
            '',
            '<i style="cursor:pointer; font-size: 17px; padding-left:10px; padding-top:6px;color: #1bcb61;" title="Generate Pin" class="change_pin fa fa-refresh" ></i>',
        ];
        $form ['forms'] = [
            base_url().'accounts/'.$entity_type.'_save/'.$id."/",
            [
                "id"   => "customer_form",
                "name" => "customer_form",
            ],
        ];
        $form [gettext('Account Profile')] = [
            [
                '',
                'HIDDEN',
                [
                    'name' => 'id',
                ],
                '',
                '',
                '',
                '',
            ],
            [
                '',
                'HIDDEN',
                [
                    'name'  => 'type',
                    'value' => $type,
                ],
                '',
                '',
                '',
            ],
            $account,
            $password,
            $pin,
            [
                gettext('First Name'),
                'INPUT',
                [
                    'name'  => 'first_name',
                    'id'    => 'first_name',
                    'size'  => '15',
                    'class' => "text field medium",
                ],
                'required',
                'tOOL TIP',
                '',
            ],
            [
                gettext('Last Name'),
                'INPUT',
                [
                    'name'  => 'last_name',
                    'size'  => '15',
                    'class' => "text field medium",
                ],
                'trim|xss_clean',
                'tOOL TIP',
                '',
            ],
            [
                gettext('Company'),
                'INPUT',
                [
                    'name'  => 'company_name',
                    'size'  => '15',
                    'class' => 'text field medium',
                ],
                'trim|xss_clean',
                'tOOL TIP',
                '',
            ],
            [
                gettext('Phone'),
                'INPUT',
                [
                    'name'  => 'telephone_1',
                    'size'  => '15',
                    'class' => "text field medium",
                ],
                'phn_number',
                'tOOL TIP',
                '',
            ],
            [
                gettext('Mobile'),
                'INPUT',
                [
                    'name'  => 'telephone_2',
                    'size'  => '15',
                    'class' => "text field medium",
                ],
                'phn_number',
                'tOOL TIP',
                '',
            ],
            [
                gettext('Email'),
                'INPUT',
                [
                    'name'  => 'email',
                    'size'  => '50',
                    'class' => "text field medium",
                ],
                'required|valid_email|is_unique['.$val.']',
                'tOOL TIP',
                '',
            ],
            [
                gettext('Address 1'),
                'INPUT',
                [
                    'name'  => 'address_1',
                    'size'  => '15',
                    'class' => "text field medium",
                ],
                '',
                'tOOL TIP',
                '',
            ],
            [
                gettext('Address 2'),
                'INPUT',
                [
                    'name'  => 'address_2',
                    'size'  => '15',
                    'class' => "text field medium",
                ],
                '',
                'tOOL TIP',
                '',
            ],
            [
                gettext('City'),
                'INPUT',
                [
                    'name'  => 'city',
                    'size'  => '20',
                    'class' => "text field medium",
                ],
                '',
                'tOOL TIP',
                '',
            ],
            [
                gettext('Province/State'),
                'INPUT',
                [
                    'name'  => 'province',
                    'size'  => '15',
                    'class' => "text field medium",
                ],
                '',
                'tOOL TIP',
                '',
            ],
            [
                gettext('Zip/Postal Code'),
                'INPUT',
                [
                    'name'  => 'postal_code',
                    'size'  => '15',
                    'class' => "text field medium",
                ],
                'trim|xss_clean',
                'tOOL TIP',
                '',
            ],
            [
                gettext('Country'),
                [
                    'name'  => 'country_id',
                    'class' => 'country_id',
                ],
                'SELECT',
                '',
                [
                    "name"  => "country_id",
                    "rules" => "required",
                ],
                'tOOL TIP',
                'Please Enter account number',
                'id',
                'country',
                'countrycode',
                'build_dropdown',
                '',
                '',
            ],
            [
                gettext('Timezone'),
                [
                    'name'  => 'timezone_id',
                    'class' => 'timezone_id',
                ],
                'SELECT',
                '',
                [
                    "name"  => "timezone_id",
                    "rules" => "required",
                ],
                'tOOL TIP',
                'Please Enter account number',
                'id',
                'gmtzone',
                'timezone',
                'build_dropdown',
                '',
                '',
            ],
            [
                gettext('Currency'),
                [
                    'name'  => 'currency_id',
                    'class' => 'currency_id',
                ],
                'SELECT',
                '',
                [
                    "name"  => "currency_id",
                    "rules" => "required",
                ],
                'tOOL TIP',
                'Please Enter account number',
                'id',
                'currencyname,currency',
                'currency',
                'build_concat_dropdown',
                '',
                [],
            ],
        ];

        $form [gettext('Account Settings')] = [
            [
                gettext('Status'),
                'status',
                'SELECT',
                '',
                '',
                'tOOL TIP',
                'Please Enter account number',
                '',
                '',
                '',
                'set_status',
            ],
            [
                gettext('Allow Recording'),
                'is_recording',
                'SELECT',
                '',
                '',
                'tOOL TIP',
                'Please Enter account number',
                '',
                '',
                '',
                'custom_status_recording',
            ],
            [
                gettext('Allow IP Management'),
                'allow_ip_management',
                'SELECT',
                '',
                '',
                'tOOL TIP',
                'Please Enter account number',
                '',
                '',
                '',
                'custom_status',
            ],
            $sip_device,
            [
                gettext('Number Translation'),
                'INPUT',
                [
                    'name'  => 'dialed_modify',
                    'size'  => '20',
                    'class' => "text field medium",
                ],
                '',
                'tOOL TIP',
                '',
            ],

            // Added caller id translation code.
            [
                gettext('OUT Callerid Translation'),
                'INPUT',
                [
                    'name'  => 'std_cid_translation',
                    'size'  => '20',
                    'class' => "text field medium",
                ],
                '',
                'tOOL TIP',
                '',
            ],
            [
                gettext('IN Callerid Translation'),
                'INPUT',
                [
                    'name'  => 'did_cid_translation',
                    'size'  => '20',
                    'class' => "text field medium",
                ],
                '',
                'tOOL TIP',
                '',
            ],

            [
                gettext('Concurrent Calls'),
                'INPUT',
                [
                    'name'  => 'maxchannels',
                    'size'  => '20',
                    'class' => "text field medium",
                ],
                'numeric',
                'tOOL TIP',
                '',
            ],
            [
                gettext('CPS'),
                'INPUT',
                [
                    'name'  => 'cps',
                    'size'  => '20',
                    'class' => "text field medium",
                ],
                'numeric',
                'tOOL TIP',
                '',
            ],
            [
                gettext('First Used'),
                'INPUT',
                [
                    'name'     => 'first_used',
                    'size'     => '20',
                    'readonly' => true,
                    'class'    => "text field medium",
                    'value'    => '0000-00-00 00:00:00',
                ],
                '',
                'tOOL TIP',
                '',
            ],
            [
                gettext('Account Valid Days'),
                'INPUT',
                [
                    'name'  => 'validfordays',
                    'size'  => '20',
                    'class' => "text field medium",
                ],
                'trim|numeric|xss_clean',
                'tOOL TIP',
                '',
            ],
            [
                gettext('Expiry Date'),
                'INPUT',
                [
                    'name'  => 'expiry',
                    'size'  => '20',
                    'class' => "text field medium",
                    'value' => $expiry_date,
                    'id'    => 'expiry',
                ],
                '',
                'tOOL TIP',
                '',
            ],
        ];

        $form [gettext('Billing Settings')] = [
            [
                gettext('Rate Group'),
                'pricelist_id',
                'SELECT',
                '',
                [
                    "name"  => "pricelist_id",
                    "rules" => "dropdown",
                ],
                'tOOL TIP',
                'Please Enter account number',
                'id',
                'name',
                'pricelists',
                'build_dropdown',
                'where_arr',
                [
                    "status"      => "0",
                    "reseller_id" => $loginid,
                ],
            ],
            [
                gettext('Account Type'),
                [
                    'name'     => 'posttoexternal',
                    'disabled' => $readable,
                    'class'    => 'posttoexternal',
                    'id'       => 'posttoexternal',
                ],
                'SELECT',
                '',
                '',
                'tOOL TIP',
                'Please Enter account number',
                '',
                '',
                '',
                'set_account_type',
            ],
            [
                gettext('Billing Schedule'),
                [
                    'name'  => 'sweep_id',
                    'class' => 'sweep_id',
                    'id'    => 'sweep_id',
                ],
                'SELECT',
                '',
                '',
                'tOOL TIP',
                '',
                'id',
                'sweep',
                'sweeplist',
                'build_dropdown',
                '',
                '',
            ],
            [
                gettext('Billing Day'),
                [
                    "name"  => 'invoice_day',
                    "class" => "invoice_day",
                ],
                'SELECT',
                '',
                '',
                'tOOL TIP',
                '',
                '',
                '',
                '',
                'set_invoice_option',
            ],
            $balance,
            [
                gettext('Credit Limit'),
                'INPUT',
                [
                    'name'  => 'credit_limit',
                    'size'  => '20',
                    'class' => "text field medium",
                ],
                'currency_decimal',
                'tOOL TIP',
                '',
            ],
            [
                gettext('Allow Local Calls'),
                'local_call',
                'SELECT',
                '',
                '',
                'tOOL TIP',
                'Please Enter account number',
                '',
                '',
                '',
                'custom_status',
            ],
            [
                gettext('LC Charge / Min'),
                'INPUT',
                [
                    'name'  => 'charge_per_min',
                    'size'  => '20',
                    'class' => "text field medium",
                ],
                '',
                'tOOL TIP',
                '',
            ],
            [
                gettext('Tax'),
                'tax_id',
                'SELECT',
                '',
                '',
                'tOOL TIP',
                'Please Enter account number',
                'id',
                'taxes_description',
                'taxes',
                'build_dropdown',
                'where_arr',
                [
                    'status'      => 0,
                    "reseller_id" => $loginid,
                ],
                'multi',
            ],
            [
                gettext('Tax Number'),
                'INPUT',
                [
                    'name'  => 'tax_number',
                    'size'  => '100',
                    'class' => "text field medium",
                ],
                '',
                'tOOL TIP',
                '',
            ],
        ];
        if ($id == 0) {
            $form [gettext('Alert Threshold')] = [
                [
                    '',
                    'HIDDEN',
                    [
                        'name' => 'id',
                    ],
                    '',
                    '',
                    '',
                    '',
                ],
                [
                    gettext('Email Alerts ?'),
                    'notify_flag',
                    'SELECT',
                    '',
                    '',
                    'tOOL TIP',
                    '',
                    '',
                    '',
                    '',
                    'custom_status_recording',
                ],
                [
                    gettext('Balance Below'),
                    'INPUT',
                    [
                        'name'  => 'notify_credit_limit',
                        'size'  => '20',
                        'class' => "text field medium",
                    ],
                    'currency_decimal',
                    'tOOL TIP',
                    '',
                ],
                [
                    gettext('Email'),
                    'INPUT',
                    [
                        'name'  => 'notify_email',
                        'size'  => '50',
                        'class' => "text field medium",
                    ],
                    'valid_email',
                    'tOOL TIP',
                    '',
                ],
            ];
        }
        $form ['button_save'] = [
            'name'    => 'action',
            'content' => gettext('Save'),
            'value'   => 'save',
            'type'    => 'submit',
            'class'   => 'btn btn-line-parrot',
        ];
        $form ['button_cancel'] = [
            'name'    => 'action',
            'content' => gettext('Cancel'),
            'value'   => 'cancel',
            'type'    => 'button',
            'class'   => 'btn btn-line-sky margin-x-10',
            'onclick' => 'return redirect_page(\'/accounts/customer_list/\')',
        ];
        if ($id > 0) {
            unset ($form [gettext('Account Settings')] [3]);
        }

        return $form;
    }

    function customer_alert_threshold($entity_type)
    {
        $form ['forms'] = [
            base_url().'accounts/'.$entity_type.'_alert_threshold_save/'.$entity_type."/",
            [
                "id"   => "customer_alert_threshold",
                "name" => "customer_alert_threshold",
            ],
        ];
        $form [gettext('Alert Threshold')] = [
            [
                '',
                'HIDDEN',
                [
                    'name' => 'id',
                ],
                '',
                '',
                '',
                '',
            ],
            [
                gettext('Email Alerts ?'),
                'notify_flag',
                'SELECT',
                '',
                '',
                'tOOL TIP',
                '',
                '',
                '',
                '',
                'custom_status',
            ],
            [
                gettext('Balance Below'),
                'INPUT',
                [
                    'name'  => 'notify_credit_limit',
                    'size'  => '20',
                    'class' => "text field medium",
                ],
                'currency_decimal',
                'tOOL TIP',
                '',
            ],
            [
                gettext('Email'),
                'INPUT',
                [
                    'name'  => 'notify_email',
                    'size'  => '50',
                    'class' => "text field medium",
                ],
                'valid_email',
                'tOOL TIP',
                '',
            ],
        ];
        $form ['button_save'] = [
            'name'    => 'action',
            'content' => gettext('Save'),
            'value'   => 'save',
            'type'    => 'submit',
            'class'   => 'btn btn-line-parrot',
        ];

        return $form;
    }

    function customer_bulk_generate_form()
    {
        $logintype = $this->CI->session->userdata('logintype');
        $sip_device = null;
        $opensips_device = null;
        if ($logintype == 5) {
            $account_data = $this->CI->session->userdata("accountinfo");
            $loginid = $account_data ['id'];
        } else {
            $loginid = "0";
            if (common_model::$global_config ['system_config'] ['opensips'] == 0) {
                $opensips_device = [
                    gettext('Create Opensips Device'),
                    'opensips_device_flag',
                    'SELECT',
                    '',
                    '',
                    'tOOL TIP',
                    'Please Enter account number',
                    '',
                    '',
                    '',
                    'custom_status',
                ];
            } else {
                $sip_device = [
                    gettext('Create SIP Device'),
                    'sip_device_flag',
                    'SELECT',
                    '',
                    '',
                    'tOOL TIP',
                    'Please Enter account number',
                    '',
                    '',
                    '',
                    'custom_status',
                ];
            }
        }
        $form ['forms'] = [
            base_url().'accounts/customer_bulk_save/',
            [
                "id"   => "customer_bulk_form",
                "name" => "customer_bulk_form",
            ],
        ];
        $form [gettext('General Details')] = [
            [
                gettext('Account Count'),
                'INPUT',
                [
                    'name'  => 'count',
                    'size'  => '20',
                    'class' => "text field medium",
                ],
                'trim|required|numeric|greater_than[0]|xss_clean',
                'tOOL TIP',
                '',
            ],
            [
                gettext('Start Prefix'),
                'INPUT',
                [
                    'name'  => 'prefix',
                    'size'  => '20',
                    'class' => "text field medium",
                ],
                'trim|required|numeric|greater_than[0]|xss_clean',
                'tOOL TIP',
                '',
            ],
            [
                gettext('Acc. Number Length'),
                'INPUT',
                [
                    'name'  => 'account_length',
                    'size'  => '20',
                    'class' => "text field medium",
                ],
                'trim|greater_than[0]|required|numeric|xss_clean',
                'tOOL TIP',
                '',
            ],
            [
                gettext('Company'),
                'INPUT',
                [
                    'name'  => 'company_name',
                    'size'  => '15',
                    'class' => 'text field medium',
                ],
                'trim|required|alpha|xss_clean',
                'tOOL TIP',
                '',
            ],
            [
                gettext('Country'),
                [
                    'name'  => 'country_id',
                    'class' => 'country_id',
                ],
                'SELECT',
                '',
                [
                    "name"  => "country_id",
                    "rules" => "required",
                ],
                'tOOL TIP',
                'Please Enter account number',
                'id',
                'country',
                'countrycode',
                'build_dropdown',
                '',
                '',
            ],
            [
                gettext('Timezone'),
                [
                    'name'  => 'timezone_id',
                    'class' => 'timezone_id',
                ],
                'SELECT',
                '',
                [
                    "name"  => "timezone_id",
                    "rules" => "required",
                ],
                'tOOL TIP',
                'Please Enter account number',
                'id',
                'gmtzone',
                'timezone',
                'build_dropdown',
                '',
                '',
            ],
            [
                gettext('Generate Pin'),
                'pin',
                'SELECT',
                '',
                '',
                'tOOL TIP',
                'Please Enter account number',
                '',
                '',
                '',
                'set_pin_allow_customer',
            ],
            [
                gettext('Allow Recording'),
                'is_recording',
                'SELECT',
                '',
                '',
                'tOOL TIP',
                'Please Enter account number',
                '',
                '',
                '',
                'custom_status_recording',
            ],
            [
                gettext('Allow IP Management'),
                'allow_ip_management',
                'SELECT',
                '',
                '',
                'tOOL TIP',
                'Please Enter account number',
                '',
                '',
                '',
                'custom_status',
            ],
            $sip_device,
            $opensips_device,
        ];
        $form [gettext('Default Settings')] = [
            [
                gettext('Rate Group'),
                [
                    'name'  => 'pricelist_id',
                    'class' => 'pricelist_id',
                ],
                'SELECT',
                '',
                "required",
                'tOOL TIP',
                'Please Enter account number',
                'id',
                'name',
                'pricelists',
                'build_dropdown',
                'where_arr',
                [
                    "status"      => "0",
                    "reseller_id" => $loginid,
                ],
            ],
            [
                gettext('Account Type'),
                'posttoexternal',
                'SELECT',
                '',
                '',
                'tOOL TIP',
                'Please Enter account number',
                '',
                '',
                '',
                'set_account_type',
            ],
            [
                gettext('Billing Schedule'),
                [
                    'name'  => 'sweep_id',
                    'id'    => 'sweep_id',
                    'class' => 'sweep_id',
                ],
                'SELECT',
                '',
                '',
                'tOOL TIP',
                '',
                'id',
                'sweep',
                'sweeplist',
                'build_dropdown',
                '',
                '',
            ],
            [
                gettext('Billing Day'),
                [
                    "name"  => 'invoice_day',
                    "class" => "invoice_day",
                ],
                'SELECT',
                '',
                '',
                'tOOL TIP',
                '',
                '',
                '',
                '',
                'set_invoice_option',
            ],
            [
                gettext('Currency'),
                [
                    'name'  => 'currency_id',
                    'class' => 'currency_id',
                ],
                'SELECT',
                '',
                [
                    "name"  => "currency_id",
                    "rules" => "required",
                ],
                'tOOL TIP',
                'Please Enter account number',
                'id',
                'currencyname,currency',
                'currency',
                'build_concat_dropdown',
                '',
                [],
            ],
            [
                gettext('Balance'),
                'INPUT',
                [
                    'name'  => 'balance',
                    'size'  => '20',
                    'class' => "text field medium",
                ],
                'trim|numeric|greater_than[0]|currency_decimal|xss_clean',
                'tOOL TIP',
                '',
            ],
            [
                gettext('Credit Limit'),
                'INPUT',
                [
                    'name'  => 'credit_limit',
                    'size'  => '20',
                    'class' => "text field medium",
                ],
                'trim|currency_decimal|xss_clean',
                'tOOL TIP',
                '',
            ],
            [
                gettext('Valid Days'),
                'INPUT',
                [
                    'name'  => 'validfordays',
                    'size'  => '20',
                    'class' => "text field medium",
                ],
                'trim|numeric|greater_than[0]|xss_clean',
                'tOOL TIP',
                '',
            ],
            [
                gettext('Allow Local Calls'),
                'local_call',
                'SELECT',
                '',
                '',
                'tOOL TIP',
                'Please Enter account number',
                '',
                '',
                '',
                'custom_status',
            ],
            [
                gettext('LC Charge / Min'),
                'INPUT',
                [
                    'name'  => 'charge_per_min',
                    'size'  => '20',
                    'class' => "text field medium",
                ],
                'numeric|greater_than[0]',
                'tOOL TIP',
                '',
            ],
        ];
        $form ['button_save'] = [
            'name'    => 'action',
            'content' => gettext('Save'),
            'value'   => 'save',
            'id'      => 'submit',
            'type'    => 'button',
            'class'   => 'btn btn-line-parrot',
        ];
        $form ['button_cancel'] = [
            'name'    => 'action',
            'content' => gettext('Close'),
            'value'   => 'cancel',
            'type'    => 'button',
            'class'   => 'btn btn-line-sky margin-x-10',
            'onclick' => 'return redirect_page(\'NULL\')',
        ];

        return $form;
    }

    function get_customer_callerid_fields()
    {
        $form ['forms'] = [
            base_url().'accounts/customer_add_callerid/',
            [
                "id" => "callerid_form",
            ],
        ];
        $form [gettext('Information')] = [
            [
                '',
                'HIDDEN',
                [
                    'name' => 'flag',
                ],
                '',
                '',
                '',
                '',
            ],
            [
                gettext('Account'),
                'INPUT',
                [
                    'name'     => 'accountid',
                    'size'     => '20',
                    'readonly' => true,
                    'class'    => "text field medium",
                ],
                '',
                'tOOL TIP',
                'Please Enter account number',
            ],
            [
                gettext('Enable'),
                'status',
                'SELECT',
                '',
                '',
                'tOOL TIP',
                'Please Enter account number',
                '',
                '',
                '',
                'custom_status',
            ],
            [
                gettext('Caller Id Name'),
                'INPUT',
                [
                    'name'  => 'callerid_name',
                    'size'  => '20',
                    'class' => "text field medium",
                ],
                'trim',
                'tOOL TIP',
                '',
            ],
            [
                gettext('Caller Id Number'),
                'INPUT',
                [
                    'name'  => 'callerid_number',
                    'size'  => '20',
                    'class' => "text field medium",
                ],
                'trim',
                'tOOL TIP',
                '',
            ],
        ];
        $form ['button_save'] = [
            'name'    => 'action',
            'content' => gettext('Save'),
            'value'   => 'save',
            "id"      => "submit",
            'type'    => 'button',
            'class'   => 'btn btn-line-parrot',
        ];
        $form ['button_cancel'] = [
            'name'    => 'action',
            'content' => gettext('Close'),
            'value'   => 'cancel',
            'type'    => 'button',
            'class'   => 'btn btn-line-sky margin-x-10',
            'onclick' => 'return redirect_page(\'NULL\')',
        ];

        return $form;
    }

    function get_customer_payment_fields($currency, $number, $currency_id, $id)
    {
        $form ['forms'] = [
            base_url().'accounts/customer_payment_save/',
            [
                'id'     => 'acccount_charges_form',
                'method' => 'POST',
                'name'   => 'acccount_charges_form',
            ],
        ];
        $form ['​Refill information'] = [
            [
                '',
                'HIDDEN',
                [
                    'name'  => 'id',
                    'value' => $id,
                ],
                '',
                '',
                '',
                '',
            ],
            [
                '',
                'HIDDEN',
                [
                    'name'  => 'account_currency',
                    'value' => $currency_id,
                ],
                '',
                '',
                '',
            ],
            [
                gettext('Account'),
                'INPUT',
                [
                    'name'     => 'accountid',
                    'size'     => '20',
                    'value'    => $number,
                    'readonly' => true,
                    'class'    => "text field medium",
                ],
                '',
                'tOOL TIP',
                'Please Enter account number',
            ],
            [
                gettext('Amount')."(".$currency.")",
                'INPUT',
                [
                    'name'  => 'credit',
                    'size'  => '20',
                    'class' => "text col-md-5 field medium",
                ],
                'trim|required',
                'tOOL TIP',
                '',
            ],
            [
                gettext('Type'),
                'payment_type',
                'SELECT',
                '',
                '',
                'tOOL TIP',
                'Please Enter account number',
                '',
                '',
                '',
                'set_payment_type',
            ],
            [
                gettext('Note'),
                'TEXTAREA',
                [
                    'name'  => 'notes',
                    'size'  => '20',
                    'cols'  => '50',
                    'rows'  => '3',
                    'class' => "text col-md-5 field medium",
                    'style' => "width: 450px; height: 100px;",
                ],
                '',
                'tOOL TIP',
                '',
            ],
        ];
        /* *************************************** */
        $form ['button_save'] = [
            'name'    => 'action',
            'content' => gettext('Process'),
            'value'   => 'save',
            'id'      => "submit",
            'type'    => 'button',
            'class'   => 'btn btn-line-parrot',
        ];
        $form ['button_cancel'] = [
            'name'    => 'action',
            'content' => gettext('Close'),
            'value'   => 'cancel',
            'type'    => 'button',
            'class'   => 'btn btn-line-sky margin-x-10',
            'onclick' => 'return redirect_page(\'NULL\')',
        ];

        return $form;
    }

    function get_form_reseller_fields($id = false)
    {
        $readable = false;
        $invoice_config = null;
        $concurrent_calls = null;
        $logintype = $this->CI->session->userdata('logintype');
        $uname = $this->CI->common->find_uniq_rendno(
            common_model::$global_config ['system_config'] ['cardlength'],
            'number',
            'accounts'
        );
        $password = $this->CI->common->generate_password();
        $params = [
            'name'     => 'number',
            'value'    => $uname,
            'size'     => '20',
            'class'    => "text field medium",
            'id'       => 'number',
            'readonly' => true,
        ];
        if ($logintype == 1 || $logintype == 5) {
            $account_data = $this->CI->session->userdata("accountinfo");
            $loginid = $account_data ['id'];
        } else {
            $loginid = "0";
        }
        if ($id > 0) {
            $val = 'accounts.email.'.$id;
            $account_val = 'accounts.number.'.$id;
            $readable = 'disabled';
            $password = [
                'Password',
                'PASSWORD',
                [
                    'name'        => 'password',
                    'id'          => 'password_show',
                    'onmouseover' => 'seetext(password_show)',
                    'onmouseout'  => 'hidepassword(password_show)',
                    'size'        => '20',
                    'class'       => "text field medium",
                ],
                'required|notMatch[number]|',
                'tOOL TIP',
                '',
            ];
            $concurrent_calls = [
                'Concurrent Calls',
                'INPUT',
                [
                    'name'  => 'maxchannels',
                    'size'  => '20',
                    'class' => "text field medium",
                ],
                'numeric',
                'tOOL TIP',
                '',
            ];
            $account = [
                'Account',
                'INPUT',
                $params,
                'required|integer|greater_than[0]|is_unique['.$account_val.']',
                'tOOL TIP',
                '',
            ];
        } else {
            $val = 'accounts.email';
            $account_val = 'accounts.number';
            $invoice_config = [
                'Use same credential for Invoice Config',
                'invoice_config_flag',
                'SELECT',
                '',
                '',
                'tOOL TIP',
                'Please Enter account number',
                '',
                '',
                '',
                'set_prorate',
            ];
            $password = [
                'Password',
                'INPUT',
                [
                    'name'  => 'password',
                    'value' => $password,
                    'size'  => '20',
                    'class' => "text field medium",
                    'id'    => 'password',
                ],
                'required|',
                'tOOL TIP',
                '',
                '<i style="cursor:pointer; font-size: 17px; padding-left:10px; padding-top:6px;color: #1bcb61;" title="Reset Password" class="change_pass fa fa-refresh" ></i>',
            ];
            $account = [
                'Account',
                'INPUT',
                $params,
                'required|integer|greater_than[0]|is_unique['.$account_val.']',
                'tOOL TIP',
                '',
                '<i style="cursor:pointer; font-size: 17px; padding-left:10px; padding-top:6px;color: #1bcb61;" title="Generate Account" class="change_number fa fa-refresh" ></i>',
            ];
        }
        if ($id == "") {
            $reg_url = [
                '',
                'HIDDEN',
                [
                    'name' => 'id',
                ],
                '',
                '',
                '',
                '',
            ];
        } else {
            $reg_url = [
                'Registration URL',
                'INPUT',
                [
                    'name'     => 'registration_url',
                    'size'     => '20',
                    'readonly' => true,
                    'class'    => "text field medium",
                ],
                'tOOL TIP',
                '',
            ];
        }
        /* * ****************************************************************** */
        $form ['forms'] = [
            base_url().'accounts/reseller_save/',
            [
                "id"   => "reseller_form",
                "name" => "reseller_form",
            ],
        ];
        $form ['Client Panel Access'] = [
            [
                '',
                'HIDDEN',
                [
                    'name' => 'id',
                ],
                '',
                '',
                '',
                '',
            ],
            [
                '',
                'HIDDEN',
                [
                    'name'  => 'type',
                    'value' => '1',
                ],
                '',
                '',
                '',
            ],
            $account,
            $password,
            $reg_url,
        ];
        if ($id > 0) {

            $form [gettext('Billing Information')] = [
                [
                    gettext('Rate Group'),
                    'pricelist_id',
                    'SELECT',
                    '',
                    [
                        "name"  => "pricelist_id",
                        'rules' => 'required',
                    ],
                    'tOOL TIP',
                    'Please Enter account number',
                    'id',
                    'name',
                    'pricelists',
                    'build_dropdown',
                    'where_arr',
                    [
                        "status"      => "0",
                        "reseller_id" => "0",
                    ],
                ],
                $concurrent_calls,
                [
                    gettext('Billing Schedule'),
                    [
                        'name'  => 'sweep_id',
                        'class' => 'sweep_id',
                    ],
                    'SELECT',
                    '',
                    '',
                    'tOOL TIP',
                    '',
                    'id',
                    'sweep',
                    'sweeplist',
                    'build_dropdown',
                    '',
                    '',
                ],
                [
                    gettext('Billing Day'),
                    [
                        "name"  => 'invoice_day',
                        "class" => "invoice_day",
                    ],
                    'SELECT',
                    '',
                    '',
                    'tOOL TIP',
                    '',
                    '',
                    '',
                    '',
                    'set_invoice_option',
                ],
                [
                    gettext('Currency'),
                    [
                        'name'  => 'currency_id',
                        'class' => 'currency_id',
                    ],
                    'SELECT',
                    '',
                    [
                        "name"  => "currency_id",
                        "rules" => "required",
                    ],
                    'tOOL TIP',
                    'Please Enter account number',
                    'id',
                    'currencyname,currency',
                    'currency',
                    'build_concat_dropdown',
                    '',
                    [],
                ],
                [
                    gettext('Account Type'),
                    [
                        'name'     => 'posttoexternal',
                        'disabled' => $readable,
                        'class'    => 'posttoexternal',
                        'id'       => 'posttoexternal',
                    ],
                    'SELECT',
                    '',
                    '',
                    'tOOL TIP',
                    'Please Enter account number',
                    '',
                    '',
                    '',
                    'set_account_type',
                ],
                [
                    gettext('Credit Limit'),
                    'INPUT',
                    [
                        'name'  => 'credit_limit',
                        'size'  => '20',
                        'class' => "text field medium",
                    ],
                    '',
                    'tOOL TIP',
                    '',
                ],
                [
                    gettext('Tax'),
                    'tax_id',
                    'SELECT',
                    '',
                    '',
                    'tOOL TIP',
                    'Please Enter account number',
                    'id',
                    'taxes_description',
                    'taxes',
                    'build_dropdown',
                    'where_arr',
                    [
                        'status'      => 0,
                        'reseller_id' => $loginid,
                    ],
                    'multi',
                ],
                [
                    gettext('Tax Number'),
                    'INPUT',
                    [
                        'name'  => 'tax_number',
                        'size'  => '100',
                        'class' => "text field medium",
                    ],
                    '',
                    'tOOL TIP',
                    '',
                ],
            ];
        } else {
            $form [gettext('Billing Information')] = [
                [
                    gettext('Rate Group'),
                    'pricelist_id',
                    'SELECT',
                    '',
                    [
                        "name"  => "pricelist_id",
                        'rules' => 'required',
                    ],
                    'tOOL TIP',
                    'Please Enter account number',
                    'id',
                    'name',
                    'pricelists',
                    'build_dropdown',
                    'where_arr',
                    [
                        "status"      => "0",
                        "reseller_id" => "0",
                    ],
                ],

                [
                    gettext('Billing Schedule'),
                    [
                        'name'  => 'sweep_id',
                        'class' => 'sweep_id',
                    ],
                    'SELECT',
                    '',
                    '',
                    'tOOL TIP',
                    '',
                    'id',
                    'sweep',
                    'sweeplist',
                    'build_dropdown',
                    '',
                    '',
                ],
                [
                    gettext('Billing Day'),
                    [
                        "name"  => 'invoice_day',
                        "class" => "invoice_day",
                    ],
                    'SELECT',
                    '',
                    '',
                    'tOOL TIP',
                    '',
                    '',
                    '',
                    '',
                    'set_invoice_option',
                ],
                [
                    gettext('Currency'),
                    [
                        'name'  => 'currency_id',
                        'class' => 'currency_id',
                    ],
                    'SELECT',
                    '',
                    [
                        "name"  => "currency_id",
                        "rules" => "required",
                    ],
                    'tOOL TIP',
                    'Please Enter account number',
                    'id',
                    'currencyname,currency',
                    'currency',
                    'build_concat_dropdown',
                    '',
                    [],
                ],
                [
                    gettext('Account Type'),
                    [
                        'name'     => 'posttoexternal',
                        'disabled' => $readable,
                        'class'    => 'posttoexternal',
                        'id'       => 'posttoexternal',
                    ],
                    'SELECT',
                    '',
                    '',
                    'tOOL TIP',
                    'Please Enter account number',
                    '',
                    '',
                    '',
                    'set_account_type',
                ],
                [
                    gettext('Credit Limit'),
                    'INPUT',
                    [
                        'name'  => 'credit_limit',
                        'size'  => '20',
                        'class' => "text field medium",
                    ],
                    '',
                    'tOOL TIP',
                    '',
                ],
                [
                    gettext('Tax'),
                    'tax_id',
                    'SELECT',
                    '',
                    '',
                    'tOOL TIP',
                    'Please Enter account number',
                    'id',
                    'taxes_description',
                    'taxes',
                    'build_dropdown',
                    'where_arr',
                    [
                        'status'      => 0,
                        'reseller_id' => $loginid,
                    ],
                    'multi',
                ],
                [
                    gettext('Tax Number'),
                    'INPUT',
                    [
                        'name'  => 'tax_number',
                        'size'  => '100',
                        'class' => "text field medium",
                    ],
                    '',
                    'tOOL TIP',
                    '',
                ],
                $invoice_config,
            ];
        }
        $form [gettext('Reseller Profile')] = [
            [
                gettext('First Name'),
                'INPUT',
                [
                    'name'  => 'first_name',
                    'id'    => 'first_name',
                    'size'  => '50',
                    'class' => "text field medium",
                ],
                'trim|required|xss_clean',
                'tOOL TIP',
                '',
            ],
            [
                gettext('Last Name'),
                'INPUT',
                [
                    'name'  => 'last_name',
                    'size'  => '15',
                    'class' => "text field medium",
                ],
                'trim|xss_clean',
                'tOOL TIP',
                '',
            ],
            [
                gettext('Company'),
                'INPUT',
                [
                    'name'  => 'company_name',
                    'size'  => '50',
                    'class' => 'text field medium',
                ],
                'trim|xss_clean',
                'tOOL TIP',
                '',
            ],
            [
                gettext('Telephone 1'),
                'INPUT',
                [
                    'name'  => 'telephone_1',
                    'size'  => '15',
                    'class' => "text field medium",
                ],
                'phn_number',
                'tOOL TIP',
                '',
            ],
            [
                gettext('Telephone 2'),
                'INPUT',
                [
                    'name'  => 'telephone_2',
                    'size'  => '15',
                    'class' => "text field medium",
                ],
                'phn_number',
                'tOOL TIP',
                '',
            ],
            [
                gettext('Email'),
                'INPUT',
                [
                    'name'  => 'email',
                    'size'  => '50',
                    'class' => "text field medium",
                ],
                'required|valid_email|is_unique['.$val.']',
                'tOOL TIP',
                '',
            ],
            [
                gettext('Address 1'),
                'INPUT',
                [
                    'name'  => 'address_1',
                    'size'  => '15',
                    'class' => "text field medium",
                ],
                '',
                'tOOL TIP',
                '',
            ],
            [
                gettext('Address 2'),
                'INPUT',
                [
                    'name'  => 'address_2',
                    'size'  => '15',
                    'class' => "text field medium",
                ],
                '',
                'tOOL TIP',
                '',
            ],
            [
                gettext('City'),
                'INPUT',
                [
                    'name'  => 'city',
                    'size'  => '20',
                    'class' => "text field medium",
                ],
                '',
                'tOOL TIP',
                '',
            ],
            [
                gettext('Province/State'),
                'INPUT',
                [
                    'name'  => 'province',
                    'size'  => '15',
                    'class' => "text field medium",
                ],
                '',
                'tOOL TIP',
                '',
            ],
            [
                gettext('Zip/Postal Code'),
                'INPUT',
                [
                    'name'  => 'postal_code',
                    'size'  => '15',
                    'class' => "text field medium",
                ],
                'trim|xss_clean',
                'tOOL TIP',
                '',
            ],
            [
                gettext('Country'),
                [
                    'name'  => 'country_id',
                    'class' => 'country_id',
                ],
                'SELECT',
                '',
                [
                    "name"  => "country_id",
                    "rules" => "required",
                ],
                'tOOL TIP',
                'Please Enter account number',
                'id',
                'country',
                'countrycode',
                'build_dropdown',
                '',
                '',
            ],
            [
                gettext('Timezone'),
                [
                    'name'  => 'timezone_id',
                    'class' => 'timezone_id',
                ],
                'SELECT',
                '',
                [
                    "name"  => "timezone_id",
                    "rules" => "required",
                ],
                'tOOL TIP',
                'Please Enter account number',
                'id',
                'gmtzone',
                'timezone',
                'build_dropdown',
                '',
                '',
            ],
            [
                gettext('Account Status'),
                'status',
                'SELECT',
                '',
                '',
                'tOOL TIP',
                'Please Enter account number',
                '',
                '',
                '',
                'set_status',
            ],
        ];
        if ($id == 0) {
            $form [gettext('Alert Threshold')] = [
                [
                    'Email Alert?',
                    'notify_flag',
                    'SELECT',
                    '',
                    '',
                    'tOOL TIP',
                    '',
                    '',
                    '',
                    '',
                    'custom_status',
                ],
                [
                    gettext('Balance Below'),
                    'INPUT',
                    [
                        'name'  => 'notify_credit_limit',
                        'size'  => '20',
                        'class' => "text field medium",
                    ],
                    '',
                    'tOOL TIP',
                    '',
                ],
                [
                    gettext('Email'),
                    'INPUT',
                    [
                        'name'  => 'notify_email',
                        'size'  => '50',
                        'class' => "text field medium",
                    ],
                    'valid_email',
                    'tOOL TIP',
                    '',
                ],
            ];
        }
        $form ['button_cancel'] = [
            'name'    => 'action',
            'content' => gettext('Cancel'),
            'value'   => 'cancel',
            'type'    => 'button',
            'class'   => 'btn btn-line-sky margin-x-10',
            'onclick' => 'return redirect_page(\'/accounts/reseller_list/\')',
        ];
        $form ['button_save'] = [
            'name'    => 'action',
            'content' => gettext('Save'),
            'value'   => 'save',
            'type'    => 'submit',
            'class'   => 'btn btn-line-parrot',
        ];

        return $form;
    }

    function get_form_admin_fields($entity_type = '', $id = false)
    {
        $uname = $this->CI->common->find_uniq_rendno(
            common_model::$global_config ['system_config'] ['cardlength'],
            'number',
            'accounts'
        );
        $params = [
            'name'     => 'number',
            'value'    => $uname,
            'size'     => '20',
            'class'    => "text field medium",
            'id'       => 'number',
            'readonly' => true,
        ];
        if ($id > 0) {
            $val = 'accounts.email.'.$id;
            $account_val = 'accounts.number.'.$id;
            $password = [
                'Password',
                'PASSWORD',
                [
                    'name'        => 'password',
                    'id'          => 'password_show',
                    'onmouseover' => 'seetext(password_show)',
                    'onmouseout'  => 'hidepassword(password_show)',
                    'size'        => '20',
                    'class'       => "text field medium",
                ],
                'required|notMatch[number]|',
                'tOOL TIP',
                '',
            ];
            $account = [
                'Account',
                'INPUT',
                $params,
                'required|is_unique['.$account_val.']',
                'tOOL TIP',
                '',
            ];
            if ($entity_type == 'subadmin') {
                $account_status = [
                    gettext('Account Status'),
                    'status',
                    'SELECT',
                    '',
                    '',
                    'tOOL TIP',
                    'Please Enter account number',
                    '',
                    '',
                    '',
                    'set_status',
                ];
            } else {
                $account_status = null;
            }
        } else {
            $val = 'accounts.email';
            $account_val = 'accounts.number';
            $password = $this->CI->common->generate_password();
            $password = [
                'Password',
                'INPUT',
                [
                    'name'  => 'password',
                    'value' => $password,
                    'size'  => '20',
                    'class' => "text field medium",
                    'id'    => 'password',
                ],
                'required|',
                'tOOL TIP',
                '',
                '<i style="cursor:pointer; font-size: 17px; padding-left:10px; padding-top:6px;color: #1bcb61;" title="Reset Password" class="change_pass fa fa-refresh" ></i>',
            ];
            $account = [
                'Account',
                'INPUT',
                $params,
                'required|is_unique['.$account_val.']',
                'tOOL TIP',
                '',
                '<i style="cursor:pointer; font-size: 17px; padding-left:10px; padding-top:6px;color: #1bcb61;" title="Generate Account" class="change_number fa fa-refresh" ></i>',
            ];
            $account_status = [
                gettext('Account Status'),
                'status',
                'SELECT',
                '',
                '',
                'tOOL TIP',
                'Please Enter account number',
                '',
                '',
                '',
                'set_status',
            ];
            /* * ****************** */
        }

        $type = $entity_type == 'admin' ? 2 : 4;
        $form ['forms'] = [
            base_url().'accounts/'.$entity_type.'_save/',
            [
                "id"   => "admin_form",
                "name" => "admin_form",
            ],
        ];
        $form ['Client Panel Access'] = [
            [
                '',
                'HIDDEN',
                [
                    'name' => 'id',
                ],
                '',
                '',
                '',
                '',
            ],
            [
                '',
                'HIDDEN',
                [
                    'name'  => 'type',
                    'value' => $type,
                ],
                '',
                '',
                '',
            ],
            $account,
            $password,
            /*                 * ********************* */
        ];
        $form [gettext($entity_type.' Profile')] = [
            [
                gettext('First Name'),
                'INPUT',
                [
                    'name'      => 'first_name',
                    'id'        => 'first_name',
                    'size'      => '15',
                    'maxlength' => '40',
                    'class'     => "text field medium",
                ],
                'trim|required|xss_clean',
                'tOOL TIP',
                'Please Enter account number',
            ],
            [
                gettext('Last Name'),
                'INPUT',
                [
                    'name'  => 'last_name',
                    'size'  => '15',
                    'class' => "text field medium",
                ],
                'trim|xss_clean',
                'tOOL TIP',
                'Please Enter Password',
            ],
            [
                gettext('Company'),
                'INPUT',
                [
                    'name'  => 'company_name',
                    'size'  => '15',
                    'class' => 'text field medium',
                ],
                'trim|xss_clean',
                'tOOL TIP',
                '',
            ],
            [
                gettext('Telephone 1'),
                'INPUT',
                [
                    'name'  => 'telephone_1',
                    'size'  => '15',
                    'class' => "text field medium",
                ],
                'phn_number',
                'tOOL TIP',
                'Please Enter Password',
            ],
            [
                gettext('Telephone 2'),
                'INPUT',
                [
                    'name'  => 'telephone_2',
                    'size'  => '15',
                    'class' => "text field medium",
                ],
                'phn_number',
                'tOOL TIP',
                'Please Enter Password',
            ],
            [
                gettext('Email'),
                'INPUT',
                [
                    'name'  => 'email',
                    'size'  => '50',
                    'class' => "text field medium",
                ],
                'required|valid_email|is_unique['.$val.']',
                'tOOL TIP',
                '',
            ],
            [
                gettext('Address 1'),
                'INPUT',
                [
                    'name'  => 'address_1',
                    'size'  => '15',
                    'class' => "text field medium",
                ],
                '',
                'tOOL TIP',
                'Please Enter Password',
            ],
            [
                gettext('Address 2'),
                'INPUT',
                [
                    'name'  => 'address_2',
                    'size'  => '15',
                    'class' => "text field medium",
                ],
                '',
                'tOOL TIP',
                'Please Enter Password',
            ],
            [
                gettext('City'),
                'INPUT',
                [
                    'name'  => 'city',
                    'size'  => '20',
                    'class' => "text field medium",
                ],
                '',
                'tOOL TIP',
                'Please Enter Password',
            ],
            [
                gettext('Province/State'),
                'INPUT',
                [
                    'name'  => 'province',
                    'size'  => '15',
                    'class' => "text field medium",
                ],
                '',
                'tOOL TIP',
                'Please Enter Password',
            ],
            [
                gettext('Zip/Postal Code'),
                'INPUT',
                [
                    'name'  => 'postal_code',
                    'size'  => '15',
                    'class' => "text field medium",
                ],
                'trim|xss_clean',
                'tOOL TIP',
                'Please Enter Password',
            ],
            [
                gettext('Country'),
                [
                    'name'  => 'country_id',
                    'class' => 'country_id',
                ],
                'SELECT',
                '',
                '',
                'tOOL TIP',
                'Please Enter account number',
                'id',
                'country',
                'countrycode',
                'build_dropdown',
                '',
                '',
            ],
            [
                gettext('Timezone'),
                [
                    'name'  => 'timezone_id',
                    'class' => 'timezone_id',
                ],
                'SELECT',
                '',
                '',
                'tOOL TIP',
                'Please Enter account number',
                'id',
                'gmtzone',
                'timezone',
                'build_dropdown',
                '',
                '',
            ],
            $account_status,
            [
                gettext('Currency'),
                [
                    'name'  => 'currency_id',
                    'class' => 'currency_id',
                ],
                'SELECT',
                '',
                '',
                'tOOL TIP',
                'Please Enter account number',
                'id',
                'currencyname,currency',
                'currency',
                'build_concat_dropdown',
                '',
                [],
            ],
        ];

        $form ['button_cancel'] = [
            'name'    => 'action',
            'content' => gettext('Cancel'),
            'value'   => 'cancel',
            'type'    => 'button',
            'class'   => 'btn btn-line-sky margin-x-10',
            'onclick' => 'return redirect_page(\'/accounts/admin_list/\')',
        ];
        $form ['button_save'] = [
            'name'    => 'action',
            'content' => gettext('Save'),
            'value'   => 'save',
            'type'    => 'submit',
            'class'   => 'btn btn-line-parrot',
        ];

        return $form;
    }

    /**
     * ASTPP 3.0
     * Reseller Batch Update
     */
    function reseller_batch_update_form()
    {
        $status = [
            'Status',
            [
                'name' => 'status[status]',
                'id'   => 'status',
            ],
            'SELECT',
            '',
            '',
            'tOOL TIP',
            'Please Enter account number',
            'id',
            'name',
            '',
            'set_status',
            '',
            '',
            [
                'name'  => 'status[operator]',
                'class' => 'update_drp',
            ],
            'update_drp_type',
        ];
        $form ['forms'] = [
            "accounts/reseller_batch_update/",
            [
                'id' => "reseller_batch_update",
            ],
        ];
        $form [gettext('Batch Update')] = [
            [
                gettext('Rate Group'),
                [
                    'name' => 'pricelist_id[pricelist_id]',
                    'id'   => 'pricelist_id',
                ],
                'SELECT',
                '',
                '',
                'tOOL TIP',
                'Please Enter account number',
                'id',
                'name',
                'pricelists',
                'build_dropdown',
                'where_arr',
                [
                    "status"      => "0",
                    "reseller_id" => "0",
                ],
                [
                    'name'  => 'pricelist_id[operator]',
                    'class' => 'update_drp',
                ],
                'update_drp_type',
            ],
            [
                gettext('Balance'),
                'INPUT',
                [
                    'name'  => 'balance[balance]',
                    'id'    => 'balance',
                    'value' => '',
                    'size'  => '20',
                    'class' => "text field ",
                ],
                '',
                'Tool tips info',
                '1',
                [
                    'name'  => 'balance[operator]',
                    'class' => 'update_drp',
                ],
                '',
                '',
                '',
                'update_int_type',
                '',
            ],

            $status,
        ];
        $form ['button_search'] = [
            'name'    => 'action',
            'id'      => "batch_update_btn",
            'content' => gettext('Update'),
            'value'   => 'save',
            'type'    => 'button',
            'class'   => 'btn btn-line-parrot pull-right',
        ];
        $form ['button_reset'] = [
            'name'    => 'action',
            'id'      => "id_batch_reset",
            'content' => gettext('Clear'),
            'value'   => 'cancel',
            'type'    => 'reset',
            'class'   => 'btn btn-line-sky pull-right margin-x-10',
        ];

        return $form;
    }

    /* * ***************************************************************************************************************************** */

    /**
     * ASTPP 3.0
     * Customer Batch Update
     */
    function customer_batch_update_form()
    {
        $status = [
            'Status',
            [
                'name' => 'status[status]',
                'id'   => 'status',
            ],
            'SELECT',
            '',
            '',
            'tOOL TIP',
            'Please Enter account number',
            'id',
            'name',
            '',
            'set_status',
            '',
            '',
            [
                'name'  => 'status[operator]',
                'class' => 'update_drp',
            ],
            'update_drp_type',
        ];
        $form ['forms'] = [
            "accounts/customer_batch_update/",
            [
                'id' => "reseller_batch_update",
            ],
        ];
        $form [gettext('Batch Update')] = [
            [
                gettext('Rate Group'),
                [
                    'name' => 'pricelist_id[pricelist_id]',
                    'id'   => 'pricelist_id',
                ],
                'SELECT',
                '',
                '',
                'tOOL TIP',
                'Please Enter account number',
                'id',
                'name',
                'pricelists',
                'build_dropdown',
                'where_arr',
                [
                    "status"      => "0",
                    "reseller_id" => "0",
                ],
                [
                    'name'  => 'pricelist_id[operator]',
                    'class' => 'update_drp',
                ],
                'update_drp_type',
            ],
            [
                gettext('Balance'),
                'INPUT',
                [
                    'name'  => 'balance[balance]',
                    'id'    => 'balance',
                    'value' => '',
                    'size'  => '20',
                    'class' => "text field ",
                ],
                '',
                'Tool tips info',
                '1',
                [
                    'name'  => 'balance[operator]',
                    'class' => 'update_drp',
                ],
                '',
                '',
                '',
                'update_int_type',
                '',
            ],
            $status,
        ];
        $form ['button_search'] = [
            'name'    => 'action',
            'id'      => "batch_update_btn",
            'content' => gettext('Update'),
            'value'   => 'save',
            'type'    => 'button',
            'class'   => 'btn btn-line-parrot pull-right',
        ];
        $form ['button_reset'] = [
            'name'    => 'action',
            'id'      => "id_batch_reset",
            'content' => gettext('Clear'),
            'value'   => 'cancel',
            'type'    => 'reset',
            'class'   => 'btn btn-line-sky pull-right margin-x-10',
        ];

        return $form;
    }

    /* * ***************************************************************************************************************************** */
    function get_search_customer_form()
    {
        $logintype = $this->CI->session->userdata('userlevel_logintype');
        if ($logintype != 1) {
            $form ['forms'] = [
                "",
                [
                    'id' => "account_search",
                ],
            ];
            $form [gettext('Search')] = [
                [
                    gettext('Account'),
                    'INPUT',
                    [
                        'name'  => 'number[number]',
                        '',
                        'size'  => '20',
                        'class' => "text field ",
                    ],
                    '',
                    'tOOL TIP',
                    '1',
                    'number[number-string]',
                    '',
                    '',
                    '',
                    'search_string_type',
                    '',
                ],
                [
                    gettext('First Name'),
                    'INPUT',
                    [
                        'name'  => 'first_name[first_name]',
                        '',
                        'id'    => 'first_name',
                        'size'  => '15',
                        'class' => "text field ",
                    ],
                    '',
                    'tOOL TIP',
                    '1',
                    'first_name[first_name-string]',
                    '',
                    '',
                    '',
                    'search_string_type',
                    '',
                ],
                [
                    gettext('Last Name'),
                    'INPUT',
                    [
                        'name'  => 'last_name[last_name]',
                        'value' => '',
                        'size'  => '20',
                        'class' => "text field ",
                    ],
                    '',
                    'Tool tips info',
                    '1',
                    'last_name[last_name-string]',
                    '',
                    '',
                    '',
                    'search_string_type',
                    '',
                ],
                [
                    gettext('Company'),
                    'INPUT',
                    [
                        'name'  => 'company_name[company_name]',
                        'value' => '',
                        'size'  => '20',
                        'class' => "text field ",
                    ],
                    '',
                    'Tool tips info',
                    '1',
                    'company_name[company_name-string]',
                    '',
                    '',
                    '',
                    'search_string_type',
                    '',
                ],
                [
                    'CC',
                    'INPUT',
                    [
                        'name'  => 'maxchannels[maxchannels]',
                        'value' => '',
                        'size'  => '20',
                        'class' => "text field ",
                    ],
                    '',
                    'Tool tips info',
                    '1',
                    'maxchannels[maxchannels-integer]',
                    '',
                    '',
                    '',
                    'search_int_type',
                ],

                [
                    gettext('Balance'),
                    'INPUT',
                    [
                        'name'  => 'balance[balance]',
                        'value' => '',
                        'size'  => '20',
                        'class' => "text field ",
                    ],
                    '',
                    'Tool tips info',
                    '1',
                    'balance[balance-integer]',
                    '',
                    '',
                    '',
                    'search_int_type',
                    '',
                ],
                [
                    gettext('Credit Limit'),
                    'INPUT',
                    [
                        'name'  => 'credit_limit[credit_limit]',
                        'value' => '',
                        'size'  => '20',
                        'class' => "text field ",
                    ],
                    '',
                    'Tool tips info',
                    '1',
                    'credit_limit[credit_limit-integer]',
                    '',
                    '',
                    '',
                    'search_int_type',
                    '',
                ],
                [
                    gettext('Email'),
                    'INPUT',
                    [
                        'name'  => 'email[email]',
                        'value' => '',
                        'size'  => '20',
                        'class' => "text field ",
                    ],
                    '',
                    'Tool tips info',
                    '1',
                    'email[email-string]',
                    '',
                    '',
                    '',
                    'search_string_type',
                    '',
                ],
                [
                    gettext('First Used'),
                    'INPUT',
                    [
                        'name'  => 'first_used[0]',
                        '',
                        'size'  => '20',
                        'class' => "text field",
                        'id'    => 'first_used',
                    ],
                    '',
                    'tOOL TIP',
                    '',
                    'first_used[first_used-date]',
                ],
                [
                    gettext('Expiry Date'),
                    'INPUT',
                    [
                        'name'  => 'expiry[0]',
                        'id'    => 'expiry',
                        'size'  => '20',
                        'class' => "text field ",
                    ],
                    '',
                    'tOOL TIP',
                    '',
                    'expiry[expiry-date]',
                ],
                [
                    gettext('Rate Group'),
                    'pricelist_id',
                    'SELECT',
                    '',
                    '',
                    'tOOL TIP',
                    'Please Enter account number',
                    'id',
                    'name',
                    'pricelists',
                    'build_dropdown',
                    'where_arr',
                    [
                        "status"      => "0",
                        "reseller_id" => "0",
                    ],
                ],
                [
                    gettext('Status'),
                    'status',
                    'SELECT',
                    '',
                    '',
                    'tOOL TIP',
                    'Please Enter account number',
                    '',
                    '',
                    '',
                    'set_search_status',
                ],
                [
                    gettext('Created Date'),
                    'INPUT',
                    [
                        'name'  => 'creation[0]',
                        '',
                        'size'  => '20',
                        'class' => "text field",
                        'id'    => 'creation',
                    ],
                    '',
                    'tOOL TIP',
                    '',
                    'creation[creation-date]',
                ],
                [
                    gettext('Entity Type'),
                    'type',
                    'SELECT',
                    '',
                    '',
                    'tOOL TIP',
                    'Please Enter account number',
                    '',
                    '',
                    '',
                    'set_entity_type_customer',
                ],
                [
                    gettext('Account Type'),
                    'posttoexternal',
                    'SELECT',
                    '',
                    '',
                    'tOOL TIP',
                    'Please Enter account number',
                    '',
                    '',
                    '',
                    'set_account_type_search',
                ],
                [
                    gettext('Billing Cycle'),
                    'sweep_id',
                    'SELECT',
                    '',
                    '',
                    'tOOL TIP',
                    'Please Enter account number',
                    '',
                    '',
                    '',
                    'set_Billing_Schedule_status',
                ],
                [
                    '',
                    'HIDDEN',
                    'ajax_search',
                    '1',
                    '',
                    '',
                    '',
                ],
                [
                    '',
                    'HIDDEN',
                    'advance_search',
                    '1',
                    '',
                    '',
                    '',
                ],
            ];
        } else {

            $form ['forms'] = [
                "",
                [
                    'id' => "account_search",
                ],
            ];
            $form [gettext('Search')] = [
                [
                    gettext('Account'),
                    'INPUT',
                    [
                        'name'  => 'number[number]',
                        '',
                        'size'  => '20',
                        'class' => "text field ",
                    ],
                    '',
                    'tOOL TIP',
                    '1',
                    'number[number-string]',
                    '',
                    '',
                    '',
                    'search_string_type',
                    '',
                ],
                [
                    gettext('First Name'),
                    'INPUT',
                    [
                        'name'  => 'first_name[first_name]',
                        '',
                        'id'    => 'first_name',
                        'size'  => '15',
                        'class' => "text field ",
                    ],
                    '',
                    'tOOL TIP',
                    '1',
                    'first_name[first_name-string]',
                    '',
                    '',
                    '',
                    'search_string_type',
                    '',
                ],
                [
                    gettext('Last Name'),
                    'INPUT',
                    [
                        'name'  => 'last_name[last_name]',
                        'value' => '',
                        'size'  => '20',
                        'class' => "text field ",
                    ],
                    '',
                    'Tool tips info',
                    '1',
                    'last_name[last_name-string]',
                    '',
                    '',
                    '',
                    'search_string_type',
                    '',
                ],
                [
                    gettext('Company'),
                    'INPUT',
                    [
                        'name'  => 'company_name[company_name]',
                        'value' => '',
                        'size'  => '20',
                        'class' => "text field ",
                    ],
                    '',
                    'Tool tips info',
                    '1',
                    'company_name[company_name-string]',
                    '',
                    '',
                    '',
                    'search_string_type',
                    '',
                ],
                [
                    gettext('Rate Group'),
                    'pricelist_id',
                    'SELECT',
                    '',
                    '',
                    'tOOL TIP',
                    'Please Enter account number',
                    'id',
                    'name',
                    'pricelists',
                    'build_dropdown',
                    'where_arr',
                    [
                        "status"      => "0",
                        "reseller_id" => "0",
                    ],
                ],
                [
                    gettext('Balance'),
                    'INPUT',
                    [
                        'name'  => 'balance[balance]',
                        'value' => '',
                        'size'  => '20',
                        'class' => "text field ",
                    ],
                    '',
                    'Tool tips info',
                    '1',
                    'balance[balance-integer]',
                    '',
                    '',
                    '',
                    'search_int_type',
                    '',
                ],
                [
                    gettext('Credit Limit'),
                    'INPUT',
                    [
                        'name'  => 'credit_limit[credit_limit]',
                        'value' => '',
                        'size'  => '20',
                        'class' => "text field ",
                    ],
                    '',
                    'Tool tips info',
                    '1',
                    'credit_limit[credit_limit-integer]',
                    '',
                    '',
                    '',
                    'search_int_type',
                    '',
                ],
                [
                    gettext('Email'),
                    'INPUT',
                    [
                        'name'  => 'email[email]',
                        'value' => '',
                        'size'  => '20',
                        'class' => "text field ",
                    ],
                    '',
                    'Tool tips info',
                    '1',
                    'email[email-string]',
                    '',
                    '',
                    '',
                    'search_string_type',
                    '',
                ],
                [
                    gettext('First Used'),
                    'INPUT',
                    [
                        'name'  => 'first_used[0]',
                        '',
                        'size'  => '20',
                        'class' => "text field",
                        'id'    => 'first_used',
                    ],
                    '',
                    'tOOL TIP',
                    '',
                    'first_used[first_used-date]',
                ],
                [
                    gettext('Expiry Date'),
                    'INPUT',
                    [
                        'name'  => 'expiry[0]',
                        'id'    => 'expiry',
                        'size'  => '20',
                        'class' => "text field ",
                    ],
                    '',
                    'tOOL TIP',
                    '',
                    'expiry[expiry-date]',
                ],
                [
                    'CC',
                    'INPUT',
                    [
                        'name'  => 'maxchannels[maxchannels]',
                        'value' => '',
                        'size'  => '20',
                        'class' => "text field ",
                    ],
                    '',
                    'Tool tips info',
                    '1',
                    'maxchannels[maxchannels-integer]',
                    '',
                    '',
                    '',
                    'search_int_type',
                ],
                [
                    gettext('Status'),
                    'status',
                    'SELECT',
                    '',
                    '',
                    'tOOL TIP',
                    'Please Enter account number',
                    '',
                    '',
                    '',
                    'set_search_status',
                ],
                [
                    gettext('Created Date'),
                    'INPUT',
                    [
                        'name'  => 'creation[0]',
                        '',
                        'size'  => '20',
                        'class' => "text field",
                        'id'    => 'creation',
                    ],
                    '',
                    'tOOL TIP',
                    '',
                    'creation[creation-date]',
                ],
                [
                    gettext('Account Type'),
                    'posttoexternal',
                    'SELECT',
                    '',
                    '',
                    'tOOL TIP',
                    'Please Enter account number',
                    '',
                    '',
                    '',
                    'set_account_type_search',
                ],
                [
                    gettext('Billing Cycle'),
                    'sweep_id',
                    'SELECT',
                    '',
                    '',
                    'tOOL TIP',
                    'Please Enter account number',
                    '',
                    '',
                    '',
                    'set_Billing_Schedule_status',
                ],
                [
                    '',
                    'HIDDEN',
                    'ajax_search',
                    '1',
                    '',
                    '',
                    '',
                ],
                [
                    '',
                    'HIDDEN',
                    'advance_search',
                    '1',
                    '',
                    '',
                    '',
                ],
            ];
        }

        $form ['button_search'] = [
            'name'    => 'action',
            'id'      => "account_search_btn",
            'content' => gettext('Search'),
            'value'   => 'save',
            'type'    => 'button',
            'class'   => 'btn btn-line-parrot pull-right',
        ];
        $form ['button_reset'] = [
            'name'    => 'action',
            'id'      => "id_reset",
            'content' => gettext('Clear'),
            'value'   => 'cancel',
            'type'    => 'reset',
            'class'   => 'btn btn-line-sky pull-right margin-x-10',
        ];

        return $form;
    }

    function get_reseller_search_form()
    {
        $form ['forms'] = [
            "",
            [
                'id' => "account_search",
            ],
        ];
        $form [gettext('Search')] = [
            [
                gettext('Account'),
                'INPUT',
                [
                    'name'  => 'number[number]',
                    '',
                    'size'  => '20',
                    'class' => "text field ",
                ],
                '',
                'tOOL TIP',
                '1',
                'number[number-string]',
                '',
                '',
                '',
                'search_string_type',
                '',
            ],
            [
                gettext('First Name'),
                'INPUT',
                [
                    'name'  => 'first_name[first_name]',
                    '',
                    'id'    => 'first_name',
                    'size'  => '15',
                    'class' => "text field ",
                ],
                '',
                'tOOL TIP',
                '1',
                'first_name[first_name-string]',
                '',
                '',
                '',
                'search_string_type',
                '',
            ],
            [
                gettext('Last Name'),
                'INPUT',
                [
                    'name'  => 'last_name[last_name]',
                    'value' => '',
                    'size'  => '20',
                    'class' => "text field ",
                ],
                '',
                'Tool tips info',
                '1',
                'last_name[last_name-string]',
                '',
                '',
                '',
                'search_string_type',
                '',
            ],
            [
                gettext('Email'),
                'INPUT',
                [
                    'name'  => 'email[email]',
                    'value' => '',
                    'size'  => '20',
                    'class' => "text field ",
                ],
                '',
                'Tool tips info',
                '1',
                'email[email-string]',
                '',
                '',
                '',
                'search_string_type',
                '',
            ],
            [
                gettext('Company'),
                'INPUT',
                [
                    'name'  => 'company_name[company_name]',
                    'value' => '',
                    'size'  => '20',
                    'class' => "text field ",
                ],
                '',
                'Tool tips info',
                '1',
                'company_name[company_name-string]',
                '',
                '',
                '',
                'search_string_type',
                '',
            ],
            [
                gettext('Rate Group'),
                'pricelist_id',
                'SELECT',
                '',
                '',
                'tOOL TIP',
                'Please Enter account number',
                'id',
                'name',
                'pricelists',
                'build_dropdown',
                'where_arr',
                [
                    "status"      => "0",
                    "reseller_id" => "0",
                ],
            ],
            [
                gettext('Account Type'),
                'posttoexternal',
                'SELECT',
                '',
                '',
                'tOOL TIP',
                'Please Enter account number',
                '',
                '',
                '',
                'set_account_type_search',
            ],
            [
                gettext('Balance'),
                'INPUT',
                [
                    'name'  => 'balance[balance]',
                    'value' => '',
                    'size'  => '20',
                    'class' => "text field ",
                ],
                '',
                'Tool tips info',
                '1',
                'balance[balance-integer]',
                '',
                '',
                '',
                'search_int_type',
                '',
            ],

            [
                gettext('Credit Limit'),
                'INPUT',
                [
                    'name'  => 'credit_limit[credit_limit]',
                    'value' => '',
                    'size'  => '20',
                    'class' => "text field ",
                ],
                '',
                'Tool tips info',
                '1',
                'credit_limit[credit_limit-integer]',
                '',
                '',
                '',
                'search_int_type',
                '',
            ],
            [
                gettext('Status'),
                'status',
                'SELECT',
                '',
                '',
                'tOOL TIP',
                'Please Enter account number',
                '',
                '',
                '',
                'set_search_status',
            ],
            [
                gettext('Created Date'),
                'INPUT',
                [
                    'name'  => 'creation[0]',
                    '',
                    'size'  => '20',
                    'class' => "text field",
                    'id'    => 'creation',
                ],
                '',
                'tOOL TIP',
                '',
                'creation[creation-date]',
            ],
            [
                '',
                'HIDDEN',
                'ajax_search',
                '1',
                '',
                '',
                '',
            ],
            [
                '',
                'HIDDEN',
                'advance_search',
                '1',
                '',
                '',
                '',
            ],
        ];

        $form ['button_search'] = [
            'name'    => 'action',
            'id'      => "account_search_btn",
            'content' => gettext('Search'),
            'value'   => 'save',
            'type'    => 'button',
            'class'   => 'btn btn-line-parrot pull-right',
        ];
        $form ['button_reset'] = [
            'name'    => 'action',
            'id'      => "id_reset",
            'content' => gettext('Clear'),
            'value'   => 'cancel',
            'type'    => 'reset',
            'class'   => 'btn btn-line-sky pull-right margin-x-10',
        ];

        return $form;
    }

    function get_admin_search_form()
    {
        $form ['forms'] = [
            "",
            [
                'id' => "account_search",
            ],
        ];
        $form ['Search'] = [
            [
                gettext('Account'),
                'INPUT',
                [
                    'name'  => 'number[number]',
                    '',
                    'size'  => '20',
                    'class' => "text field",
                ],
                '',
                'tOOL TIP',
                '1',
                'number[number-string]',
                '',
                '',
                '',
                'search_string_type',
                '',
            ],
            [
                gettext('First Name'),
                'INPUT',
                [
                    'name'  => 'first_name[first_name]',
                    '',
                    'id'    => 'first_name',
                    'size'  => '15',
                    'class' => "text field ",
                ],
                '',
                'tOOL TIP',
                '1',
                'first_name[first_name-string]',
                '',
                '',
                '',
                'search_string_type',
                '',
            ],
            [
                gettext('Last Name'),
                'INPUT',
                [
                    'name'  => 'last_name[last_name]',
                    'value' => '',
                    'size'  => '20',
                    'class' => "text field ",
                ],
                '',
                'Tool tips info',
                '1',
                'last_name[last_name-string]',
                '',
                '',
                '',
                'search_string_type',
                '',
            ],
            [
                gettext('Company'),
                'INPUT',
                [
                    'name'  => 'company_name[company_name]',
                    'value' => '',
                    'size'  => '20',
                    'class' => "text field ",
                ],
                '',
                'Tool tips info',
                '1',
                'company_name[company_name-string]',
                '',
                '',
                '',
                'search_string_type',
                '',
            ],
            [
                gettext('Email'),
                'INPUT',
                [
                    'name'  => 'email[email]',
                    'value' => '',
                    'size'  => '20',
                    'class' => "text field ",
                ],
                '',
                'Tool tips info',
                '1',
                'email[email-string]',
                '',
                '',
                '',
                'search_string_type',
                '',
            ],
            [
                gettext('Entity Type'),
                'type',
                'SELECT',
                '',
                '',
                'tOOL TIP',
                'Please Enter account number',
                '',
                '',
                '',
                'set_entity_type_admin',
            ],
            [
                gettext('Phone'),
                'INPUT',
                [
                    'name'  => 'telephone_1[telephone_1]',
                    'value' => '',
                    'size'  => '20',
                    'class' => "text field ",
                ],
                '',
                'Tool tips info',
                '1',
                'telephone_1[telephone_1-integer]',
                '',
                '',
                '',
                'search_int_type',
                '',
            ],
            [
                gettext('Country'),
                'country_id',
                'SELECT',
                '',
                '',
                'tOOL TIP',
                'Please Enter account number',
                'id',
                'country',
                'countrycode',
                'build_dropdown',
                '',
                '',
            ],
            [
                'Status',
                'status',
                'SELECT',
                '',
                '',
                'tOOL TIP',
                'Please Enter account number',
                '',
                '',
                '',
                'set_search_status',
            ],
            [
                '',
                'HIDDEN',
                'ajax_search',
                '1',
                '',
                '',
                '',
            ],
            [
                '',
                'HIDDEN',
                'advance_search',
                '1',
                '',
                '',
                '',
            ],
        ];

        $form ['button_search'] = [
            'name'    => 'action',
            'id'      => "account_search_btn",
            'content' => gettext('Search'),
            'value'   => 'save',
            'type'    => 'button',
            'class'   => 'btn btn-line-parrot pull-right',
        ];
        $form ['button_reset'] = [
            'name'    => 'action',
            'id'      => "id_reset",
            'content' => gettext('Clear'),
            'value'   => 'cancel',
            'type'    => 'reset',
            'class'   => 'btn btn-line-sky pull-right margin-x-10',
        ];

        return $form;
    }

    function build_account_list_for_admin()
    {
        // array(display name, width, db_field_parent_table,feidname, db_field_child_table,function name);
        $grid_field_arr = json_encode(
            [
                [
                    "<input type='checkbox' name='chkAll' class='ace checkall'/><label class='lbl'></label>",
                    "30",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "false",
                    "center",
                ],
                [
                    gettext("Account"),
                    "135",
                    "number",
                    "number",
                    "accounts",
                    "account_number_icon",
                    "EDITABLE",
                    "true",
                    "left",
                ],
                [
                    gettext("First Name"),
                    "150",
                    "first_name",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Last Name"),
                    "150",
                    "last_name",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Company"),
                    "150",
                    "company_name",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Email"),
                    "170",
                    "email",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Phone"),
                    "150",
                    "telephone_1",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Country"),
                    "110",
                    "country_id",
                    "country",
                    "countrycode",
                    "get_field_name",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Status"),
                    "110",
                    "status",
                    "status",
                    "accounts",
                    "get_status",
                    "",
                    "true",
                    "center",
                ],
                /**
                 * ****************************************************************
                 */
                [
                    gettext("Action"),
                    "100",
                    "",
                    "",
                    "",
                    [
                        "EDIT"   => [
                            "url"  => "accounts/admin_edit/",
                            "mode" => "single",
                        ],
                        "DELETE" => [
                            "url"  => "accounts/admin_delete/",
                            "mode" => "single",
                        ],
                    ],
                ],
            ]
        );

        return $grid_field_arr;
    }

    function build_account_list_for_customer()
    {
        $account_info = $accountinfo = $this->CI->session->userdata('accountinfo');
        $currency_id = $account_info ['currency_id'];
        $currency = $this->CI->common->get_field_name('currency', 'currency', $currency_id);

        // array(display name, width, db_field_parent_table,feidname, db_field_child_table,function name);
        $grid_field_arr = json_encode(
            [
                [
                    "<input type='checkbox' name='chkAll' class='ace checkall'/><label class='lbl'></label>",
                    "30",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "false",
                    "center",
                ],
                [
                    gettext("Account"),
                    "125",
                    "number",
                    "number",
                    "accounts",
                    "account_number_icon",
                    "EDITABLE",
                    "true",
                    "left",
                ],
                [
                    gettext("First Name"),
                    "95",
                    "first_name",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Last Name"),
                    "95",
                    "last_name",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Company"),
                    "85",
                    "company_name",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Rate Group"),
                    "85",
                    "pricelist_id",
                    "name",
                    "pricelists",
                    "get_field_name",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Balance")." ($currency)",
                    "100",
                    "balance",
                    "balance",
                    "balance",
                    "convert_to_currency_account",
                    "",
                    "true",
                    "right",
                ],
                [
                    gettext("Credit Limit")." ($currency)",
                    "120",
                    "credit_limit",
                    "credit_limit",
                    "credit_limit",
                    "convert_to_currency_account",
                    "",
                    "true",
                    "right",
                ],
                [
                    gettext("First Used"),
                    "80",
                    "first_used",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Expiry Date"),
                    "80",
                    "expiry",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "center",
                ],
                [
                    "CC",
                    "45",
                    "maxchannels",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Status"),
                    "90",
                    "status",
                    "status",
                    "accounts",
                    "get_status",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Created Date"),
                    "90",
                    "creation",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "center",
                ],
                /**
                 * *********************************************************************
                 */
                [
                    gettext("Action"),
                    "140",
                    "",
                    "",
                    "",
                    [
                        "PAYMENT"  => [
                            "url"  => "accounts/customer_payment_process_add/",
                            "mode" => "single",
                        ],
                        "CALLERID" => [
                            "url"  => "accounts/customer_add_callerid/",
                            "mode" => "popup",
                        ],
                        "EDIT"     => [
                            "url"  => "accounts/customer_edit/",
                            "mode" => "single",
                        ],
                        "DELETE"   => [
                            "url"  => "accounts/customer_delete/",
                            "mode" => "single",
                        ],
                    ],
                ],
            ]
        );

        return $grid_field_arr;
    }

    function build_account_list_for_reseller()
    {
        $account_info = $accountinfo = $this->CI->session->userdata('accountinfo');
        $currency_id = $account_info ['currency_id'];
        $currency = $this->CI->common->get_field_name('currency', 'currency', $currency_id);

        // array(display name, width, db_field_parent_table,feidname, db_field_child_table,function name);
        $grid_field_arr = json_encode(
            [
                [
                    "<input type='checkbox' name='chkAll' class='ace checkall'/><label class='lbl'></label>",
                    "30",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "false",
                    "center",
                ],
                [
                    gettext("Account"),
                    "105",
                    "number",
                    "",
                    "",
                    "",
                    "EDITABLE",
                    "true",
                    "center",
                ],
                [
                    gettext("First Name"),
                    "120",
                    "first_name",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Last Name"),
                    "115",
                    "last_name",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Company"),
                    "130",
                    "company_name",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Rate Group"),
                    "95",
                    "pricelist_id",
                    "name",
                    "pricelists",
                    "get_field_name",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Account Type"),
                    "107",
                    "posttoexternal",
                    "posttoexternal",
                    "posttoexternal",
                    "get_account_type",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Balance")." ($currency)",
                    "100",
                    "balance",
                    "balance",
                    "balance",
                    "convert_to_currency",
                    "",
                    "true",
                    "right",
                ],
                [
                    gettext("Credit Limit")." ($currency)",
                    "120",
                    "credit_limit",
                    "credit_limit",
                    "credit_limit",
                    "convert_to_currency",
                    "",
                    "true",
                    "right",
                ],
                [
                    gettext("Status"),
                    "110",
                    "status",
                    "status",
                    "accounts",
                    "get_status",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Created Date"),
                    "90",
                    "creation",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "center",
                ],
                /**
                 * ***********************************************************
                 */
                [
                    gettext("Action"),
                    "139",
                    "",
                    "",
                    "",
                    [
                        "PAYMENT"  => [
                            "url"  => "accounts/customer_payment_process_add/",
                            "mode" => "single",
                        ],
                        "CALLERID" => [
                            "url"  => "accounts/customer_add_callerid/",
                            "mode" => 'popup',
                        ],
                        "EDIT"     => [
                            "url"  => "accounts/reseller_edit/",
                            "mode" => "single",
                        ],
                        "DELETE"   => [
                            "url"  => "accounts/reseller_delete/",
                            "mode" => "single",
                        ],
                    ],
                ],
            ]
        );

        return $grid_field_arr;
    }

    function build_grid_buttons_customer()
    {
        $logintype = $this->CI->session->userdata('userlevel_logintype');
        $provider = null;
        $account_import = [];
        if ($logintype != 1) {
            $account_import = [
                gettext("Import customers"),
                "btn btn-line-warning",
                "fa fa-upload fa-lg",
                "button_action",
                "/account_import/customer_import_mapper/",
                'single',
            ];
            $provider = [
                gettext("Create Provider"),
                "btn btn-line-blue btn",
                "fa fa-plus-circle fa-lg",
                "button_action",
                "/accounts/provider_add/",
            ];
        }
        // array(display name, width, db_field_parent_table,feidname, db_field_child_table,function name);
        $buttons_json = json_encode(
            [
                [
                    gettext("Create Customer"),
                    "btn btn-line-warning btn",
                    "fa fa-plus-circle fa-lg",
                    "button_action",
                    "/accounts/customer_add/",
                ],
                [
                    gettext("Mass Create"),
                    "btn btn-line-warning btn",
                    "fa fa-plus-circle fa-lg",
                    "button_action",
                    "/accounts/customer_bulk_creation/",
                    "popup",
                    "medium",
                ],
                $account_import,
                $provider,
                [
                    gettext("Export"),
                    "btn btn-xing",
                    " fa fa-download fa-lg",
                    "button_action",
                    "/accounts/customer_export_cdr_xls/",
                    'single',
                ],
                [
                    gettext("Delete"),
                    "btn btn-line-danger",
                    "fa fa-times-circle fa-lg",
                    "button_action",
                    "/accounts/customer_selected_delete/",
                ],
            ]
        );

        return $buttons_json;
    }

    function build_grid_buttons_admin()
    {
        $buttons_json = json_encode(
            [
                [
                    gettext("Create Admin"),
                    "btn btn-line-warning btn",
                    "fa fa-plus-circle fa-lg",
                    "button_action",
                    "/accounts/admin_add/",
                ],
                [
                    gettext("Create Subadmin"),
                    "btn btn-line-warning btn",
                    "fa fa-plus-circle fa-lg",
                    "button_action",
                    "/accounts/subadmin_add/4",
                ],
                [
                    gettext("Delete"),
                    "btn btn-line-danger",
                    "fa fa-times-circle fa-lg",
                    "button_action",
                    "/accounts/admin_selected_delete/",
                ],
            ]
        );

        return $buttons_json;
    }

    function build_grid_buttons_reseller()
    {
        $buttons_json = json_encode(
            [
                [
                    gettext("Create"),
                    "btn btn-line-warning btn",
                    "fa fa-plus-circle fa-lg",
                    "button_action",
                    "/accounts/reseller_add/",
                ],
                [
                    gettext("Export"),
                    "btn btn-xing",
                    " fa fa-download fa-lg",
                    "button_action",
                    "/accounts/reseller_export_cdr_xls",
                    'single',
                ],
                [
                    gettext("Delete"),
                    "btn btn-line-danger",
                    "fa fa-times-circle fa-lg",
                    "button_action",
                    "/accounts/reseller_selected_delete/",
                ],
            ]
        );

        return $buttons_json;
    }

    function build_ip_list_for_customer($accountid, $accountype)
    {
        $grid_field_arr = json_encode(
            [
                [
                    gettext('Name'),
                    "180",
                    "name",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext('IP'),
                    "180",
                    "ip",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext('Prefix'),
                    "180",
                    "prefix",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext('Created Date'),
                    "174",
                    "created_date",
                    "created_date",
                    "created_date",
                    "convert_GMT_to",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext('Modified Date'),
                    "160",
                    "last_modified_date",
                    "last_modified_date",
                    "last_modified_date",
                    "convert_GMT_to",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext('Action'),
                    "150",
                    "",
                    "",
                    "",
                    [
                        "DELETE" => [
                            "url"  => "accounts/".$accountype."_ipmap_action/delete/$accountid/$accountype/",
                            "mode" => "single",
                        ],
                    ],
                ],
            ]
        );

        return $grid_field_arr;
    }

    function build_animap_list_for_customer($accountid, $accounttype)
    {
        $grid_field_arr = json_encode(
            [
                [
                    gettext("Caller ID"),
                    "200",
                    "number",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Status"),
                    "180",
                    "status",
                    "status",
                    "ani_map",
                    "get_status",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Created Date"),
                    "200",
                    "creation_date",
                    "creation_date",
                    "creation_date",
                    "convert_GMT_to",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Modified Date"),
                    "170",
                    "last_modified_date",
                    "last_modified_date",
                    "last_modified_date",
                    "convert_GMT_to",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Action"),
                    "200",
                    "",
                    "",
                    "",
                    [
                        "DELETE" => [
                            "url"  => "accounts/".$accounttype."_animap_action/delete/$accountid/",
                            "mode" => "single",
                        ],
                    ],
                ],
            ]
        );

        return $grid_field_arr;
    }

    function build_sipiax_list_for_customer()
    {
        $grid_field_arr = json_encode(
            [
                [
                    "Tech",
                    "150",
                    "tech",
                    "",
                    "",
                    "",
                ],
                [
                    gettext("Type"),
                    "150",
                    "type",
                    "",
                    "",
                    "",
                ],
                [
                    gettext("User Name"),
                    "150",
                    "username",
                    "sweep",
                    "sweeplist",
                    "get_field_name",
                ],
                [
                    gettext("Password"),
                    "150",
                    "secret",
                    "",
                    "",
                    "",
                ],
                [
                    gettext("Context"),
                    "150",
                    "context",
                    "",
                    "",
                    "",
                ],
            ]
        );

        return $grid_field_arr;
    }

    function set_block_pattern_action_buttons($id)
    {
        $ret_url = '';
        $ret_url .= '<a href="/did/delete/'.$id.'/" class="icon delete_image" title="Delete" onClick="return get_alert_msg();">&nbsp;</a>';

        return $ret_url;
    }

    function build_animap_list()
    {
        $grid_field_arr = json_encode(
            [
                [
                    "Caller ID",
                    "180",
                    "number",
                    "",
                    "",
                    "",
                ],
                [
                    gettext("status"),
                    "180",
                    "status",
                    "status",
                    "animap",
                    "get_status",
                ],
                [
                    gettext("Action"),
                    "130",
                    "",
                    "",
                    "",
                    [
                        "EDIT_ANIMAP"   => [
                            "url"  => "accounts/callingcards_animap_list_edit/",
                            "mode" => "single",
                        ],
                        "DELETE_ANIMAP" => [
                            "url"  => "accounts/callingcards_animap_list_remove/",
                            "mode" => "single",
                        ],
                    ],
                ],
            ]
        );

        return $grid_field_arr;
    }

    function build_grid_buttons_destination()
    {
        $buttons_json = json_encode([]);

        return $buttons_json;
    }
}

?>
