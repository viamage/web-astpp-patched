<?php

// ##############################################################################
// ASTPP - Open Source VoIP Billing Solution
//
// Copyright (C) 2016 iNextrix Technologies Pvt. Ltd.
// Samir Doshi <samir.doshi@inextrix.com>
// ASTPP Version 3.0 and above
// License https://www.gnu.org/licenses/agpl-3.0.html
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
// ##############################################################################
if (!defined('BASEPATH')) {
    exit ('No direct script access allowed');
}

class did_form
{
    function __construct()
    {
        $this->CI = &get_instance();
    }

    function get_dids_form_fields($id = false, $parent_id = '0', $account_id = '0', $country_id = false)
    {
        if ($id != 0) {
            if ($parent_id > 0) {
                $account_dropdown = [
                    'Reseller',
                    [
                        'name'     => 'parent_id',
                        'disabled' => 'disabled',
                        'class'    => 'accountid',
                        'id'       => 'accountid',
                    ],
                    'SELECT',
                    '',
                    '',
                    'tOOL TIP',
                    'Please Enter account number',
                    'id',
                    'first_name,last_name,number',
                    'accounts',
                    'build_concat_dropdown',
                    'where_arr',
                    [
                        "reseller_id" => "0",
                        "type"        => "1",
                        "deleted"     => "0",
                        "status"      => "0",
                    ],
                ];
            } else {
                if ($account_id > 0) {
                    $account_dropdown = [
                        'Account ',
                        [
                            'name'     => 'accountid',
                            'disabled' => 'disabled',
                            'class'    => 'accountid',
                            'id'       => 'accountid',
                        ],
                        'SELECT',
                        '',
                        '',
                        'tOOL TIP',
                        'Please Enter account number',
                        'id',
                        'first_name,last_name,number',
                        'accounts',
                        'build_concat_dropdown',
                        'where_arr',
                        [
                            "reseller_id" => "0",
                            "type"        => "0,3",
                            "deleted"     => "0",
                            "status"      => "0",
                        ],
                    ];
                } else {
                    $account_dropdown = [
                        'Account',
                        'accountid',
                        'SELECT',
                        '',
                        [
                            "name"  => "accountid",
                            "rules" => "did_account_checking",
                        ],
                        'tOOL TIP',
                        'Please Enter account number',
                        'id',
                        'first_name,last_name,number',
                        'accounts',
                        'build_concat_dropdown',
                        'where_arr',
                        [
                            "reseller_id" => "0",
                            "type"        => "0,3",
                            "deleted"     => "0",
                            "status"      => "0",
                        ],
                    ];
                }
            }
        } else {
            $account_dropdown = [
                'Account',
                'accountid',
                'SELECT',
                '',
                [
                    "name"  => "accountid",
                    "rules" => "did_account_checking",
                ],
                'tOOL TIP',
                'Please Enter account number',
                'id',
                'first_name,last_name,number',
                'accounts',
                'build_concat_dropdown',
                'where_arr',
                [
                    "reseller_id" => "0",
                    "type"        => "0,3",
                    "deleted"     => "0",
                    "status"      => "0",
                ],
            ];
        }
        if (!$country_id) {

            $country = [
                'Country',
                [
                    'name'  => 'country_id',
                    'class' => 'country_id',
                ],
                'SELECT',
                '',
                [
                    "name"  => "country_id",
                    "rules" => "required",
                ],
                'tOOL TIP',
                'Please Enter account number',
                'id',
                'country',
                'countrycode',
                'build_dropdown',
                '',
                '',
            ];
        } else {
            $country = [
                'Country',
                [
                    'name'  => 'country_id',
                    'class' => 'country_id',
                    'vlaue' => $country_id,
                ],
                'SELECT',
                '',
                [
                    "name"     => "country_id",
                    "rules"    => "required",
                    'selected' => 'selected',
                ],
                'tOOL TIP',
                'Please Enter account number',
                'id',
                'country',
                'countrycode',
                'build_dropdown',
                '',
                '',
            ];
        }

        $val = $id > 0 ? 'dids.number.'.$id : 'dids.number';
        $form ['forms'] = [
            base_url().'/did/did_save/',
            [
                'id'     => 'did_form',
                'method' => 'POST',
                'name'   => 'did_form',
            ],
        ];
        $form ['DID Information'] = [
            [
                '',
                'HIDDEN',
                [
                    'name' => 'id',
                ],
                '',
                '',
                '',
                '',
            ],
            [
                gettext('DID'),
                'INPUT',
                [
                    'name'  => 'number',
                    'size'  => '20',
                    'class' => "text field medium",
                ],
                'trim|required|is_numeric|xss_clean|integer|is_unique['.$val.']',
                'tOOL TIP',
                'Please Enter account number',
            ],
            $country,

            [
                gettext('City'),
                'INPUT',
                [
                    'name'  => 'city',
                    'size'  => '20',
                    'class' => "text field medium",
                ],
                '',
                'tOOL TIP',
                'Please Enter Password',
            ],
            [
                gettext('Province'),
                'INPUT',
                [
                    'name'  => 'province',
                    'size'  => '15',
                    'class' => "text field medium",
                ],
                '',
                'tOOL TIP',
                'Please Enter Password',
            ],
            [
                gettext('Provider'),
                'provider_id',
                'SELECT',
                '',
                '',
                'tOOL TIP',
                'Please Enter account number',
                'id',
                'first_name,last_name,number',
                'accounts',
                'build_concat_dropdown',
                'where_arr',
                [
                    "type"    => "3",
                    "deleted" => "0",
                    "status"  => "0",
                ],
            ],
        ];

        $form ['Billing Information'] = [
            $account_dropdown,
            [
                gettext('Connection Cost'),
                'INPUT',
                [
                    'name'  => 'connectcost',
                    'size'  => '15',
                    'class' => "text field medium",
                ],
                'trim|is_numeric|xss_clean',
                'tOOL TIP',
                'Please Enter Password',
            ],
            [
                gettext('Included Seconds'),
                'INPUT',
                [
                    'name'  => 'includedseconds',
                    'size'  => '50',
                    'class' => "text field medium",
                ],
                'trim|is_numeric|xss_clean',
                'tOOL TIP',
                'Please Enter Password',
            ],
            [
                gettext('Per Minute Cost'),
                'INPUT',
                [
                    'name'  => 'cost',
                    'size'  => '15',
                    'class' => "text field medium",
                ],
                'trim|is_numeric|xss_clean',
                'tOOL TIP',
                'Please Enter Password',
            ],
            [
                gettext('Initial Increment'),
                'INPUT',
                [
                    'name'  => 'init_inc',
                    'size'  => '20',
                    'class' => "text field medium",
                ],
                'trim|is_numeric|xss_clean',
                'tOOL TIP',
                'Please Enter Initial Increment',
            ],
            [
                gettext('Increment'),
                'INPUT',
                [
                    'name'  => 'inc',
                    'size'  => '20',
                    'class' => "text field medium",
                ],
                'trim|is_numeric|xss_clean',
                'tOOL TIP',
                'Please Enter Increment',
            ],
            [
                gettext('Setup Fee'),
                'INPUT',
                [
                    'name'  => 'setup',
                    'size'  => '15',
                    'class' => 'text field medium',
                ],
                'trim|is_numeric|xss_clean',
                'tOOL TIP',
                '',
            ],
            [
                gettext('Monthly<br>Fee'),
                'INPUT',
                [
                    'name'  => 'monthlycost',
                    'size'  => '15',
                    'class' => "text field medium",
                ],
                'trim|is_numeric|xss_clean',
                'tOOL TIP',
                'Please Enter Password',
            ],
            // Added call leg_timeout parameter to timeout the calls.
            [
                'Call Timeout (Sec.)',
                'INPUT',
                [
                    'name'  => 'leg_timeout',
                    'size'  => '4',
                    'class' => "text field medium",
                ],
                'trim|xss_clean',
                'tOOL TIP',
                'Please Enter Call Leg Timeout',
            ],
        ];

        $form ['DID Setting'] = [
            [
                gettext('Call Type'),
                'call_type',
                'SELECT',
                '',
                '',
                'tOOL TIP',
                '',
                '',
                '',
                '',
                'set_call_type',
                '',
            ],
            [
                gettext('Destination'),
                'INPUT',
                [
                    'name'  => 'extensions',
                    'size'  => '20',
                    'class' => "text field medium",
                ],
                '',
                'tOOL TIP',
                'Please Enter Password',
            ],
            [
                gettext('Concurrent Calls'),
                'INPUT',
                [
                    'name'  => 'maxchannels',
                    'size'  => '20',
                    'class' => "text field medium",
                ],
                'trim|is_numeric|xss_clean',
                'tOOL TIP',
                'Please Enter account number',
            ],
            [
                gettext('Status'),
                'status',
                'SELECT',
                '',
                '',
                'tOOL TIP',
                'Please Select Status',
                '',
                '',
                '',
                'set_status',
            ],
        ];

        $form ['button_save'] = [
            'name'    => 'action',
            'content' => gettext('Save'),
            'value'   => 'save',
            'id'      => 'submit',
            'type'    => 'button',
            'class'   => 'btn btn-line-parrot',
        ];
        $form ['button_cancel'] = [
            'name'           => 'action',
            'content'        => 'Close',
            gettext('value') => 'cancel',
            'type'           => 'button',
            'class'          => 'btn btn-line-sky margin-x-10',
            'onclick'        => 'return redirect_page(\'NULL\')',
        ];

        return $form;
    }

    /**
     * ************************************************************************
     */
    function get_search_did_form()
    {
        $form ['forms'] = [
            "",
            [
                'id' => "did_search",
            ],
        ];
        $form ['Search'] = [
            [
                gettext('DID'),
                'INPUT',
                [
                    'name'  => 'number[number]',
                    '',
                    'size'  => '20',
                    'class' => "text field",
                ],
                '',
                'tOOL TIP',
                '1',
                'number[number-string]',
                '',
                '',
                '',
                'search_string_type',
                '',
            ],
            [
                gettext('Country'),
                'country_id',
                'SELECT',
                '',
                '',
                'tOOL TIP',
                'Please Enter account number',
                'id',
                'country',
                'countrycode',
                'build_dropdown',
                '',
                '',
            ],
            [
                gettext('Account'),
                'accountid',
                'SELECT',
                '',
                '',
                'tOOL TIP',
                'Please Enter account number',
                'id',
                'first_name,last_name,number',
                'accounts',
                'build_concat_dropdown',
                'where_arr',
                [
                    "reseller_id" => "0",
                    "type"        => "0",
                    "deleted"     => "0",
                ],
            ],
            [
                gettext('Initial Increment'),
                'INPUT',
                [
                    'name'  => 'init_inc[init_inc]',
                    '',
                    'size'  => '20',
                    'class' => "text field ",
                ],
                '',
                'tOOL TIP',
                '1',
                'init_inc[init_inc-integer]',
                '',
                '',
                '',
                'search_int_type',
                '',
            ],
            [
                gettext('Call Type'),
                'call_type',
                'SELECT',
                '',
                '',
                'tOOL TIP',
                '',
                '',
                '',
                '',
                'set_call_type_search',
                '',
                '',
            ],
            [
                gettext('Destination'),
                'INPUT',
                [
                    'name'  => 'extensions[extensions]',
                    '',
                    'size'  => '20',
                    'class' => "text field",
                ],
                '',
                'tOOL TIP',
                '1',
                'extensions[extensions-string]',
                '',
                '',
                '',
                'search_string_type',
                '',
            ],
            [
                gettext('Status'),
                'status',
                'SELECT',
                '',
                '',
                'tOOL TIP',
                'Please Enter account number',
                '',
                '',
                '',
                'set_search_status',
                '',
                '',
            ],

            [
                '',
                'HIDDEN',
                'ajax_search',
                '1',
                '',
                '',
                '',
            ],
            [
                '',
                'HIDDEN',
                'advance_search',
                '1',
                '',
                '',
                '',
            ],
        ];

        $form ['button_search'] = [
            'name'    => 'action',
            'id'      => "did_search_btn",
            'content' => gettext('Search'),
            'value'   => 'save',
            'type'    => 'button',
            'class'   => 'btn btn-line-parrot pull-right',
        ];
        $form ['button_reset'] = [
            'name'    => 'action',
            'id'      => "id_reset",
            'content' => gettext('Clear'),
            'value'   => 'cancel',
            'type'    => 'reset',
            'class'   => 'btn btn-line-sky pull-right margin-x-10',
        ];

        return $form;
    }

    function get_search_did_form_for_reseller()
    {
        $form ['forms'] = [
            "",
            [
                'id' => "did_search",
            ],
        ];
        $form ['Search'] = [
            [
                gettext('DID'),
                'INPUT',
                [
                    'name'  => 'note[note]',
                    '',
                    'size'  => '20',
                    'class' => "text field",
                ],
                '',
                'tOOL TIP',
                '1',
                'note[note-string]',
                '',
                '',
                '',
                'search_string_type',
                '',
            ],
            [
                gettext('Account'),
                'accountid',
                'SELECT',
                '',
                '',
                'tOOL TIP',
                'Please Enter account number',
                'id',
                'first_name,last_name,number',
                'accounts',
                'build_concat_dropdown',
                'where_arr',
                [
                    "reseller_id" => "0",
                    "type"        => "0",
                    "deleted"     => "0",
                ],
            ],
            [
                gettext('Call Type'),
                'call_type',
                'SELECT',
                '',
                '',
                'tOOL TIP',
                '',
                '',
                '',
                '',
                'set_call_type_search',
                '',
                '',
            ],
            [
                gettext('Destination'),
                'INPUT',
                [
                    'name'  => 'extensions[extensions]',
                    '',
                    'size'  => '20',
                    'class' => "text field",
                ],
                '',
                'tOOL TIP',
                '1',
                'extensions[extensions-string]',
                '',
                '',
                '',
                'search_string_type',
                '',
            ],
            [
                gettext('Status'),
                'status',
                'SELECT',
                '',
                '',
                'tOOL TIP',
                'Please Enter account number',
                '',
                '',
                '',
                'set_search_status',
                '',
                '',
            ],
            [
                '',
                'HIDDEN',
                'ajax_search',
                '1',
                '',
                '',
                '',
            ],
            [
                '',
                'HIDDEN',
                'advance_search',
                '1',
                '',
                '',
                '',
            ],
        ];

        $form ['button_search'] = [
            'name'    => 'action',
            'id'      => "did_search_btn",
            'content' => gettext('Search'),
            'value'   => 'save',
            'type'    => 'button',
            'class'   => 'btn btn-line-parrot pull-right',
        ];
        $form ['button_reset'] = [
            'name'    => 'action',
            'id'      => "id_reset",
            'content' => gettext('Clear'),
            'value'   => 'cancel',
            'type'    => 'reset',
            'class'   => 'btn btn-line-sky pull-right margin-x-10',
        ];

        return $form;
    }

    /*
     * ASTPP 3.0 grid size is change.
     */
    function build_did_list_for_admin()
    {
        $account_info = $accountinfo = $this->CI->session->userdata('accountinfo');
        $currency_id = $account_info ['currency_id'];
        $currency = $this->CI->common->get_field_name('currency', 'currency', $currency_id);

        // array(display name, width, db_field_parent_table,feidname, db_field_child_table,function name);
        $grid_field_arr = json_encode(
            [
                [
                    "<input type='checkbox' name='chkAll' class='ace checkall'/><label class='lbl'></label>",
                    "30",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "false",
                    "center",
                ],

                /**
                 * ASTPP 3.0
                 * For DID edit on DID number
                 * *
                 */
                [
                    gettext("DID"),
                    "80",
                    "number",
                    "",
                    "",
                    "",
                    "EDITABLE",
                    "true",
                    "center",
                ],
                [
                    gettext("Country"),
                    "60",
                    "country_id",
                    "country",
                    "countrycode",
                    "get_field_name",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Account"),
                    "95",
                    "accountid",
                    "first_name,last_name,number",
                    "accounts",
                    "get_field_name_coma_new",
                ],
                [
                    gettext("Per Minute <br>Cost($currency)"),
                    "85",
                    "cost",
                    "cost",
                    "cost",
                    "convert_to_currency",
                    "",
                    "true",
                    "right",
                ],
                [
                    gettext("Initial <br>Increment"),
                    "80",
                    "init_inc",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Increment"),
                    "90",
                    "inc",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Setup <br>Fee($currency)"),
                    "70",
                    "setup",
                    "setup",
                    "setup",
                    "convert_to_currency",
                    "",
                    "true",
                    "right",
                ],
                [
                    gettext("Monthly<br>Fee($currency)"),
                    "90",
                    "monthlycost",
                    "monthlycost",
                    "monthlycost",
                    "convert_to_currency",
                    "",
                    "true",
                    "right",
                ],
                [
                    gettext("Call Type"),
                    "90",
                    "call_type",
                    "call_type",
                    "call_type",
                    "get_call_type",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Destination"),
                    "80",
                    "extensions",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Status"),
                    "90",
                    "status",
                    "status",
                    "dids",
                    "get_status",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Modified <br/>Date"),
                    "90",
                    "last_modified_date",
                    "last_modified_date",
                    "last_modified_date",
                    "convert_GMT_to",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Is Purchased?"),
                    "110",
                    "number",
                    "number",
                    "number",
                    "check_did_avl",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Action"),
                    "100",
                    "",
                    "",
                    "",
                    [
                        "EDIT"   => [
                            "url"    => "did/did_edit/",
                            "mode"   => "popup",
                            "layout" => "medium",
                        ],
                        "DELETE" => [
                            "url"  => "did/did_remove/",
                            "mode" => "single",
                        ],
                    ],
                ],
            ]
        );

        return $grid_field_arr;
    }
    /**
     * **************************************************************************************
     */
    /*
     * ASTPP 3.0
     * change in grid size
     */
    function build_did_list_for_reseller_login()
    {
        $account_info = $accountinfo = $this->CI->session->userdata('accountinfo');
        $currency_id = $account_info ['currency_id'];
        $currency = $this->CI->common->get_field_name('currency', 'currency', $currency_id);
        // array(display name, width, db_field_parent_table,feidname, db_field_child_table,function name);
        $grid_field_arr = json_encode(
            [
                [
                    gettext("DID"),
                    "90",
                    "number",
                    "",
                    "",
                    "",
                    "EDITABLE",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Account"),
                    "100",
                    "accountid",
                    "first_name,last_name,number",
                    "accounts",
                    "get_field_name_coma_new",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Per Minute<br>Cost($currency)"),
                    "80",
                    "cost",
                    "cost",
                    "cost",
                    "convert_to_currency",
                    "",
                    "true",
                    "right",
                ],
                [
                    gettext("Initial <br>Increment"),
                    "100",
                    "init_inc",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Increment"),
                    "95",
                    "inc",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Setup <br> Fee($currency)"),
                    "90",
                    "setup",
                    "setup",
                    "setup",
                    "convert_to_currency",
                    "",
                    "true",
                    "right",
                ],
                [
                    gettext("Monthly<br> fee($currency)"),
                    "90",
                    "monthlycost",
                    "monthlycost",
                    "monthlycost",
                    "convert_to_currency",
                    "",
                    "true",
                    "right",
                ],
                [
                    gettext("Call Type"),
                    "80",
                    "call_type",
                    "call_type",
                    "call_type",
                    "get_call_type",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Destination"),
                    "95",
                    "extensions",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Status"),
                    "90",
                    "status",
                    "status",
                    "reseller_pricing",
                    "get_status",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Modified Date"),
                    "130",
                    "last_modified_date",
                    "last_modified_date",
                    "last_modified_date",
                    "convert_GMT_to",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Is purchased?"),
                    "100",
                    "number",
                    "number",
                    "number",
                    "check_did_avl_reseller",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Action"),
                    "90",
                    "",
                    "",
                    "",
                    [
                        "EDIT"   => [
                            "url"  => "did/did_reseller_edit/edit/",
                            "mode" => "popup",
                        ],
                        "DELETE" => [
                            "url"  => "did/did_reseller_edit/delete/",
                            "mode" => "single",
                        ],
                    ],
                ],
            ]
        );

        return $grid_field_arr;
    }

    /**
     * *********************************************************************
     */
    function build_grid_buttons()
    {
        $buttons_json = json_encode(
            [
                [
                    gettext("Create"),
                    "btn btn-line-warning btn",
                    "fa fa-plus-circle fa-lg",
                    "button_action",
                    "/did/did_add/",
                    "popup",
                    "medium",
                ],
                [
                    gettext("Delete"),
                    "btn btn-line-danger",
                    "fa fa-times-circle fa-lg",
                    "button_action",
                    "/did/did_delete_multiple/",
                ],
                [
                    gettext("Import"),
                    "btn btn-line-blue",
                    "fa fa-upload fa-lg",
                    "button_action",
                    "/did/did_import/",
                    '',
                    "small",
                ],
                [
                    gettext("Export"),
                    "btn btn-xing",
                    "fa fa-download fa-lg",
                    "button_action",
                    "/did/did_export_data_xls",
                    'single',
                ],
            ]
        );

        return $buttons_json;
    }

    function build_did_list_for_customer($accountid, $accounttype)
    {
        $account_info = $accountinfo = $this->CI->session->userdata('accountinfo');
        $currency_id = $account_info ['currency_id'];
        $currency = $this->CI->common->get_field_name('currency', 'currency', $currency_id);
        // array(display name, width, db_field_parent_table,feidname, db_field_child_table,function name);
        $grid_field_arr = json_encode(
            [
                [
                    gettext("DID"),
                    "110",
                    "number",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Country"),
                    "110",
                    "country_id",
                    "country",
                    "countrycode",
                    "get_field_name",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Per Minute Cost($currency)"),
                    "150",
                    "cost",
                    "cost",
                    "cost",
                    "convert_to_currency",
                    "",
                    "true",
                    "right",
                ],
                [
                    gettext("Initial Increment"),
                    "140",
                    "init_inc",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Increment"),
                    "120",
                    "inc",
                    "",
                    "",
                    "",
                    "",
                    "true",
                    "center",
                ],
                [
                    gettext("Setup Fee($currency)"),
                    "140",
                    "setup",
                    "setup",
                    "setup",
                    "convert_to_currency",
                    "",
                    "true",
                    "right",
                ],
                [
                    gettext("Monthly Fee($currency)"),
                    "140",
                    "monthlycost",
                    "monthlycost",
                    "monthlycost",
                    "convert_to_currency",
                    "",
                    "true",
                    "right",
                ],
                [
                    gettext("Action"),
                    "110",
                    "",
                    "",
                    "",
                    [
                        "DELETE" => [
                            "url"  => "accounts/".$accounttype."_dids_action/delete/$accountid/$accounttype/",
                            "mode" => "single",
                        ],
                    ],
                ],
            ]
        );

        return $grid_field_arr;
    }

    function build_did_list_for_reseller($accountid, $accounttype)
    {
        // array(display name, width, db_field_parent_table,feidname, db_field_child_table,function name);
        $grid_field_arr = json_encode(
            [
                [
                    "DID Number",
                    "120",
                    "number",
                    "",
                    "",
                    "",
                ],
                [
                    gettext("Increment"),
                    "120",
                    "inc",
                    "",
                    "",
                    "",
                ],
                [
                    gettext("Is purchased?"),
                    "120",
                    "number",
                    "number",
                    "number",
                    "check_did_avl_reseller",
                ],
                [
                    gettext("Per Minute Cost"),
                    "120",
                    "cost",
                    "cost",
                    "cost",
                    "convert_to_currency",
                ],
                [
                    gettext("Included<br> Seconds"),
                    "100",
                    "includedseconds",
                    "",
                    "",
                    "",
                ],
                [
                    gettext("Setup <br> Fee"),
                    "109",
                    "setup",
                    "setup",
                    "setup",
                    "convert_to_currency",
                ],
                [
                    gettext("Monthly<br> Fee"),
                    "140",
                    "monthlycost",
                    "monthlycost",
                    "monthlycost",
                    "convert_to_currency",
                ],
                [
                    gettext("Connection Cost"),
                    "149",
                    "connectcost",
                    "connectcost",
                    "connectcost",
                    "convert_to_currency",
                ],
                [
                    gettext("Disconnection <br> Fee"),
                    "140",
                    "disconnectionfee",
                    "disconnectionfee",
                    "disconnectionfee",
                    "convert_to_currency",
                ],
                [
                    gettext("Action"),
                    "100",
                    "",
                    "",
                    "",
                    [
                        "DELETE" => [
                            "url"  => "/accounts/reseller_did_action/delete/$accountid/$accounttype/",
                            "mode" => "single",
                        ],
                    ],
                ],
            ]
        );

        return $grid_field_arr;
    }
}

?>
